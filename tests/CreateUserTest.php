<?php

namespace Tests;

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('passport:install');
    }

    public function test_users_can_created_by_administrator()
    {
        $this->user = factory(User::class)->states('active')->create();
        $this->user->assign('Administrator');

        $this->authenticate();

        $this->post('/users', [
            'username' => 'johndoe',
            'name' => 'John Doe',
            'email' => 'john.doe@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
        ], [
            'Authorization' => 'Bearer ' . $this->accessToken
        ]);

        $this->seeStatusCode(200);

        $this->seeInDatabase('users', [
            'email' => 'john.doe@gmail.com',
        ]);
    }

    public function test_users_cant_created_except_by_administrator()
    {
        $this->user = factory(User::class)->states('active')->create();

        $this->authenticate();

        $this->post('/users', [
            'username' => 'johndoe',
            'name' => 'John Doe',
            'email' => 'john.doe@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
        ], [
            'Authorization' => 'Bearer ' . $this->accessToken
        ]);

        $this->seeStatusCode(403);

        $this->notSeeInDatabase('users', [
            'email' => 'john.doe@gmail.com',
        ]);
    }
}
