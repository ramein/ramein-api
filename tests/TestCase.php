<?php

namespace Tests;

use Laravel\Passport\Client;

abstract class TestCase extends \Laravel\Lumen\Testing\TestCase
{
    /**
     * The application url.
     *
     * @var string
     */
    protected $appUrl;

    /**
     * @var \App\Models\User
     */
    protected $user;

    /**
     * @var string
     */
    protected $accessToken;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->appUrl = env('APP_URL', 'http://app.dev');
        $this->baseUrl = env('APP_URL', 'http://app.dev').'/api/v1';
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('bouncer:seed');
    }

    /**
     * Define hooks to authenticating to the app before each test.
     *
     * @param  string  $email
     * @param  string  $password
     * @return $json
     */
    protected function authenticate($password = '12345678')
    {
        $client = Client::find(2);

        $this->post("{$this->appUrl}/oauth/token", [
            'username'      => $this->user->email,
            'password'      => $password,
            'client_id'     => $client->id,
            'client_secret' => $client->secret,
            'grant_type'    => 'password',
        ]);

        $this->accessToken = json_decode($this->response->getContent())->access_token;

        return $this;
    }

    /**
     * Seed a given database connection.
     *
     * @param  string  $class
     * @return void
     */
    public function seed($class = 'DatabaseSeeder')
    {
        $this->artisan('db:seed', ['--class' => $class]);
    }
}
