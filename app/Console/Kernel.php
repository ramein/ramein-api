<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Mlntn\Console\Commands\Serve::class,
        \App\Console\Commands\ControllerMakeCommand::class,
        \App\Console\Commands\ModelMakeCommand::class,
        \App\Console\Commands\RouteListCommand::class,
        \App\Console\Commands\StorageLinkCommand::class,
        \App\Console\Commands\TestMakeCommand::class,
        \App\Console\Commands\RequestMakeCommand::class,
        \App\Console\Commands\TinkerCommand::class,
        \App\Console\Commands\EventMakeCommand::class,
        \App\Console\Commands\ListenerMakeCommand::class,
        \App\Console\Commands\MailMakeCommand::class,
        \App\Console\Commands\NotificationMakeCommand::class,
        \App\Console\Commands\EventGenerateCommand::class,
        \App\Console\Commands\VendorPublishCommand::class,
        \App\Console\Commands\ViewClearCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
