<?php

if (! function_exists('isUserASuperAdminOrAdmin')) {
    /**
     * Determine is the user an Admin or Super Admin.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    function isUserASuperAdminOrAdmin($user = null)
    {
        return is_null($user)
            ? auth()->guest()
                ? false
                : auth()->user()->isSuperAdminOrAdmin()
            : $user->isSuperAdminOrAdmin();
    }
}

if (! function_exists('isUserASuperAdminOrAdminOrEO')) {
    /**
     * Determine is the user an Admin or Super Admin or Event Organizer.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    function isUserASuperAdminOrAdminOrEO($user = null)
    {
        return is_null($user)
            ? auth()->guest()
                ? false
                : auth()->user()->isSuperAdminOrAdmin() || auth()->user()->isEventOrganizer()
            : $user->isSuperAdminOrAdmin() || $user->isEventOrganizer();
    }
}

if (! function_exists('isUserAnAdmin')) {
    /**
     * Determine is the user an Admin.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    function isUserAnAdmin($user = null)
    {
        return is_null($user)
            ? auth()->guest()
                ? false
                : auth()->user()->isAdmin()
            : $user->isAdmin();
    }
}

if (! function_exists('isUserASuperAdmin')) {
    /**
     * Determine is the user a Super Admin.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    function isUserASuperAdmin($user = null)
    {
        return is_null($user)
            ? auth()->guest()
                ? false
                : auth()->user()->isSuperAdmin()
            : $user->isSuperAdmin();
    }
}
