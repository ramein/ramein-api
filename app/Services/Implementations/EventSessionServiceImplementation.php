<?php

namespace App\Services\Implementations;

use App\Models;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Services\EventSessionService;

class EventSessionServiceImplementation extends BaseService implements EventSessionService
{
    /**
     * {@inheritdoc}
     */
    public function createAll(Collection $data, Models\Event $event)
    {
        return DB::transaction(function () use ($data, $event) {
            $eventSession = $this->create($data, $event);

            // Location
            $eventSession->location()->save(new Models\Location([
                'longitude' => $data->get('location')['longitude'],
                'latitude' => $data->get('location')['latitude'],
            ]));

            // Tickets
            collect($data->get('tickets'))->each(function ($ticketData) use ($eventSession) {
                $this->createEventTicket(collect($ticketData), $eventSession);
            });

            // Questions
            collect($data->get('questions'))->each(function ($questionData) use ($eventSession) {
                $this->createQuestion(collect($questionData), $eventSession);
            });

            return $eventSession;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function updateAll(Collection $data, Models\EventSession $eventSession)
    {
        return DB::transaction(function () use ($data, $eventSession) {
            $eventSession = $this->update($data, $eventSession);

            // Location
            $eventSession->location->forceFill([
                'longitude' => $data->get('location')['longitude'],
                'latitude' => $data->get('location')['latitude'],
            ])->save();

            // Tickets
            collect($data->get('tickets'))->each(function ($ticketData) use ($eventSession) {
                $ticketData = collect($ticketData);

                $eventTicket = $eventSession->eventTickets()->find($ticketData->get('ticket_id'));

                if (is_null($eventTicket)) {
                    $this->createEventTicket($ticketData, $eventSession);
                } else {
                    $this->updateEventTicket($ticketData, $eventTicket);
                }
            });

            // Questions
            collect($data->get('questions'))->each(function ($questionData) use ($eventSession) {
                $questionData = collect($questionData);

                $eventSessionQuestion = $eventSession->questions()->find($questionData->get('question_id'));

                if (is_null($eventSessionQuestion)) {
                    $this->createQuestion($questionData, $eventSession);
                } else {
                    $this->updateQuestion($questionData, $eventSessionQuestion);
                }
            });

            return $eventSession;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function create(Collection $data, Models\Event $event)
    {
        $eventSession = Models\EventSession::forceCreate([
            'event_id' => $event->id,
            'event_type_id' => $data->get('event_type'),
            'session' => $data->get('session'),
            'details' => $data->get('details'),
            'address' => $data->get('address'),
            'from' => Date::createFromFormat('d/m/Y H:i', $data->get('from')),
            'until' => Date::createFromFormat('d/m/Y H:i', $data->get('until')),
        ]);

        return $eventSession;
    }

    /**
     * {@inheritdoc}
     */
    public function update(Collection $data, Models\EventSession $eventSession)
    {
        $eventSession->forceFill([
            'event_type_id' => $data->get('event_type'),
            'session' => $data->get('session'),
            'details' => $data->get('details'),
            'address' => $data->get('address'),
            'from' => Date::createFromFormat('d/m/Y H:i', $data->get('from')),
            'until' => Date::createFromFormat('d/m/Y H:i', $data->get('until')),
        ])->save();

        return $eventSession;
    }

    /**
     * {@inheritdoc}
     */
    public function createEventTicket(Collection $data, Models\EventSession $eventSession)
    {
        $eventTicket = Models\EventTicket::forceCreate([
            'event_id' => $eventSession->event->id,
            'name' => $data->get('name'),
            'price' => $data->get('price', 0),
            'quota' => $data->get('quota', 0),
            'description' => $data->get('description'),
            'ticket_currency_id' => $data->get('ticket_currency'),
            'max_refund_on' => $data->get('max_refund_on'),
            'from' => Date::createFromFormat('d/m/Y H:i', $data->get('from')),
            'until' => Date::createFromFormat('d/m/Y H:i', $data->get('until')),
            'auto_approve_ticket' => $data->get('auto_approve', true),
            'auto_hide_ticket' => $data->get('auto_hide', true),
        ]);

        $eventSession->eventTickets()->save($eventTicket);

        return $eventTicket;
    }

    /**
     * {@inheritdoc}
     */
    public function updateEventTicket(Collection $data, Models\EventTicket $eventTicket)
    {
        $eventTicket->forceFill([
            'name' => $data->get('name'),
            'price' => $data->get('price', 0),
            'quota' => $data->get('quota', 0),
            'description' => $data->get('description'),
            'ticket_currency_id' => $data->get('ticket_currency'),
            'max_refund_on' => $data->get('max_refund_on'),
            'from' => Date::createFromFormat('d/m/Y H:i', $data->get('from')),
            'until' => Date::createFromFormat('d/m/Y H:i', $data->get('until')),
            'auto_approve_ticket' => $data->get('auto_approve', true),
            'auto_hide_ticket' => $data->get('auto_hide', true),
        ])->save();

        return $eventTicket;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuestion(Collection $data, Models\EventSession $eventSession)
    {
        $question = Models\EventSessionQuestion::forceCreate([
            'event_session_id' => $eventSession->id,
            'value' => $data->get('value'),
            'is_required' => $data->get('is_required', false),
            'need_attachment' => $data->get('need_attachment', false),
        ]);

        return $question;
    }

    /**
     * {@inheritdoc}
     */
    public function updateQuestion(Collection $data, Models\EventSessionQuestion $eventSessionQuestion)
    {
        $eventSessionQuestion->forceFill([
            'value' => $data->get('value'),
            'is_required' => $data->get('is_required', false),
            'need_attachment' => $data->get('need_attachment', false),
        ])->save();

        return $eventSessionQuestion;
    }

    /**
     * Get event sessions from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getEventSessions(Collection $options, $model = null, array $includes = [])
    {
        if (is_null($model)) {
            $eventSessions = new Models\EventSession();
        } else {
            $eventSessions = $model->sessions();
        }

        $includes = array_merge($includes, Models\EventSession::shouldLoads());

        return $this->preprocessing(
            $eventSessions->with($includes), $options
        );
    }

    /**
     * Get specific event session from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\EventSession  $eventSession
     * @param  array  $includes
     * @return \App\Models\EventSession
     */
    public function getEventSession(Collection $options, Models\EventSession $eventSession, array $includes = [])
    {
        $includes = array_merge($includes, Models\EventSession::shouldLoads());

        return $this->preprocessing(
            $eventSession->load($includes), $options, true
        );
    }

    /**
     * Filter the given model with the given filters.
     *
     * @param  mixed  $model
     * @param  \Illuminate\Support\Collection  $filters
     * @param  boolean  $isSingleRecord
     * @return void
     */
    protected function filter(&$model, Collection $filters, $isSingleRecord = false)
    {
        parent::filter($model, $filters, $isSingleRecord);

        //
    }
}
