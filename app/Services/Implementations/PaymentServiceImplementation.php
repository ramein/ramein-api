<?php

namespace App\Services\Implementations;

use App\Models;
use Carbon\Carbon;
use Keygen\Keygen;
use App\Services\PaymentService;
use Illuminate\Support\Collection;
use Silber\Bouncer\BouncerFacade as Bouncer;

class PaymentServiceImplementation extends BaseService implements PaymentService
{
    /**
     * Create payment.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Transaction  $transaction
     * @return \App\Models\Payment
     */
    public function create(Collection $data, Models\Transaction $transaction)
    {
        $payment = new Models\Payment();

        $payment->forceFill([
            'code' => $this->generateCode(),
            'name' => $this->generateName($transaction),
            'destination_bank_id' => $data->get('destination_bank'),
            'provider' => $data->get('provider'),
            'provider_number' => $data->get('provider_number'),
            'provider_holder_name' => $data->get('provider_holder_name'),
            'amount' => $data->get('amount', 0),
            'description' => $data->get('description'),
            'is_verified' => $data->get('is_verified', false),
            'is_declined' => $data->get('is_declined', false),
            'paid_on' => Carbon::parse($data->get('paid_on', Carbon::now())),
        ]);

        $transaction->payments()->save($payment);

        return $payment;
    }

    /**
     * Get payments from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getPayments(Collection $options, $model = null, array $includes = [])
    {
        if (is_null($model)) {
            $payments = new Models\Payment();
        } else {
            $payments = $model->payments();
        }

        $includes = array_merge($includes, Models\Payment::shouldLoads());

        return $this->preprocessing(
            $payments->with($includes), $options
        );
    }

    /**
     * Get specific payment from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Payment  $payment
     * @param  array  $includes
     * @return \App\Models\Payment
     */
    public function getPayment(Collection $options, Models\Payment $payment, array $includes = [])
    {
        $includes = array_merge($includes, Models\Payment::shouldLoads());

        return $this->preprocessing(
            $payment->load($includes), $options, true
        );
    }

    /**
     * Attach ability for payment to the appropriate customer.
     *
     * @param  \App\Models\Payment  $payment
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function attachAbility(Models\Payment $payment, Models\Customer $customer)
    {
        // Attach ability to the Administrator role.
        Bouncer::allow('Administrator')->toManage($payment);

        // Attach ability to the customer.
        Bouncer::allow($customer->user)->toManage($payment);
    }

    /**
     * Detach ability for payment from the appropriate customer.
     *
     * @param  \App\Models\Payment  $payment
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function detachAbility(Models\Payment $payment, Models\Customer $customer)
    {
        // Detach ability from the Administrator role.
        Bouncer::disallow('Administrator')->toManage($payment);

        // Detach ability from the customer.
        Bouncer::disallow($customer->user)->toManage($payment);
    }

    /**
     * Generate payment code.
     *
     * @return string
     */
    public function generateCode()
    {
        $code = Keygen::length(60)->alphanum()->generate();

        while (Models\Payment::whereCode($code)->count() > 0) {
            $code = Keygen::length(60)->alphanum()->generate();
        }

        return $code;
    }

    /**
     * Generate payment name.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return string
     */
    public function generateName(Models\Transaction $transaction)
    {
        return str_replace('TRX', 'PAY', $transaction->name);
    }
}
