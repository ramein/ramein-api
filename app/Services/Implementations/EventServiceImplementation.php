<?php

namespace App\Services\Implementations;

use App\Models;
use Keygen\Keygen;
use App\Services\EventService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Services\EventSessionService;
use Illuminate\Support\Facades\Storage;
use Silber\Bouncer\BouncerFacade as Bouncer;

class EventServiceImplementation extends BaseService implements EventService
{
    /**
     * The default attributes that should be except-ed from response.
     *
     * @var  array
     */
    protected $defaultExcept = [
        'sessions.event_tickets.tickets_not_canceled',
        'sessions.tickets.transaction',
        'sessions.tickets.transaction.valid_payments',
        'sessions.tickets.transaction.event_session_tickets',
        'sessions.tickets.transaction.event_session_tickets.event_ticket',
    ];

    /**
     * @var \App\Services\EventSessionService
     */
    protected $eventSessionService;

    /**
     * Constructor.
     *
     * @param  \App\Services\EventSessionService  $eventSessionService
     */
    public function __construct(EventSessionService $eventSessionService)
    {
        $this->eventSessionService = $eventSessionService;
    }

    /**
     * Preprocessing the event with options params.
     *
     * @param  mixed  $model
     * @param  \Illuminate\Support\Collection  $options
     * @param  boolean  $isSingleRecord
     * @param  array  $includes
     * @param  boolean  $paginate
     * @return mixed
     */
    protected function preprocessing(
        $model,
        Collection $options,
        $isSingleRecord = false,
        array $includes = [],
        $paginate = true
    ) {
        $this->mergeWithDefaultIncludes([
            $includes,
            Models\Event::shouldLoads(),
        ]);

        $ticketRelation = array_prefix(Models\Ticket::shouldLoads(), 'sessions.tickets');

        $include = $this->processIncludes($options);
        $except = $this->parseParam('except', $options, ',');
        $sorts = $this->processSort($options);

        foreach ($include as $item) {
            switch (true) {
                case in_array($item, [
                    'user',
                    'category',
                    'poster',
                    'sessions',
                    'sessions.location',
                    'sessions.questions',
                ]):
                    break;
                case $item === 'sessions.tickets':
                    if (! isUserASuperAdminOrAdminOrEO()) {
                        $item = [];
                    } else {
                        $item = $ticketRelation;
                    }
                    break;
                case in_array($item, [
                    'sessions.eventTickets',
                    'sessions.eventTickets.currency',
                    'sessions.eventTickets.eventSessionTicket',
                ]):
                    //
                    break;
                case in_array($item, [
                    'sessions.eventTickets.tickets',
                    'sessions.eventTickets.ticketsNotCanceled'
                ]):
                    if (! isUserASuperAdminOrAdminOrEO()) {
                        $item = [];
                    } else {
                        $this->removeFromDefaultExcept([$item]);
                        $item = array_merge(
                            [$item],
                            array_prefix(Models\Ticket::shouldLoads(), $item)
                        );
                    }
                    break;
                case $item === 'sessions.tickets.transaction':
                    if (! isUserASuperAdminOrAdminOrEO()) {
                        $item = [];
                    } else {
                        $this->removeFromDefaultExcept([$item]);
                        $item = array_merge([$item], $ticketRelation);
                    }
                    break;
                case in_array($item, [
                    'sessions.tickets.transaction.customer',
                    'sessions.tickets.transaction.customer.user',
                ]):
                    if (! isUserASuperAdminOrAdminOrEO()) {
                        $item = [];
                    } else {
                        $this->removeFromDefaultExcept([
                            'sessions.tickets.transaction',
                        ]);
                        $item = array_merge([$item], $ticketRelation);
                    }
                    break;
                case $item === 'sessions.tickets.transaction.validPayments':
                    if (! isUserASuperAdminOrAdminOrEO()) {
                        $item = [];
                    } else {
                        $this->removeFromDefaultExcept([
                            $item,
                            'sessions.tickets.transaction',
                        ]);
                        $item = array_merge([$item], $ticketRelation);
                    }
                    break;
                case in_array($item, $relations = [
                    'sessions.tickets.transaction.eventSessionTickets',
                    'sessions.tickets.transaction.eventSessionTickets.eventTicket',
                ]):
                    if (! isUserASuperAdminOrAdminOrEO()) {
                        $item = [];
                    } else {
                        $this->removeFromDefaultExcept([
                            'sessions.tickets.transaction',
                        ]);
                        $this->removeParentFromDefaultExcept($relations, $item);
                        $item = array_merge([$item], $ticketRelation);
                    }
                    break;
                default:
                    $item = [];
            }

            $includes = array_merge($includes, is_array($item) ? $item : [$item]);
        }

        foreach ($sorts as $sort) {
            $include = [];

            switch ($sort) {
                case '':
                    //
                    break;
            }

            $includes = array_merge($includes, $include);
        }

        return parent::preprocessing($model, $options, $isSingleRecord, $includes, $paginate);
    }

    /**
     * {@inheritdoc}
     */
    public function createAll(Collection $data, Models\User $user)
    {
        return DB::transaction(function () use ($data, $user) {
            $event = $this->create($data, $user);

            collect($data->get('sessions'))->each(function ($sessionData) use ($event) {
                $this->eventSessionService->createAll(collect($sessionData), $event);
            });

            return $event;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function updateAll(Collection $data, Models\Event $event)
    {
        return DB::transaction(function () use ($data, $event) {
            $event = $this->update($data, $event);

            collect($data->get('sessions'))->each(function ($sessionData) use ($event) {
                $sessionData = collect($sessionData);

                $eventSession = $event->sessions()->find($sessionData->get('session_id'));

                if (is_null($eventSession)) {
                    $this->eventSessionService->createAll(collect($sessionData), $event);
                } else {
                    $this->eventSessionService->updateAll(collect($sessionData), $eventSession);
                }
            });

            return $event;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function create(Collection $data, Models\User $user)
    {
        $event = Models\Event::forceCreate([
            'code' => $this->generateCode(),
            'title' => $data->get('title'),
            'user_id' => $user->id,
            'event_category_id' => $data->get('event_category'),
            'is_public' => $data->get('public', true),
            'password' => ($password = $data->get('password')) ? bcrypt($password) : null,
        ]);

        return $event;
    }

    /**
     * {@inheritdoc}
     */
    public function update(Collection $data, Models\Event $event)
    {
        $event->forceFill([
            'title' => $data->get('title'),
            'event_category_id' => $data->get('event_category'),
            'is_public' => $data->get('public', true),
            'password' => ($password = $data->get('password')) ? bcrypt($password) : null,
        ])->save();

        return $event;
    }

    /**
     * Get events from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getEvents(
        Collection $options,
        $model = null,
        array $includes = []
    ) {
        if (is_null($model)) {
            $events = new Models\Event();
        } else {
            $events = $model->events();
        }

        return $this->preprocessing(
            $events, $options, false, $includes
        );
    }

    /**
     * Get specific event from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Event  $event
     * @param  array  $includes
     * @return \App\Models\Event
     */
    public function getEvent(
        Collection $options,
        Models\Event $event,
        array $includes = []
    ) {
        return $this->preprocessing(
            $event, $options, true, $includes
        );
    }

    /**
     * Sort the results by the sortest event.
     *
     * @param  \Illuminate\Support\Collection|\App\Models\Event  $results
     * @param  array  $option
     * @return void
     */
    public function sortSortest(&$results, array $option)
    {
        if ($results instanceof Collection) {
            $direction = ! isset($option[0]) ? 'asc' : $option[0];

            $results = $results->sortBy(
                'sessions.0.from',
                SORT_REGULAR,
                strtolower($direction) === 'desc'
            )->values();
        }
    }

    /**
     * Sort the results by minimum ticket price.
     *
     * @param  \Illuminate\Support\Collection|\App\Models\Event  $results
     * @param  array  $option
     * @return void
     */
    public function sortMinimumTicketPrice(&$results, array $option)
    {
        $func = function ($results, $direction) {
            return $results->sortBy(
                'minimum_ticket.price',
                SORT_REGULAR,
                strtolower($direction) === 'desc'
            )->values();
        };

        if ($results instanceof Collection) {
            if (isset($option[0]) && strtolower($option[0]) === 'sessions') {
                $direction = ! isset($option[1]) ? 'asc' : $option[1];

                $results->transform(function ($event) use ($func, $direction) {
                    $event->setRelation(
                        'sessions',
                        $func($event->sessions, $direction)
                    );

                    return $event;
                });
            } else {
                $direction = ! isset($option[0]) ? 'asc' : $option[0];

                $results = $func($results, $direction);
            }
        } else {
            $direction = ! isset($option[0]) ? 'asc' : $option[0];

            $results->setRelation(
                'sessions',
                $func($results->sessions, $direction)
            );
        }
    }

    /**
     * Attach ability for event to the appropriate user.
     *
     * @param  \App\Models\Event  $event
     * @param  \App\Models\User  $user
     * @return void
     */
    public function attachAbility(Models\Event $event, Models\User $user)
    {
        if (! $user->isSuperAdmin()) {
            // Attach ability to the Administrator role.
            Bouncer::allow('Administrator')->toManage($event);
        }

        // Attach ability to the user.
        Bouncer::allow($user)->toManage($event);
    }

    /**
     * Detach ability for event from the appropriate user.
     *
     * @param  \App\Models\Event  $event
     * @param  \App\Models\User  $user
     * @return void
     */
    public function detachAbility(Models\Event $event, Models\User $user)
    {
        if (! $user->isSuperAdmin()) {
            // Detach ability from the Administrator role.
            Bouncer::disallow('Administrator')->toManage($event);
        }

        // Detach ability from the user.
        Bouncer::disallow($user)->toManage($event);
    }

    /**
     * {@inheritdoc}
     */
    public function storePoster($media, Models\User $user, Models\Event $event)
    {
        if ($location = $media->store('media/' . $user->id . '/event', 'public')) {
            if ($file = $this->storeMedia($media, $location, $user)) {
                $event->poster()->save($file);

                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function updatePoster($media, Models\User $user, Models\Event $event)
    {
        $this->destroyPoster($event);

        return $this->storePoster($media, $user, $event);
    }

    /**
     * {@inheritdoc}
     */
    public function destroyPoster(Models\Event $event)
    {
        if (! is_null($event->poster)) {
            Storage::disk('public')->delete($event->poster->location);
            $event->poster->delete();
        }
    }

    /**
     * Generate event code.
     *
     * @return string
     */
    public function generateCode()
    {
        $code = Keygen::length(60)->alphanum()->generate();

        while (Models\Event::whereCode($code)->count() > 0) {
            $code = Keygen::length(60)->alphanum()->generate();
        }

        return $code;
    }
}
