<?php

namespace App\Services\Implementations;

use Exception;
use App\Models;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\Process\Process;
use App\Exceptions\InvalidParameterException;

abstract class BaseService
{
    /**
     * The default relations that should be eager loaded.
     *
     * @var  array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * The default attributes that should be except-ed from response.
     *
     * @var  array
     */
    protected $defaultExcept = [
        //
    ];

    /**
     * The default sort that should be run.
     *
     * @var  array
     */
    protected $defaultSort = [
        //
    ];

    /**
     * Preprocessing the given model with options params.
     *
     * @param  mixed  $model
     * @param  \Illuminate\Support\Collection  $options
     * @param  boolean  $isSingleRecord
     * @param  array  $includes
     * @param  boolean  $paginate
     * @return mixed
     *
     * @throws Exception
     */
    protected function preprocessing(
        $model,
        Collection $options,
        $isSingleRecord = false,
        array $includes = [],
        $paginate = true
    ) {
        $filters = $this->parseParam('filters', $options);
        $sorts = $this->parseParam('sort', $options);
        $except = $this->processExcept($options);
        $includes = array_unique(array_merge($this->defaultIncludes, $includes));

        if (! $model instanceof Collection) {
            // @INCLUDES
            if ($isSingleRecord) {
                $model = $model->load($includes);
            } else {
                $model = $model->with($includes);
            }

            // @FILTERS
            $this->filter($model, collect($filters), $isSingleRecord);

            if (! $isSingleRecord) {
                // @SEARCH
                // @TODO: Improve this.
                if (!is_null($q = $options->get('q'))) {
                    $searching = function ($column) use (&$model, $q) {
                        if (Schema::hasColumn($model->getModel()->getTable(), $column)) {
                            $model = $model->orWhere($column, 'like', '%' . $q . '%');
                        }
                    };

                    $columns = ['title', 'name', 'address', 'details'];

                    foreach ($columns as $column) {
                        $searching($column);
                    }
                }

                // @SORT
                foreach ($sorts as $sort) {
                    $sort = explode(',', $sort);
                    $direction = !isset($sort[1]) ? 'asc' : $sort[1];

                    $tmp = clone $model;

                    try {
                        $model->orderBy($sort[0], $direction)->get();
                        $model = $tmp->orderBy($sort[0], $direction);
                    } catch (Exception $e) {
                        $model = $tmp;
                    }
                }

                $results = $model->get();
            } else {
                $results = $model;
            }
        } else {
            $results = $model;
        }

        // @FILTERS
//        $this->filter($results, collect($filters), $isSingleRecord);

        // @SORT
        foreach ($sorts as $sort) {
            $sort = explode(',', $sort);
            $func = 'sort' . studly_case($sort[0]);
            $option = array_slice($sort, 1);

            if (method_exists($this, $func)) {
                $this->$func($results, $option);
            } else {
                $sortKeys = explode('.', $sort[0]);
                $direction = isset($sort[1]) && $sort[1] === 'desc' ? true : false;

                try {
                    if ($results instanceof Collection) {
                        if (count($sortKeys) > 1) {
                            $results->transform(function ($item) use ($sortKeys, $direction) {
                                $item = collect($item);
                                $key = array_pop($sortKeys);
                                $getKey = implode('.', $sortKeys);

                                if (is_array($data = $item->get($getKey))) {
                                    $data = collect($data)->sortBy(
                                        $key,
                                        SORT_REGULAR,
                                        $direction
                                    )->values();

                                    $item->put($getKey, $data);
                                }

                                return $item->toArray();
                            });
                        }
                    }
                } catch (Exception $e) {
                    //
                }
            }
        }

        // @EXCEPT
        $deep = 5;

        // TODO: Grouping the 'except'-ed attr, so It can save an iteration.
        $unsetIt = function (&$results) use ($except, $deep) {
            foreach ($except as $attr) {
                if (! in_array($attr, $this->defaultExcept)) {
                    if (count(explode('.', $attr)) <= $deep) {
                        array_unset_value($results, $attr);
                    } else {
                        throw new InvalidParameterException('The except deep maximum is ' . $deep);
                    }
                } else {
                    array_unset_value($results, $attr);
                }
            }
        };

        if ($results instanceof Collection) {
            $results = $results->toArray();
            foreach ($results as &$result) {
                $unsetIt($result);
            }
            $results = collect($results);
        } else {
            $results = $results->toArray();
            $unsetIt($results);
        }

        if ($results instanceof Collection && $paginate) {
            return paginate($results, 10);
        }

        return $results;
    }

    /**
     * Filter the given model with the given filters.
     *
     * @param  mixed  $model
     * @param  \Illuminate\Support\Collection  $filters
     * @param  boolean  $isSingleRecord
     * @return void
     */
    protected function filter(&$model, Collection $filters, $isSingleRecord = false)
    {
        $filters->each(function ($filter) use (&$model, $isSingleRecord) {
            $origFilter = $filter;
            $origModel = clone $model;

            $filter = explode(',', $filter);
            $filterLength = count($filter);
            $executed = false;

            $testQuery = function (&$model, $scope = null) use ($origModel, &$executed) {
                try {
                    if (! is_null($scope)) {
                        $model = $model->$scope();
                    } else {
                        $model->get();
                    }

                    $executed = true;
                } catch (Exception $e) {
                    $model = $origModel;
                }
            };

            if (!$model instanceof Collection) {
                // 1
                $func = 'filter' . studly_case($filter[0]);

                if (method_exists($this, $func)) {
                    $this->$func($model, array_slice($filter, 1));
                    $executed = true;
                }

                // 2
                if ($filterLength == 1 && !$executed) {
                    $testQuery($model, $filter[0]);
                }

                // 3
                if ($filterLength == 2 && !$executed) {
                    $func = $isSingleRecord ? 'load' : 'with';
                    list($scope, $relation) = $filter;

                    $origModel = clone $model;

                    try {
                        $model = $model->$func([$relation => function ($query) use ($scope) {
                            return $query->$scope();
                        }]);

                        $model->get();

                        $executed = true;
                    } catch (Exception $e) {
                        $model = $origModel;
                    }
                }

                // 4
                if (!$executed) {
                    if ($filterLength == 2) {
                        $model = $model->where($filter[0], '=', $filter[1]);
                        $testQuery($model);
                    } elseif ($filterLength == 3) {
                        $filter[1] = strtolower($filter[1]);

                        if ($filter[1] === 'like') {
                            $filter[2] = '%' . $filter[2] . '%';
                        }

                        $model = $model->where($filter[0], $filter[1], $filter[2]);
                        $testQuery($model);
                    }
                }
            } else {
                $tmpModel = clone $model;

                try {
                    if ($filterLength === 2) {
                        $model = $model->where($filter[0], $filter[1]);
                    } elseif ($filterLength === 3) {
                        $model = $model->where($filter[0], $filter[1], $filter[2]);
                    }
                } catch (Exception $e) {
                    $model = $tmpModel;
                }
            }
        });
    }

    /**
     * Store model media to the storage.
     *
     * @param  mixed  $media
     * @param  string  $location
     * @param  \App\Models\User  $user
     * @return \App\Models\File
     */
    public function storeMedia()
    {
        $args = func_get_args();
        $this->checkParameters($args, 3);

        $media = $args[0];
        $location = $args[1];
        $user = $args[2];

        $this->compressMedia($media, $location);

        $file           = new Models\File;
        $file->user_id  = $user->id;
        $file->name     = $media->getClientOriginalName();
        $file->location = $location;

        return $file;
    }

    /**
     * Compress media in the storage.
     *
     * @param  mixed  $media
     * @param  string  $location
     * @return boolean
     */
    protected function compressMedia($media, $location)
    {
        $locationFull = public_path('storage/' . $location);
        $mediaSize    = $media->getClientSize();
        $mediaExt     = strtolower($media->getClientOriginalExtension());

        $allowedImgExt = [
            'png', 'jpg', 'jpeg', 'gif',
        ];

        try {
            if (in_array($mediaExt, $allowedImgExt)) {
                if ($mediaExt == "png") {
                    $process = new Process(
                        'pngquant -o "'. $locationFull .'" --force --quality=50 "'. $locationFull .'"'
                    );
                    $process->run();
                } else {
                    if ($mediaSize >= 100000) {
                        $process = new Process(
                            'convert "'. $locationFull .'" -quality 40 "'. $locationFull . '"'
                        );
                        $process->run();
                    }
                }
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Check minimum parameters.
     *
     * @param  integer  $numArgs
     * @param  integer  $min
     * @return void
     *
     * @throws Exception
     */
    protected function checkParameters($numArgs, $min)
    {
        if ($numArgs < $min) {
            throw new Exception('Required more parameters.');
        }
    }

    /**
     * Parse the given param.
     *
     * @param  string  $name
     * @param  \Illuminate\Support\Collection  $options
     * @param  string  $delimiter
     * @return array
     *
     * @throws \App\Exceptions\InvalidParameterException
     */
    protected function parseParam($name, $options, $delimiter = ';')
    {
        $param = $options->get($name, ['']);

        if (! is_array($param)) {
            // throw new InvalidParameterException(ucfirst($name) . ' should be an array.');
            return [];
        }

        return array_unique(array_filter(explode($delimiter, $param[0]), 'strlen'));
    }

    /**
     * Process the includes.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @return array
     */
    protected function processIncludes(Collection $options)
    {
        $include = $this->parseParam('includes', $options, ',');

        return array_merge($include, $this->defaultIncludes);
    }

    /**
     * Process the except.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @return array
     */
    protected function processExcept(Collection $options)
    {
        $except = $this->parseParam('except', $options, ',');

        return array_merge($except, $this->defaultExcept);
    }

    /**
     * Process the sort.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @return array
     */
    protected function processSort(Collection $options)
    {
        $sort = $this->parseParam('sort', $options);

        return array_merge($sort, $this->defaultSort);
    }

    /**
     * Determine the given except exists or not in default except.
     *
     * @param  array  $value
     * @return boolean
     */
    protected function existOnDefaultExcept($value)
    {
        return $this->existOnDefault($value, $this->defaultExcept);
    }

    /**
     * Determine the given 'include' exists or not in default includes.
     *
     * @param  array  $value
     * @return boolean
     */
    protected function existOnDefaultIncludes($value)
    {
        return $this->existOnDefault($value, $this->defaultIncludes);
    }

    /**
     * Determine the given value exists or not in default.
     *
     * @param  array  $value
     * @param  array  $default
     * @return boolean
     */
    protected function existOnDefault($value, array $default)
    {
        return in_array($value, $default);
    }

    /**
     * Determine the given value exists or not in values.
     *
     * @param  array  $value
     * @param  array  $values
     * @return boolean
     */
    protected function existOn($value, array $values)
    {
        return in_array($value, $values);
    }

    /**
     * Merge 'includes' with default includes and
     * remove 'except' from default except.
     *
     * @param  array  $relations
     * @return void
     */
    protected function mergeIncludesRemoveExcept(array $relations)
    {
        $this->mergeWithDefaultIncludes($relations);
        $this->removeFromDefaultExcept($relations);
    }

    /**
     * Remove 'includes' from default includes and
     * merge 'except' with default except.
     *
     * @param  array  $relations
     * @return void
     */
    protected function removeIncludesMergeExcept(array $relations)
    {
        $this->removeFromDefaultIncludes($relations);
        $this->mergeWithDefaultExcept($relations);
    }

    /**
     * Merge 'includes' with default includes.
     *
     * @param  array  $includes
     * @return void
     */
    protected function mergeWithDefaultIncludes(array $includes)
    {
        $includes = array_transform_value($includes, 'camel_case');

        $this->mergeWithDefault($includes, $this->defaultIncludes);
    }

    /**
     * Remove 'includes' from default includes.
     *
     * @param  array  $includes
     * @return void
     */
    protected function removeFromDefaultIncludes(array $includes)
    {
        $includes = array_transform_value($includes, 'camel_case');

        $this->removeFromDefault($includes, $this->defaultIncludes);
    }

    /**
     * Merge 'except' with default except.
     *
     * @param  array  $except
     * @return void
     */
    protected function mergeWithDefaultExcept(array $except)
    {
        $except = array_transform_value($except, 'snake_case');

        $this->mergeWithDefault($except, $this->defaultExcept);
    }

    /**
     * Remove 'except' from default except.
     *
     * @param  array  $except
     * @return void
     */
    protected function removeFromDefaultExcept(array $except)
    {
        $except = array_transform_value($except, 'snake_case');

        $this->removeFromDefault($except, $this->defaultExcept);
    }

    /**
     * Remove the parent of 'except' from default except.
     *
     * @param  array  $relations
     * @param  string  $except
     * @return void
     */
    protected function removeParentFromDefaultExcept(array $relations, $except)
    {
        sort($relations);

        $relations = array_slice($relations, 0, array_search($except, $relations)+1);

        $this->removeFromDefaultExcept($relations);
    }

    /**
     * Transform the given 'includes'.
     *
     * @param  array  $includes
     * @return array
     */
    protected function transformIncludes(array $includes)
    {
        return array_transform_value($includes, 'camel_case');
    }

    /**
     * Transform the given 'except'.
     *
     * @param  array  $except
     * @return array
     */
    protected function transformExcept(array $except)
    {
        return array_transform_value($except, 'snake_case');
    }

    /**
     * Merge the given values with default.
     *
     * @param  array  $values
     * @param  array  $default
     * @return void
     */
    protected function mergeWithDefault(array $values, array &$default)
    {
        foreach ($values as $item) {
            $default = array_merge(
                $default,
                is_array($item) ? $item : [$item]
            );
        }
    }

    /**
     * Remove the given values from default.
     *
     * @param  array  $values
     * @param  array  $default
     * @return void
     */
    protected function removeFromDefault(array $values, array &$default)
    {
        $default = array_diff($default, $values);
    }

    /**
     * Get the deepest relation from the given relations.
     *
     * @param  array  $relations
     * @return string
     */
    protected function getDeepestRelation(array $relations)
    {
        sort($relations);

        return $relations[count($relations)-1];
    }
}
