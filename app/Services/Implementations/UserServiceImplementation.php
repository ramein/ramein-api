<?php

namespace App\Services\Implementations;

use App\Models;
use Keygen\Keygen;
use Illuminate\Support\Str;
use App\Services\UserService;
use Illuminate\Support\Collection;
use Silber\Bouncer\BouncerFacade as Bouncer;

class UserServiceImplementation extends BaseService implements UserService
{
    /**
     * Create user.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @return \App\Models\User
     */
    public function create(Collection $data)
    {
        $model = null;
        $role = strtoupper($data->get('role'));

        if (Str::is($role, 'OWNER')) {
            $model = Models\Owner::create();
        } elseif (Str::is($role, 'CUSTOMER')) {
            $model = Models\Customer::create();
        } else {
            $model = Models\EventOrganizer::create([
                'type' => $data->get('type'),
            ]);
        }

        $model->user()->save(new Models\User($data->all()));

        if (! is_null($photo = $data->get('photo'))) {
            $this->storeMedia($photo, $model->user);
        }

        return $model->user->load('userable');
    }

    /**
     * Update user.
     *
     * @param  \App\Models\User  $user
     * @param  \Illuminate\Support\Collection  $data
     * @return \App\Models\User
     */
    public function update(Models\User $user, Collection $data)
    {
        if (Str::is($user->role, 'OWNER')) {
            $user->userable->update($data->all());
        } elseif (Str::is($user->role, 'CUSTOMER')) {
            $user->userable->update($data->all());
        } else {
            if ($data->get('type') && auth()->user()->isSuperAdminOrAdmin()) {
                $user->userable->update([
                    'type' => $data->get('type'),
                ]);
            }
        }

        $user->update($data->all());

        return $user->load('userable');
    }

    /**
     * Get users from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $data
     * @return mixed
     */
    public function getUsers(Collection $options, $data)
    {
        return $this->preprocessing($data, $options);
    }

    /**
     * Get specific user from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\User  $user
     * @return \App\Models\User
     */
    public function getUser(Collection $options, Models\User $user)
    {
        return $this->preprocessing($user, $options, true);
    }

    /**
     * Attach ability to the appropriate user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function attachAbility(Models\User $user)
    {
        // Attach ability to the Administrator role.
        Bouncer::allow('Administrator')->toManage($user);

        // Attach ability to the user itself.
        Bouncer::allow($user)->toManage($user);
    }

    /**
     * Detach ability from the appropriate user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function detachAbility(Models\User $user)
    {
        // Detach ability from the Administrator role.
        Bouncer::disallow('Administrator')->toManage($user);

        // Detach ability from the user.
        Bouncer::disallow($user)->toManage($user);
    }

    /**
     * Assign Super Administrator role to the appropriate user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function assignSuperAdminRole(Models\User $user)
    {
        // Disallow Administrator to manage Super Administrator resource.
        Bouncer::disallow('Administrator')->toManage($user);

        // Retract Administrator role.
        $user->retract('Administrator');

        // Assign Super Administrator role to the user.
        $user->assign('Super Administrator');
    }

    /**
     * Give user follower (friend) ability to all user posts.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $friend
     * @return void
     */
    public function attachPostsAbility(Models\User $user, Models\User $friend)
    {
        $user->posts->each(function ($post) use ($friend) {
            Bouncer::allow($friend)->to('like-dislike', $post);
            Bouncer::allow($friend)->to('view', $post);
        });
    }

    /**
     * Detach ability from the user follower (friend).
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $friend
     * @return void
     */
    public function detachPostsAbility(Models\User $user, Models\User $friend)
    {
        $user->posts->each(function ($post) use ($friend) {
            Bouncer::disallow($friend)->to('like-dislike', $post);
            Bouncer::disallow($friend)->to('view', $post);
        });
    }

    /**
     * Store user photo to the storage.
     *
     * @param  mixed  $media
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function storeMedia()
    {
        $args = func_get_args();
        $numArgs = func_num_args();
        $this->checkParameters($numArgs, 2);

        $media = $args[0];
        $user = $args[1];

        if ($location = $media->store('media/' . $user->id . '/profile', 'public')) {
            $this->compressMedia($media, $location);

            $user->photo_url = $location;
            $user->save();

            return true;
        }

        return false;
    }

    /**
     * Activated the user by the given token.
     *
     * @param  string  $token
     * @return void
     */
    public function activateByToken($token)
    {
        if (! is_null($token)) {
            $user = Models\User::whereActivationToken($token)->firstOrFail();

            $user->forceFill([
                'activation_token' => null,
                'status' => Models\User::statuses()->get('active'),
            ])->save();
        }
    }

    /**
     * Generate activation token.
     *
     * @return string
     */
    public function generateActivationToken()
    {
        $token = Keygen::length(40)->alphanum()->generate();

        while (Models\User::whereActivationToken($token)->count() > 0) {
            $token = Keygen::length(40)->alphanum()->generate();
        }

        return $token;
    }
}
