<?php

namespace App\Services\Implementations;

use App\Models;
use App\Services\PostService;
use Illuminate\Support\Facades\Storage;
use Silber\Bouncer\BouncerFacade as Bouncer;

class PostServiceImplementation extends BaseService implements PostService
{
    /**
     * Attach ability for post to the appropriate user.
     *
     * @param  \App\Models\Post  $post
     * @param  \App\Models\User  $user
     * @return void
     */
    public function attachAbility(Models\Post $post, Models\User $user)
    {
        if (! $user->isSuperAdmin()) {
            // Attach ability to the Administrator role.
            Bouncer::allow('Administrator')->toManage($post);
        }

        // Attach ability to the user.
        Bouncer::allow($user)->toManage($post);

        // Attach ability to the user followers.
        $user->followers->each(function ($user) use ($post) {
            Bouncer::allow($user)->to('like-dislike', $post);
            Bouncer::allow($user)->to('view', $post);
        });
    }

    /**
     * Detach ability for post from the appropriate user.
     *
     * @param  \App\Models\Post  $post
     * @param  \App\Models\User  $user
     * @return void
     */
    public function detachAbility(Models\Post $post, Models\User $user)
    {
        if (! $user->isSuperAdmin()) {
            // Detach ability from the Administrator role.
            Bouncer::disallow('Administrator')->toManage($post);
        }

        // Detach ability from the user.
        Bouncer::disallow($user)->toManage($post);

        // Detach ability from the user followers.
        $user->followers->each(function ($user) use ($post) {
            Bouncer::disallow($user)->to('like-dislike', $post);
            Bouncer::disallow($user)->to('view', $post);
        });
    }

    /**
     * Store post media to the storage.
     *
     * @param  mixed  $media
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return boolean
     */
    public function storeMedia()
    {
        $args = func_get_args();
        $this->checkParameters($args, 3);

        $media = $args[0];
        $user = $args[1];
        $post = $args[2];

        if ($location = $media->store('media/' . $user->id . '/post', 'public')) {
            if ($file = parent::storeMedia($media, $location, $user)) {
                $post->media()->save($file);

                return true;
            }
        }

        return false;
    }

    /**
     * Destroy post media from the storage.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function destroyMedia(Models\Post $post)
    {
        if (! is_null($media = $post->media)) {
            Storage::disk('public')->delete($media->location);

            $media->delete();
        }
    }
}
