<?php

namespace App\Services\Implementations;

use App\Models;
use Illuminate\Support\Collection;
use App\Services\EventCategoryService;

class EventCategoryServiceImplementation extends BaseService implements EventCategoryService
{
    /**
     * Preprocessing the event category with options params.
     *
     * @param  mixed  $model
     * @param  \Illuminate\Support\Collection  $options
     * @param  boolean  $isSingleRecord
     * @param  array  $includes
     * @param  boolean  $paginate
     * @return mixed
     */
    protected function preprocessing(
        $model,
        Collection $options,
        $isSingleRecord = false,
        array $includes = [],
        $paginate = true
    ) {
        $eventsRelation = array_prefix(Models\Event::shouldLoads(), 'events');

        $include = $this->processIncludes($options);
        $except = $this->parseParam('except', $options, ',');
        $sorts = $this->processSort($options);

        foreach ($include as $item) {
            switch (true) {
                case $item === 'events':
                    $item = $eventsRelation;
                    break;
                case $item === 'events.user':
                    $item = array_merge($eventsRelation, [
                        'events.user',
                    ]);
                    break;
                case $item === 'events.sessions.location':
                    $item = array_merge($eventsRelation, [
                        'events.sessions.location',
                    ]);
                    break;
                default:
                    $item = [];
            }

            $includes = array_merge($includes, is_array($item) ? $item : [$item]);
        }

        foreach ($sorts as $sort) {
            $include = [];

            switch ($sort) {
                case 'minimum_ticket_price':
                    $include = $eventsRelation;
                    break;
            }

            $includes = array_merge($includes, $include);
        }

        return parent::preprocessing($model, $options, $isSingleRecord, $includes, $paginate);
    }

    /**
     * Create event category.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @return \App\Models\EventCategory
     */
    public function create(Collection $data)
    {
        //
    }

    /**
     * Get event categories from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  array  $includes
     * @return mixed
     */
    public function getEventCategories(Collection $options, array $includes = [])
    {
        $eventCategories = new Models\EventCategory();

        return $this->preprocessing(
            $eventCategories, $options, false, $includes
        );
    }

    /**
     * Get specific event category from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\EventCategory  $eventCategory
     * @param  array  $includes
     * @return \App\Models\EventCategory
     */
    public function getEventCategory(Collection $options, Models\EventCategory $eventCategory, array $includes = [])
    {
        return $this->preprocessing(
            $eventCategory, $options, true, $includes
        );
    }

    /**
     * Sort the results by minimum ticket price.
     *
     * @param  \Illuminate\Support\Collection|\App\Models\EventCategory  $results
     * @param  array  $option
     * @return void
     */
    public function sortMinimumTicketPrice(&$results, array $option)
    {
        $func = function ($eventCategory) use ($option) {
            $direction = ! isset($option[0]) ? 'asc' : $option[0];

            $eventCategory->setRelation(
                'events',
                $eventCategory->events->sortBy(
                    'minimum_ticket_price',
                    SORT_REGULAR,
                    strtolower($direction) === 'desc'
                )->values()
            );

            return $eventCategory;
        };

        if ($results instanceof Collection) {
            $results->transform(function ($eventCategory) use ($func) {
                return $func($eventCategory);
            });
        } else {
            $results = $func($results);
        }
    }
}
