<?php

namespace App\Services\Implementations;

use App\Models;
use App\Services;
use Carbon\Carbon;
use Keygen\Keygen;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Services\TransactionService;
use Silber\Bouncer\BouncerFacade as Bouncer;
use App\Exceptions\Event\EventTicketNotOnSaleException;
use App\Exceptions\Event\EventTicketOutOfStockException;

class TransactionServiceImplementation extends BaseService implements TransactionService
{
    /**
     * @var \App\Services\TicketService
     */
    protected $ticketService;

    /**
     * @var \App\Services\PaymentService
     */
    protected $paymentService;

    /**
     * The default attributes that should be except-ed from response.
     *
     * @var  array
     */
    protected $defaultExcept = [
        //
    ];

    /**
     * Constructor.
     *
     * @param  \App\Services\TicketService  $ticketService
     * @param  \App\Services\PaymentService  $paymentService
     */
    public function __construct(
        Services\TicketService $ticketService,
        Services\PaymentService $paymentService
    ) {
        $this->ticketService = $ticketService;
        $this->paymentService = $paymentService;
    }

    /**
     * Preprocessing the transaction with options params.
     *
     * @param  mixed  $model
     * @param  \Illuminate\Support\Collection  $options
     * @param  boolean  $isSingleRecord
     * @param  array  $includes
     * @param  boolean  $paginate
     * @return mixed
     */
    protected function preprocessing(
        $model,
        Collection $options,
        $isSingleRecord = false,
        array $includes = [],
        $paginate = true
    ) {
        $include = $this->processIncludes($options);
        $except = $this->parseParam('except', $options, ',');
        $sorts = $this->processSort($options);

        foreach ($include as $item) {
            switch (true) {
                case in_array($item, [
                    'customer',
                    'customer.user',
                    'tickets.eventSessionTicket.eventTicket',
                    'validPayments',
                ]):
                    break;
                case $item === 'tickets':
                    $item = 'tickets.transaction';
                    break;
                case in_array($item, $relations = [
                    'eventSessionTickets',
                    'eventSessionTickets.eventTicket',
                    'eventSessionTickets.eventTicket.ticketsNotCanceled',
                ]):
                    if (! isUserASuperAdminOrAdminOrEO()) {
                        $item = [];
                    } else {
                        $this->removeParentFromDefaultExcept($relations, $item);
                        $item = $this->getDeepestRelation($relations);
                    }
                    break;
                default:
                    $item = [];
            }

            $includes = array_merge($includes, is_array($item) ? $item : [$item]);
        }

        foreach ($sorts as $sort) {
            $include = [];

            switch ($sort) {
                case '':
                    //
                    break;
            }

            $includes = array_merge($includes, $include);
        }

        return parent::preprocessing($model, $options, $isSingleRecord, $includes, $paginate);
    }

    /**
     * Create transaction.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Customer  $customer
     * @return array
     *
     * @throws mixed
     */
    public function create(Collection $data, Models\Customer $customer)
    {
        $transactions = collect();

        $relations = array_merge(
            array_prefix(Models\Event::shouldLoads(), 'eventTicket.event'),
            [
                'eventTicket.event.user',
            ]
        );

        $eventSessionTickets = Models\EventSessionTicket::with($relations)->whereIn(
            'id', $data->pluck('event_session_ticket')
        )->get();

        $outOfStock = $eventSessionTickets->filter(function ($eventSessionTicket) use ($data) {
            $quantity = array_get($data->where('event_session_ticket', $eventSessionTicket->id)
                ->first(), 'quantity', 0);

            return $eventSessionTicket->eventTicket->stock - $quantity < 0;
        });

        $notOnSale = $eventSessionTickets->filter(function ($eventSessionTicket) {
            $now = Carbon::now();
            $from = $eventSessionTicket->eventTicket->from;
            $until = $eventSessionTicket->eventTicket->until;

            return $now->lt($from) || $now->gt($until);
        });

        if (! $outOfStock->isEmpty()) {
            throw new EventTicketOutOfStockException(
                $outOfStock->pluck('id')->toArray()
            );
        }

        if (! $notOnSale->isEmpty()) {
            throw new EventTicketNotOnSaleException(
                $notOnSale->pluck('id')->toArray()
            );
        }

        DB::transaction(function () use ($eventSessionTickets, $data, $customer, $relations, &$transactions) {
            $eventSessionTickets->groupBy('eventTicket.event.user.id')->each(
                function ($eventSessionTickets) use ($data, $customer, $relations, &$transactions) {
                    $pieceIds = collect($eventSessionTickets)->pluck('id');

                    $freeIds = Models\EventSessionTicket::with($relations)->whereIn('id', $pieceIds)
                        ->whereHas('eventTicket', function ($query) {
                            return $query->free();
                        })->get()->pluck('id');

                    $free = $data->whereIn('event_session_ticket', $freeIds);
                    $paid = $data->whereNotIn('event_session_ticket', $freeIds);

                    // Free
                    if (!$free->isEmpty()) {
                        $transaction = $this->createOnce($free, $customer);

                        // Automatically create the payments when the transaction is free,
                        // it means the total == 0.
                        $this->paymentService->create(collect([
                            'is_verified' => true,
                        ]), $transaction);

                        $transactions->push($transaction);
                    }

                    // Paid
                    if (!$paid->isEmpty()) {
                        $transaction = $this->createOnce($paid, $customer);

                        $transactions->push($transaction);
                    }
                }
            );
        });

        return $this->preprocessing(
            $transactions, collect(), false, [], false
        );
    }

    /**
     * Create transaction once.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Customer  $customer
     * @return \App\Models\Transaction
     */
    public function createOnce(Collection $data, Models\Customer $customer)
    {
        $transaction = new Models\Transaction();
        $data = $data->unique('event_session_ticket');

        $transaction->forceFill([
            'code' => $this->generateCode(),
            'name' => $this->generateName($customer),
        ]);

        $customer->transactions()->save($transaction);

        $data->each(function ($item) use ($transaction) {
            foreach (range(1, (int)$item['quantity']) as $index) {
                $this->ticketService->create(collect([
                    'event_session_ticket' => $item['event_session_ticket'],
                ]), $transaction);
            }
        });

        return $transaction;
    }

    /**
     * Get transactions from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getTransactions(Collection $options, $model = null, array $includes = [])
    {
        if (is_null($model)) {
            $transactions = new Models\Transaction();
        } else {
            $transactions = $model->transactions();
        }

        return $this->preprocessing(
            $transactions, $options
        );
    }

    /**
     * Get specific transaction from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Transaction  $transaction
     * @param  array  $includes
     * @return \App\Models\Transaction
     */
    public function getTransaction(Collection $options, Models\Transaction $transaction, array $includes = [])
    {
        return $this->preprocessing(
            $transaction, $options, true
        );
    }

    /**
     * Attach ability for transaction to the appropriate customer.
     *
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function attachAbility(Models\Transaction $transaction, Models\Customer $customer)
    {
        // Attach ability to the Administrator role.
        Bouncer::allow('Administrator')->toManage($transaction);

        // Attach ability to the customer.
        Bouncer::allow($customer->user)->toManage($transaction);
    }

    /**
     * Detach ability for transaction from the appropriate customer.
     *
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function detachAbility(Models\Transaction $transaction, Models\Customer $customer)
    {
        // Detach ability from the Administrator role.
        Bouncer::disallow('Administrator')->toManage($transaction);

        // Detach ability from the customer.
        Bouncer::disallow($customer->user)->toManage($transaction);
    }

    /**
     * Generate transaction code.
     *
     * @return string
     */
    public function generateCode()
    {
        $code = Keygen::length(60)->alphanum()->generate();

        while (Models\Transaction::whereCode($code)->count() > 0) {
            $code = Keygen::length(60)->alphanum()->generate();
        }

        return $code;
    }

    /**
     * Generate transaction name.
     *
     * @param  \App\Models\Customer  $customer
     * @return string
     */
    public function generateName(Models\Customer $customer)
    {
        $generate = function () use ($customer) {
            return sprintf(
                '%s/%d/%d/%d',
                'TRX/RAMEIN',
                $customer->id,
                Carbon::now()->timestamp,
                Keygen::length(5)->numeric()->generate()
            );
        };

        $name = $generate();

        while (Models\Transaction::whereName($name)->count() > 0) {
            $name = $generate();
        }

        return $name;
    }
}
