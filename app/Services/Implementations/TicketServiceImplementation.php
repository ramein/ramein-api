<?php

namespace App\Services\Implementations;

use App\Models;
use Carbon\Carbon;
use Keygen\Keygen;
use App\Services\TicketService;
use Illuminate\Support\Collection;
use Silber\Bouncer\BouncerFacade as Bouncer;
use App\Exceptions\Ticket\TicketNotValidException;

class TicketServiceImplementation extends BaseService implements TicketService
{
    /**
     * The default relations that should be eager loaded.
     *
     * @var  array
     */
    protected $defaultIncludes = [
        'transaction',
        'eventSessionTicket.eventTicket',
    ];

    /**
     * The default attributes that should be except-ed from response.
     *
     * @var  array
     */
    protected $defaultExcept = [
        'transaction.event_session_tickets',
        'transaction.event_session_tickets.event_ticket.tickets_not_canceled',
    ];

    /**
     * Preprocessing the ticket with options params.
     *
     * @param  mixed  $model
     * @param  \Illuminate\Support\Collection  $options
     * @param  boolean  $isSingleRecord
     * @param  array  $includes
     * @param  boolean  $paginate
     * @return mixed
     */
    protected function preprocessing(
        $model,
        Collection $options,
        $isSingleRecord = false,
        array $includes = [],
        $paginate = true
    ) {
        $this->mergeWithDefaultIncludes([
            $includes,
            Models\Ticket::shouldLoads(),
        ]);

        $include = $this->processIncludes($options);
        $except = $this->parseParam('except', $options, ',');
        $sorts = $this->processSort($options);

        foreach ($include as $item) {
            switch (true) {
                case in_array($item, [
                    'customer',
                    'customer.user',
                    'eventSessionTicket',
                    'eventSessionTicket.eventTicket',
                    'eventSessionTicket.eventSession',
                    'eventSessionTicket.eventTicket.event',
                    'eventSessionTicket.eventSession.event',
                ]):
                    break;
                case $item === 'transaction':
                    if (isUserASuperAdminOrAdmin()) {
                        $this->removeFromDefaultExcept([
                            'transaction.event_session_tickets.event_ticket.tickets_not_canceled',
                        ]);
                    }
                    break;
                case $item === 'transaction.eventSessionTickets':
                    $this->removeFromDefault([
                        'transaction.event_session_tickets',
                    ], $this->defaultExcept);
                    break;
                case $item === 'eventSessionTicket.eventTicket':
                    if (! isUserASuperAdminOrAdmin()) {
                        $this->mergeWithDefaultExcept([
                            'event_session_ticket.event_ticket.tickets_not_canceled',
                        ]);
                    }
                    break;
                default:
                    $item = [];
            }

            $includes = array_merge($includes, is_array($item) ? $item : [$item]);
        }

        foreach ($sorts as $sort) {
            $include = [];

            switch ($sort) {
                case '':
                    //
                    break;
            }

            $includes = array_merge($includes, $include);
        }

        return parent::preprocessing($model, $options, $isSingleRecord, $includes, $paginate);
    }

    /**
     * Create ticket.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Transaction  $transaction
     * @return \App\Models\Ticket
     */
    public function create(Collection $data, Models\Transaction $transaction)
    {
        $ticket = new Models\Ticket();

        $ticket->forceFill([
            'code' => $this->generateCode(),
            'event_session_ticket_id' => $data->get('event_session_ticket'),
        ]);

        $transaction->tickets()->save($ticket);

        // After we created the ticket, then we set the ticket validity.
        $eventSession = $ticket->eventSessionTicket->eventSession;

        $ticket->forceFill([
            'valid_from' => Carbon::parse($eventSession->from)->subDays(Models\EventSession::$ticketValidLength),
            'valid_until' => Carbon::parse($eventSession->until)->addDays(Models\EventSession::$ticketValidLength),
        ])->save();

        return $ticket;
    }

    /**
     * Get tickets from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getTickets(Collection $options, $model = null, array $includes = [])
    {
        if (is_null($model)) {
            $tickets = new Models\Ticket();
        } else {
            $tickets = $model->tickets();
        }

        return $this->preprocessing(
            $tickets, $options, false, $includes
        );
    }

    /**
     * Get specific ticket from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Ticket  $ticket
     * @param  array  $includes
     * @return \App\Models\Ticket
     */
    public function getTicket(Collection $options, Models\Ticket $ticket, array $includes = [])
    {
        return $this->preprocessing(
            $ticket, $options, true, $includes
        );
    }

    /**
     * Handle the ticket scanning.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \App\Models\Ticket
     *
     * @throws \App\Exceptions\Ticket\TicketNotValidException
     */
    public function scanning(Models\Ticket $ticket)
    {
        if (Carbon::now()->lt($ticket->valid_from) || Carbon::now()->gt($ticket->valid_until)) {
            throw new TicketNotValidException(sprintf("%s '%s' until '%s'",
                'The ticket is invalid. Ticket valid from',
                $ticket->valid_from->toDateTimeString(),
                $ticket->valid_until->toDateTimeString()
            ));
        }

        return $this->markTicketAsScanned($ticket);
    }

    /**
     * Mark the ticket as scanned.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \App\Models\Ticket
     */
    public function markTicketAsScanned(Models\Ticket $ticket)
    {
        $ticket->forceFill([
            'is_scanned' => true,
            'scanned_on' => Carbon::now(),
        ])->save();

        return $ticket;
    }

    /**
     * Attach ability for ticket to the appropriate customer.
     *
     * @param  \App\Models\Ticket  $ticket
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function attachAbility(Models\Ticket $ticket, Models\Customer $customer)
    {
        // Attach ability to the Administrator role.
        Bouncer::allow('Administrator')->toManage($ticket);

        // Attach ability to the customer.
        Bouncer::allow($customer->user)->toManage($ticket);
    }

    /**
     * Detach ability for ticket from the appropriate customer.
     *
     * @param  \App\Models\Ticket  $ticket
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function detachAbility(Models\Ticket $ticket, Models\Customer $customer)
    {
        // Detach ability from the Administrator role.
        Bouncer::disallow('Administrator')->toManage($ticket);

        // Detach ability from the customer.
        Bouncer::disallow($customer->user)->toManage($ticket);
    }

    /**
     * Generate ticket code.
     *
     * @return string
     */
    public function generateCode()
    {
        $code = Keygen::length(60)->alphanum()->generate();

        while (Models\Ticket::whereCode($code)->count() > 0) {
            $code = Keygen::length(60)->alphanum()->generate();
        }

        return $code;
    }
}
