<?php

namespace App\Services;

use App\Models;

interface PostService
{
    /**
     * Attach ability for post to the appropriate user.
     *
     * @param  \App\Models\Post  $post
     * @param  \App\Models\User  $user
     * @return void
     */
    public function attachAbility(Models\Post $post, Models\User $user);

    /**
     * Detach ability for post from the appropriate user.
     *
     * @param  \App\Models\Post  $post
     * @param  \App\Models\User  $user
     * @return void
     */
    public function detachAbility(Models\Post $post, Models\User $user);

    /**
     * Store post media to the storage.
     *
     * @param  mixed  $media
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return boolean
     */
    public function storeMedia();

    /**
     * Destroy post media from the storage.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function destroyMedia(Models\Post $post);
}
