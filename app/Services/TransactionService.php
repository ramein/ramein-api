<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Collection;

interface TransactionService
{
    /**
     * Create transaction.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Customer  $customer
     * @return \App\Models\Transaction
     */
    public function create(Collection $data, Models\Customer $customer);

    /**
     * Create transaction once.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Customer  $customer
     * @return \App\Models\Transaction
     */
    public function createOnce(Collection $data, Models\Customer $customer);

    /**
     * Get transactions from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getTransactions(Collection $options, $model = null, array $includes = []);

    /**
     * Get specific transaction from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Transaction  $transaction
     * @param  array  $includes
     * @return \App\Models\Transaction
     */
    public function getTransaction(Collection $options, Models\Transaction $transaction, array $includes = []);

    /**
     * Attach ability for transaction to the appropriate customer.
     *
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function attachAbility(Models\Transaction $transaction, Models\Customer $customer);

    /**
     * Detach ability for transaction from the appropriate customer.
     *
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function detachAbility(Models\Transaction $transaction, Models\Customer $customer);

    /**
     * Generate transaction code.
     *
     * @return string
     */
    public function generateCode();
}
