<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Collection;

interface TicketService
{
    /**
     * Create ticket.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Transaction  $transaction
     * @return \App\Models\Ticket
     */
    public function create(Collection $data, Models\Transaction $transaction);

    /**
     * Get tickets from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Customer|\App\Models\Transaction  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getTickets(Collection $options, $model = null, array $includes = []);

    /**
     * Get specific ticket from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Ticket  $ticket
     * @param  array  $includes
     * @return \App\Models\Ticket
     */
    public function getTicket(Collection $options, Models\Ticket $ticket, array $includes = []);

    /**
     * Handle the ticket scanning.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \App\Models\Ticket
     */
    public function scanning(Models\Ticket $ticket);

    /**
     * Mark the ticket as scanned.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \App\Models\Ticket
     */
    public function markTicketAsScanned(Models\Ticket $ticket);

    /**
     * Attach ability for ticket to the appropriate customer.
     *
     * @param  \App\Models\Ticket  $ticket
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function attachAbility(Models\Ticket $ticket, Models\Customer $customer);

    /**
     * Detach ability for ticket from the appropriate customer.
     *
     * @param  \App\Models\Ticket  $ticket
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function detachAbility(Models\Ticket $ticket, Models\Customer $customer);

    /**
     * Generate ticket code.
     *
     * @return string
     */
    public function generateCode();
}
