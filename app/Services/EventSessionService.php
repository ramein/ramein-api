<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Collection;

interface EventSessionService
{
    /**
     * Create all event session resources.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Event  $event
     * @return \App\Models\EventSession
     */
    public function createAll(Collection $data, Models\Event $event);

    /**
     * Update all event session resources.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\EventSession  $eventSession
     * @return \App\Models\EventSession
     */
    public function updateAll(Collection $data, Models\EventSession $eventSession);

    /**
     * Create event session.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Event  $event
     * @return \App\Models\EventSession
     */
    public function create(Collection $data, Models\Event $event);

    /**
     * Update event session.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\EventSession  $eventSession
     * @return \App\Models\EventSession
     */
    public function update(Collection $data, Models\EventSession $eventSession);

    /**
     * Create event session ticket.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\EventSession  $eventSession
     * @return \App\Models\EventTicket
     */
    public function createEventTicket(Collection $data, Models\EventSession $eventSession);

    /**
     * Update event session ticket.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\EventTicket  $eventTicket
     * @return \App\Models\EventTicket
     */
    public function updateEventTicket(Collection $data, Models\EventTicket $eventTicket);

    /**
     * Create event session question.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\EventSession  $eventSession
     * @return \App\Models\EventSessionQuestion
     */
    public function createQuestion(Collection $data, Models\EventSession $eventSession);

    /**
     * Update event session question.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\EventSessionQuestion  $eventSessionQuestion
     * @return \App\Models\EventSessionQuestion
     */
    public function updateQuestion(Collection $data, Models\EventSessionQuestion $eventSessionQuestion);

    /**
     * Get event sessions from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getEventSessions(Collection $options, $model = null, array $includes = []);

    /**
     * Get specific event session from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\EventSession  $eventSession
     * @param  array  $includes
     * @return \App\Models\EventSession
     */
    public function getEventSession(Collection $options, Models\EventSession $eventSession, array $includes = []);
}
