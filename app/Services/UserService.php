<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Collection;

interface UserService
{
    /**
     * Create user.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @return \App\Models\User
     */
    public function create(Collection $data);

    /**
     * Update user.
     *
     * @param  \App\Models\User  $user
     * @param  \Illuminate\Support\Collection  $data
     * @return \App\Models\User
     */
    public function update(Models\User $user, Collection $data);

    /**
     * Get users from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $data
     * @return mixed
     */
    public function getUsers(Collection $options, $data);

    /**
     * Get specific user from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\User  $user
     * @return \App\Models\User
     */
    public function getUser(Collection $options, Models\User $user);

    /**
     * Attach ability to the appropriate user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function attachAbility(Models\User $user);

    /**
     * Detach ability from the appropriate user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function detachAbility(Models\User $user);

    /**
     * Assign Super Administrator role to the appropriate user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function assignSuperAdminRole(Models\User $user);

    /**
     * Give user follower (friend) ability to all posts.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $friend
     * @return void
     */
    public function attachPostsAbility(Models\User $user, Models\User $friend);

    /**
     * Detach ability from the user follower (friend).
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $friend
     * @return void
     */
    public function detachPostsAbility(Models\User $user, Models\User $friend);

    /**
     * Store user photo to the storage.
     *
     * @param  mixed  $media
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function storeMedia();

    /**
     * Activated the user by the given token.
     *
     * @param  string  $token
     * @return void
     */
    public function activateByToken($token);

    /**
     * Generate activation token.
     *
     * @return string
     */
    public function generateActivationToken();
}
