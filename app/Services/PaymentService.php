<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Collection;

interface PaymentService
{
    /**
     * Create payment.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Transaction  $transaction
     * @return \App\Models\Payment
     */
    public function create(Collection $data, Models\Transaction $transaction);

    /**
     * Get payments from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getPayments(Collection $options, $model = null, array $includes = []);

    /**
     * Get specific payment from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Payment  $payment
     * @param  array  $includes
     * @return \App\Models\Payment
     */
    public function getPayment(Collection $options, Models\Payment $payment, array $includes = []);

    /**
     * Attach ability for payment to the appropriate customer.
     *
     * @param  \App\Models\Payment  $payment
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function attachAbility(Models\Payment $payment, Models\Customer $customer);

    /**
     * Detach ability for payment from the appropriate customer.
     *
     * @param  \App\Models\Payment  $payment
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function detachAbility(Models\Payment $payment, Models\Customer $customer);

    /**
     * Generate payment code.
     *
     * @return string
     */
    public function generateCode();
}
