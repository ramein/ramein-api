<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Collection;

interface EventCategoryService
{
    /**
     * Create event category.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @return \App\Models\EventCategory
     */
    public function create(Collection $data);

    /**
     * Get event categories from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  array  $includes
     * @return mixed
     */
    public function getEventCategories(Collection $options, array $includes = []);

    /**
     * Get specific event category from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\EventCategory  $eventCategory
     * @param  array  $includes
     * @return \App\Models\EventCategory
     */
    public function getEventCategory(Collection $options, Models\EventCategory $eventCategory, array $includes = []);
}
