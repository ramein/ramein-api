<?php

namespace App\Services;

use App\Models;
use Illuminate\Support\Collection;

interface EventService
{
    /**
     * Create all event resources.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\User  $user
     * @return \App\Models\Event
     */
    public function createAll(Collection $data, Models\User $user);

    /**
     * Update all event resources.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Event  $event
     * @return \App\Models\Event
     */
    public function updateAll(Collection $data, Models\Event $event);

    /**
     * Create event.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\User  $user
     * @return \App\Models\Event
     */
    public function create(Collection $data, Models\User $user);

    /**
     * Update event.
     *
     * @param  \Illuminate\Support\Collection  $data
     * @param  \App\Models\Event  $event
     * @return \App\Models\Event
     */
    public function update(Collection $data, Models\Event $event);

    /**
     * Get events from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  mixed  $model
     * @param  array  $includes
     * @return mixed
     */
    public function getEvents(Collection $options, $model = null, array $includes = []);

    /**
     * Get specific event from database.
     *
     * @param  \Illuminate\Support\Collection  $options
     * @param  \App\Models\Event  $event
     * @param  array  $includes
     * @return \App\Models\Event
     */
    public function getEvent(Collection $options, Models\Event $event, array $includes = []);

    /**
     * Attach ability for event to the appropriate user.
     *
     * @param  \App\Models\Event  $event
     * @param  \App\Models\User  $user
     * @return void
     */
    public function attachAbility(Models\Event $event, Models\User $user);

    /**
     * Detach ability for event from the appropriate user.
     *
     * @param  \App\Models\Event  $event
     * @param  \App\Models\User  $user
     * @return void
     */
    public function detachAbility(Models\Event $event, Models\User $user);

    /**
     * Store event poster to the storage.
     *
     * @param  mixed  $media
     * @param  \App\Models\User  $user
     * @param  \App\Models\Event  $event
     * @return boolean
     */
    public function storePoster($media, Models\User $user, Models\Event $event);

    /**
     * Update event poster in the storage.
     *
     * @param  mixed  $media
     * @param  \App\Models\User  $user
     * @param  \App\Models\Event  $event
     * @return boolean
     */
    public function updatePoster($media, Models\User $user, Models\Event $event);

    /**
     * Destroy event poster from the storage.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function destroyPoster(Models\Event $event);

    /**
     * Generate event code.
     *
     * @return string
     */
    public function generateCode();
}
