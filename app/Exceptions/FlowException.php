<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class FlowException extends Exception
{
    /**
     * The recommended response to send to the client.
     *
     * @var \Symfony\Component\HttpFoundation\Response|null
     */
    public $response;

    /**
     * Create a new exception instance.
     *
     * @param  array  $message
     */
    public function __construct(array $message)
    {
        parent::__construct('The given data can\'t be processed.');

        $this->response = new JsonResponse($message, 422);
    }

    /**
     * Get the underlying response instance.
     *
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
}
