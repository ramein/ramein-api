<?php

namespace App\Exceptions\Event;

use Exception;
use Illuminate\Http\JsonResponse;

class EventTicketOutOfStockException extends Exception
{
    /**
     * The recommended response to send to the client.
     *
     * @var \Symfony\Component\HttpFoundation\Response|null
     */
    public $response;

    /**
     * Create a new exception instance.
     *
     * @param  array  $eventSessionTickets
     */
    public function __construct(array $eventSessionTickets)
    {
        parent::__construct('Some of Event Ticket is out of stock.');

        $errors = [];
        $eventSessionTickets = array_unique($eventSessionTickets);

        foreach ($eventSessionTickets as $eventSessionTicket) {
            $errors[] = [
                'event_session_ticket' => $eventSessionTicket,
                'message' => 'This event ticket is out of stock.',
            ];
        }

        $this->response = new JsonResponse([
            'out_of_stock' => $errors,
        ], 422);
    }

    /**
     * Get the underlying response instance.
     *
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
}
