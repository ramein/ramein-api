<?php

namespace App\Exceptions;

use Exception;
use App\Exceptions\Event;
use App\Exceptions\Ticket;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        FlowException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        switch (true) {
            case $e instanceof AuthorizationException:
                return response()->json(['message' => 'This action is unauthorized.'], 403);
            case $e instanceof ModelNotFoundException:
                return response()->json(['message' => 'The data with the given key not found in our database.'], 404);
            case (
                $e instanceof FlowException ||
                $e instanceof InvalidParameterException ||
                $e instanceof Ticket\TicketNotValidException ||
                $e instanceof Event\EventTicketNotOnSaleException ||
                $e instanceof Event\EventTicketOutOfStockException
            ) && $e->getResponse():
                return $e->getResponse();
            case $e instanceof HttpException:
                $message = $e->getMessage();
                $message = is_string($message) && $message === '' ? null : $message;

                if (is_null($message)) {
                    switch ($e->getStatusCode()) {
                        case 403:
                            $message = 'You don\'t have permission to access this endpoint!';
                            break;
                        case 404:
                            $message = 'Endpoint not found!';
                            break;
                        case 405:
                            $message = 'Method not allowed!';
                            break;
                        default:
                            $message = 'Something went wrong!';
                            break;
                    }
                }

                return response()->json(['message' => $message], $e->getStatusCode());
        }

        return parent::render($request, $e);
    }
}
