<?php

namespace App\Exceptions\Ticket;

use Exception;
use Illuminate\Http\JsonResponse;

class TicketNotValidException extends Exception
{
    /**
     * The recommended response to send to the client.
     *
     * @var \Symfony\Component\HttpFoundation\Response|null
     */
    public $response;

    /**
     * Create a new exception instance.
     *
     * @param  string  $message
     */
    public function __construct($message)
    {
        parent::__construct('The ticket is invalid.');

        $this->response = new JsonResponse([
            'message' => $message,
        ], 422);
    }

    /**
     * Get the underlying response instance.
     *
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
}
