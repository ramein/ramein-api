<?php

if (! function_exists('public_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function public_path($path = '')
    {
        return base_path('public').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (! function_exists('url_front')) {
    /**
     * Generate a url for front url.
     *
     * @param  string  $path
     * @param  mixed   $parameters
     * @param  bool    $secure
     * @return string
     */
    function url_front($path = null, $parameters = [], $secure = null)
    {
        app('url')->forceCachedSchemeAndRoot(config('app.front_url'));

        $url = url($path, $parameters, $secure);

        app('url')->forceCachedSchemeAndRoot(config('app.url'));

        return $url;
    }
}

if (! function_exists('config_path')) {
    /**
     * Get the path to the config folder.
     *
     * @param  string  $path
     * @return string
     */
    function config_path($path = '')
    {
        return base_path('config').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (! function_exists('app_path')) {
    /**
     * Get the path to the app folder.
     *
     * @param  string  $path
     * @return string
     */
    function app_path($path = '')
    {
        return base_path('app').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (! function_exists('bcrypt')) {
    /**
     * Hash the given value.
     *
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    function bcrypt($value, $options = [])
    {
        return app('hash')->make($value, $options);
    }
}

if (! function_exists('abort_unless')) {
    /**
     * Throw an HttpException with the given data unless the given condition is true.
     *
     * @param  bool    $boolean
     * @param  int     $code
     * @param  string  $message
     * @param  array   $headers
     * @return void
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    function abort_unless($boolean, $code, $message = '', array $headers = [])
    {
        if (! $boolean) {
            abort($code, $message, $headers);
        }
    }
}


if (! function_exists('rest')) {
    /**
     * Lumen RESTful route.
     *
     * @param  string  $path
     * @param  string  $controller
     * @param  string  $binding
     * @param  string  $namespace
     * @param  array  $options
     * @return void
     */
    function rest(&$app, $path, $controller, $binding = 'id', $namespace = '', array $options = ['*'])
    {
        if (in_array('*', $options)
            || (isset($options['except']) && ! in_array('all', $options['except']))
            || (isset($options['only']) && in_array('all', $options['only']))
        ) {
            $app->get($path, $namespace.'\\'.$controller.'@all');
        }

        if (in_array('*', $options)
            || (isset($options['except']) && ! in_array('get', $options['except']))
            || (isset($options['only']) && in_array('get', $options['only']))
        ) {
            $app->get($path.'/{'.$binding.'}', $namespace.'\\'.$controller.'@get');
        }

        if (in_array('*', $options)
            || (isset($options['except']) && ! in_array('store', $options['except']))
            || (isset($options['only']) && in_array('store', $options['only']))
        ) {
            $app->post($path, $namespace.'\\'.$controller.'@store');
        }

        if (in_array('*', $options)
            || (isset($options['except']) && ! in_array('update', $options['except']))
            || (isset($options['only']) && in_array('update', $options['only']))
        ) {
            $app->put($path.'/{'.$binding.'}', $namespace.'\\'.$controller.'@update');
        }

        if (in_array('*', $options)
            || (isset($options['except']) && ! in_array('destroy', $options['except']))
            || (isset($options['only']) && in_array('destroy', $options['only']))
        ) {
            $app->delete($path.'/{'.$binding.'}', $namespace.'\\'.$controller.'@destroy');
        }
    }
}

if (! function_exists('paginate')) {
    /**
     * Create a paginator for collection.
     *
     * @param  \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection  $data
     * @param  integer  $perPage
     * @param  string  $path
     * @param  string  $pageName
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    function paginate($data, $perPage = 10, $path = null, $pageName = 'page')
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageResults = $data->slice(($currentPage-1) * $perPage, $perPage)->values();

        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageResults,
            $data->count(),
            (int)$perPage,
            $currentPage
        );
        $path = $path ?: \Illuminate\Pagination\Paginator::resolveCurrentPath();

        $paginator->setPageName($pageName);
        $paginator->setPath($path);
        $paginator->appends($_GET);

        return $paginator;
    }
}

if (! function_exists('array_prefix')) {
    /**
     * Prefixed the given array
     *
     * @param  array  $data
     * @param  string  $prefix
     * @return array
     */
    function array_prefix($data, $prefix = null)
    {
        foreach ($data as &$item) {
            $item = $prefix ? $prefix . '.' . $item : $item;
        }

        return $data;
    }
}

if (! function_exists('auth')) {
    /**
     * Get the available auth instance.
     *
     * @param  string|null  $guard
     * @return \Illuminate\Contracts\Auth\Factory|\Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    function auth($guard = null)
    {
        if (is_null($guard)) {
            return app(\Illuminate\Contracts\Auth\Factory::class);
        } else {
            return app(\Illuminate\Contracts\Auth\Factory::class)->guard($guard);
        }
    }
}

if (! function_exists('request')) {
    /**
     * Get an instance of the current request or an input item from the request.
     *
     * @param  array|string  $key
     * @param  mixed   $default
     * @return \Illuminate\Http\Request|string|array
     */
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('request');
        }

        if (is_array($key)) {
            return app('request')->only($key);
        }

        return data_get(app('request')->all(), $key, $default);
    }
}

if (! function_exists('updateDotEnv')) {
    /**
     * Update the .env value based on the given data.
     *
     * @param  array   $data
     * @return boolean
     */
    function updateDotEnv($data = [])
    {
        if (count($data) > 0) {
            $env = file_get_contents(base_path() . '/.env');
            $env = explode(PHP_EOL, $env);

            foreach ($data as $key => $value) {
                foreach ($env as $env_key => $env_value) {
                    $entry = explode("=", $env_value, 2);
                    if ($entry[0] == $key) {
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        $env[$env_key] = $env_value;
                    }
                }
            }

            $env = implode("\n", $env);
            file_put_contents(base_path() . '/.env', $env);

            return true;
        }

        return false;
    }
}

if (! function_exists('array_unset_value')) {
    /**
     * Unset given key from array.
     *
     * @param  array  $array
     * @param  mixed  $parents
     * @param  string  $glue
     * @return void
     */
    function array_unset_value(array &$array, $parents, $glue = '.')
    {
        if (!is_array($parents)) {
            $parents = array_filter(explode($glue, $parents), 'strlen');
        }

        $key = array_shift($parents);

        if (empty($parents)) {
            unset($array[$key]);
        } elseif (is_array($array) && array_key_exists($key, $array)) {
            if (!is_array($array[$key])) {
                array_unset_value($array[$key], $parents);
            } elseif (array_key_exists(0, $array[$key])) {
                foreach ($array[$key] as &$arr) {
                    $arrr = isset($arr[$key]) && is_array($arr[$key]) ? $arr[$key] : $arr;
                    array_unset_value($arrr, $parents);
                    $arr = $arrr;
                }
            } else {
                array_unset_value($array[$key], $parents);
            }
        }
    }
}

if (! function_exists('array_transform_value')) {
    /**
     * Transform all values in an array to the given callback.
     *
     * @param  array  $array
     * @param  mixed  $callback
     * @return array
     */
    function array_transform_value(array $array, $callback)
    {
        foreach ($array as $key => &$item) {
            if (is_array($item)) {
                $item = array_transform_value($item, $callback);
            } elseif (is_string($item)) {
                $item = $callback($item);
            }
        }

        return $array;
    }
}
