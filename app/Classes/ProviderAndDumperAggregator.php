<?php

namespace App\Classes;

use Geocoder\Laravel\ProviderAndDumperAggregator as BaseClass;

class ProviderAndDumperAggregator extends BaseClass
{
    protected function removeEmptyCacheEntry(string $cacheKey)
    {
        $result = app('cache')->get($cacheKey);

        if ($result && $result->isEmpty()) {
            // app('cache')->forget($cacheKey);
        }
    }
}
