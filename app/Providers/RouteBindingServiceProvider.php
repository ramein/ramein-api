<?php

namespace App\Providers;

use App\Models;
use mmghv\LumenRouteBinding\RouteBindingServiceProvider as ServiceProvider;

class RouteBindingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $binder = $this->binder;

        // Here we define all the route model bindings
        $binder->bind('user', Models\User::class);
        $binder->bind('customer', Models\Customer::class);
        $binder->bind('friend', Models\User::class);
        $binder->bind('tag', Models\Tag::class);
        $binder->bind('post', Models\Post::class);
        $binder->bind('event', Models\Event::class);
        $binder->bind('eventCategory', Models\EventCategory::class);
        $binder->bind('eventType', Models\EventType::class);
        $binder->bind('eventSession', Models\EventSession::class);
        $binder->bind('transaction', Models\Transaction::class);
        $binder->bind('payment', Models\Payment::class);
        $binder->bind('ticket', Models\Ticket::class);
    }
}
