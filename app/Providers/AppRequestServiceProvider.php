<?php

namespace App\Providers;

use App\Http\Requests\Request;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class AppRequestServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $request= Request::capture();

        $this->app->instance(Request::class, $this->prepareRequest($request, $request->route()));
    }

    /**
     * Prepare the given request instance for use with the application.
     *
     * @param  \Symfony\Component\HttpFoundation\Request  $request
     * @param  array  $routeInfo
     * @return \App\Http\Requests\Request
     */
    protected function prepareRequest(SymfonyRequest $request, $routeInfo)
    {
        if (! $request instanceof Request) {
            $request = Request::createFromBase($request);
        }

        $request->setUserResolver(function ($guard = null) {
            return $this->app->make('auth')->guard($guard)->user();
        })->setRouteResolver(function () use ($routeInfo) {
            return $routeInfo;
        });

        return $request;
    }
}
