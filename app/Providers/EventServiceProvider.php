<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\User\UserCreated' => [
            'App\Listeners\User\SendActivationEmail',
        ],

        'App\Events\Transaction\TransactionPaid' => [
            'App\Listeners\Transaction\SendTransactionPaidEmail',
        ],
    ];
}
