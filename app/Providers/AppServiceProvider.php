<?php

namespace App\Providers;

use App\Models;
use App\Services;
use App\Observers;
use Carbon\Carbon;
use Illuminate\Support;
use Silber\Bouncer\Bouncer;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\Mail;
use App\Illuminate\Routing\UrlGenerator;
use Dusterio\LumenPassport\LumenPassport;
use App\Classes\ProviderAndDumperAggregator;

class AppServiceProvider extends Support\ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRouting();
        $this->configureMailer();
        $this->registerRoles();
        $this->configureGoogleMaps();
        $this->registerEloquentObservers();
        $this->registerCustomValidations();

        LumenPassport::routes($this->app);
        Passport::tokensExpireIn(Carbon::now()->addYears(1));
        Passport::refreshTokensExpireIn(Carbon::now()->addYears(1));
        LumenPassport::allowMultipleTokens();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            Services\PostService::class,
            Services\Implementations\PostServiceImplementation::class
        );
        $this->app->bind(
            Services\UserService::class,
            Services\Implementations\UserServiceImplementation::class
        );
        $this->app->bind(
            Services\EventCategoryService::class,
            Services\Implementations\EventCategoryServiceImplementation::class
        );
        $this->app->bind(
            Services\EventSessionService::class,
            Services\Implementations\EventSessionServiceImplementation::class
        );
        $this->app->bind(
            Services\PaymentService::class,
            Services\Implementations\PaymentServiceImplementation::class
        );
        $this->app->bind(
            Services\TicketService::class,
            Services\Implementations\TicketServiceImplementation::class
        );
        $this->app->bind(
            Services\TransactionService::class,
            Services\Implementations\TransactionServiceImplementation::class
        );
        $this->app->bind(Services\EventService::class, function ($app) {
            return new Services\Implementations\EventServiceImplementation(
                $app->make(Services\EventSessionService::class)
            );
        });
    }

    /**
     * Register observers with the Models.
     *
     * @return void
     */
    protected function registerEloquentObservers()
    {
        Models\Post::observe(Observers\PostObserver::class);
        Models\Tag::observe(Observers\TagObserver::class);
        Models\User::observe(Observers\UserObserver::class);
        Models\EventType::observe(Observers\EventTypeObserver::class);
        Models\EventCategory::observe(Observers\EventCategoryObserver::class);
        Models\Event::observe(Observers\EventObserver::class);
        Models\EventTicket::observe(Observers\EventTicketObserver::class);
        Models\Transaction::observe(Observers\TransactionObserver::class);
        Models\Payment::observe(Observers\PaymentObserver::class);
        Models\Ticket::observe(Observers\TicketObserver::class);
    }

    /**
     * Register custom validation rules.
     *
     * @return void
     */
    protected function registerCustomValidations()
    {
        Facades\Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            if (! is_null($value)) {
                return Facades\Hash::check($value, $parameters[0]);
            }

            return true;
        });

        Facades\Validator::extend('hashtag', function ($attribute, $value, $parameters, $validator) {
            if (! is_string($value) && ! is_numeric($value)) {
                return false;
            }

            return ($value[0] === '#') && (preg_match('/^[\pL\pM\pN]+$/u', substr($value, 1)) > 0);
        });

        Facades\Validator::extend('must_contains', function ($attribute, $value, $parameters, $validator) {
            $data = array_column(Support\Arr::get($validator->getData(), $attribute), $parameters[0]);

            return in_array($parameters[1], array_values($data));
        });
        Facades\Validator::replacer('must_contains', function ($message, $attribute, $rule, $parameters) {
            $message = str_replace(':key', $parameters[0], $message);
            $message = str_replace(':value', $parameters[1], $message);

            return $message;
        });
    }

    /**
     * Register the application's roles and permissions.
     *
     * @return void
     */
    protected function registerRoles()
    {
        if (! is_dir($path = resource_path('roles'))) {
            return;
        }

        Bouncer::useAbilityModel(Models\Ability::class);
        Bouncer::useRoleModel(Models\Role::class);

        $files = Finder::create()
            ->in($path)
            ->name('*.php')
            ->notName('defaults.php');

        $bouncer = app(Bouncer::class);
        $bouncer->seeder(function () use ($bouncer, $files) {
            collect($files)->each(function ($file) use ($bouncer) {
                $role = require $file->getRealPath();
                $name = $role['name'];
                $permissions = $role['permissions'];

                $bouncer->allow("$name")->to($permissions);
            });
        });
    }

    /**
     * Configure the mailer queue.
     *
     * @return void
     */
    protected function configureMailer()
    {
        app('mailer')->setQueue(app('queue'));

        if (env('MAIL_FAKE')) {
            Mail::fake();
        }
    }

    /**
     * Configure the routing.
     *
     * @return void
     */
    protected function configureRouting()
    {
        $this->app->extend('url', function () {
            return new UrlGenerator($this->app);
        });

        app('url')->forceCachedSchemeAndRoot(config('app.url'));
    }

    /**
     * Configure the google maps.
     *
     * @return void
     */
    protected function configureGoogleMaps()
    {
        $this->app->extend('geocoder', function () {
            return (new ProviderAndDumperAggregator)
                ->registerProvidersFromConfig(collect(config('geocoder.providers')));
        });
    }
}
