<?php

namespace App\Illuminate\Routing;

use Laravel\Lumen\Routing\UrlGenerator as BaseUrlGenerator;

class UrlGenerator extends BaseUrlGenerator
{
    /**
     * Force the cached scheme and root.
     *
     * @param  string  $value
     * @return void
     */
    public function forceCachedSchemeAndRoot($value)
    {
        $scheme = starts_with($value, 'http://') ? 'http://' : 'https://';

        $this->cachedScheme = $scheme;
        $this->cachedRoot = $value;
    }
}
