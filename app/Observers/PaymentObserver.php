<?php

namespace App\Observers;

use App\Models\Payment;
use Illuminate\Support\Facades\DB;

class PaymentObserver
{
    /**
     * Listen to the Payment created event.
     *
     * @param  \App\Models\Payment $payment
     * @return void
     */
    public function created(Payment $payment)
    {
        $this->handle($payment);
    }

    /**
     * Listen to the Payment updated event.
     *
     * @param  \App\Models\Payment $payment
     * @return void
     */
    public function updated(Payment $payment)
    {
        $this->handle($payment);
    }

    /**
     * Handle incoming payment.
     *
     * @param  \App\Models\Payment $payment
     * @return void
     */
    protected function handle(Payment $payment)
    {
        $transaction = $payment->transaction;

        if (! $transaction->is_paid) {
            $totalPayments = (int)$transaction->validPayments()->sum('amount');
            $totalTransaction = (int)$transaction->eventSessionTickets()
                ->with('eventTicket')
                ->get()
                ->sum('eventTicket.price');

            // Check is the transaction is already paid off.
            if ($totalPayments >= $totalTransaction) {
                DB::transaction(function () use ($transaction) {
                    // Because the transaction already paid off, then we update
                    // the transaction 'is_paid' attribute to true.
                    $transaction->forceFill([
                        'is_paid' => true,
                    ])->save();

                    // Then we reduce each of event ticket 'stock' attribute by 1.
                    $transaction->tickets->each(function ($ticket) {
                        $ticket->load('eventSessionTicket.eventTicket');

                        $eventTicket = $ticket->eventSessionTicket->eventTicket;

                        $eventTicket->forceFill([
                            'stock' => (int)$eventTicket->stock - 1
                        ])->save();
                    });
                });
            }
        }
    }
}
