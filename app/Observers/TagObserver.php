<?php

namespace App\Observers;

use App\Models\Tag;

class TagObserver
{
    /**
     * Listen to the Tag creating event.
     *
     * @param  \App\Models\Tag  $tag
     * @return void
     */
    public function creating(Tag $tag)
    {
        $tag->slug = str_slug($tag->name, '-');
    }

    /**
     * Listen to the Tag updating event.
     *
     * @param  \App\Models\Tag  $tag
     * @return void
     */
    public function updating(Tag $tag)
    {
        $tag->slug = str_slug($tag->name, '-');
    }
}
