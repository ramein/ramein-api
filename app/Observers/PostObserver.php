<?php

namespace App\Observers;

use App\Models\Post;
use App\Services\PostService;

class PostObserver
{
    /**
     * @var \App\Services\PostService
     */
    protected $postService;

    /**
     * Constructor.
     *
     * @param  \App\Services\PostService  $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Listen to the Post created event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        $this->postService->attachAbility($post, $post->user);
    }

    /**
     * Listen to the Post deleting event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleting(Post $post)
    {
        $this->postService->detachAbility($post, $post->user);
        $this->postService->destroyMedia($post);
    }
}
