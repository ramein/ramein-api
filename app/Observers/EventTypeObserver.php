<?php

namespace App\Observers;

use App\Models\EventType;

class EventTypeObserver
{
    /**
     * Listen to the EventType creating event.
     *
     * @param  \App\Models\EventType $eventType
     * @return void
     */
    public function creating(EventType $eventType)
    {
        $eventType->slug = str_slug($eventType->name, '-');
    }

    /**
     * Listen to the EventType updating event.
     *
     * @param  \App\Models\EventType $eventType
     * @return void
     */
    public function updating(EventType $eventType)
    {
        $eventType->slug = str_slug($eventType->name, '-');
    }
}
