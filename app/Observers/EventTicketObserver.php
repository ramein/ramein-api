<?php

namespace App\Observers;

use App\Models\EventTicket;

class EventTicketObserver
{
    /**
     * Listen to the EventTicket creating event.
     *
     * @param  \App\Models\EventTicket $eventTicket
     * @return void
     */
    public function creating(EventTicket $eventTicket)
    {
        $eventTicket->stock = $eventTicket->quota;
    }

    /**
     * Listen to the EventTicket updating event.
     *
     * @param  \App\Models\EventTicket $eventTicket
     * @return void
     */
    public function updating(EventTicket $eventTicket)
    {
        if ($eventTicket->tickets()->count() > 0) {
            if ($eventTicket->isDirty('quota')) {
                // quota can't be updated if event ticket already have a transaction.
                $eventTicket->quota = $eventTicket->getOriginal('quota');
            }
        }
    }
}
