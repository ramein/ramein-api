<?php

namespace App\Observers;

use App\Models\Ticket;

class TicketObserver
{
    /**
     * Listen to the Ticket created event.
     *
     * @param  \App\Models\Ticket $ticket
     * @return void
     */
    public function created(Ticket $ticket)
    {
        $transaction = $ticket->fresh('transaction')->transaction;

        // Sum the event ticket 'price' of the current ticket, with the current
        // transaction 'total' value.
        $transaction->forceFill([
            'total' => $transaction->total + $ticket->eventSessionTicket->eventTicket->price,
        ])->save();
    }

    /**
     * Listen to the Ticket updated event.
     *
     * @param  \App\Models\Ticket $ticket
     * @return void
     */
    public function updated(Ticket $ticket)
    {
        //
    }
}
