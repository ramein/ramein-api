<?php

namespace App\Observers;

use App\Models;
use App\Services\UserService;
use App\Events\User\UserCreated;

class UserObserver
{
    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Constructor.
     *
     * @param  \App\Services\UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(Models\User $user)
    {
        $permissions = require resource_path('roles/defaults.php');

        $user->allow($permissions[get_class($user->userable)]);
        $this->userService->attachAbility($user);

        if ($user->userable instanceof Models\Owner) {
            $user->assign('Administrator');
        }

        event(new UserCreated($user));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleting(Models\User $user)
    {
        $this->userService->detachAbility($user);
    }
}
