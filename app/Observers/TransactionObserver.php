<?php

namespace App\Observers;

use App\Models\Transaction;
use App\Events\Transaction\TransactionPaid;

class TransactionObserver
{
    /**
     * Listen to the Transaction creating event.
     *
     * @param  \App\Models\Transaction $transaction
     * @return void
     */
    public function creating(Transaction $transaction)
    {
        //
    }

    /**
     * Listen to the Transaction updating event.
     *
     * @param  \App\Models\Transaction $transaction
     * @return void
     */
    public function updating(Transaction $transaction)
    {
        // Check if the 'is_paid' attribute changed from 'false' to 'true'.
        if (! $transaction->getOriginal('is_paid') && $transaction->is_paid) {
            // Because the transaction already paid off, then we notified the customer
            // that has the transaction, in case of the transaction is already paid off.
            event(new TransactionPaid($transaction));
        }
    }
}
