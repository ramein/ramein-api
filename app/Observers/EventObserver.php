<?php

namespace App\Observers;

use App\Models\Event;
use App\Services\EventService;

class EventObserver
{
    /**
     * @var \App\Services\EventService
     */
    protected $eventService;

    /**
     * Constructor.
     *
     * @param  \App\Services\EventService  $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * Listen to the Event created event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function created(Event $event)
    {
        $this->eventService->attachAbility($event, $event->user);
    }

    /**
     * Listen to the Event deleting event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function deleting(Event $event)
    {
        $this->eventService->detachAbility($event, $event->user);
        $this->eventService->destroyPoster($event);
    }
}
