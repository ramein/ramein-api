<?php

namespace App\Observers;

use App\Models\EventCategory;

class EventCategoryObserver
{
    /**
     * Listen to the EventCategory creating event.
     *
     * @param  \App\Models\EventCategory $eventCategory
     * @return void
     */
    public function creating(EventCategory $eventCategory)
    {
        $eventCategory->slug = str_slug($eventCategory->name, '-');
    }

    /**
     * Listen to the EventCategory updating event.
     *
     * @param  \App\Models\EventCategory $eventCategory
     * @return void
     */
    public function updating(EventCategory $eventCategory)
    {
        $eventCategory->slug = str_slug($eventCategory->name, '-');
    }
}
