<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Arr;

class DevApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_ENV') === 'local' && ! is_null($token = $request->get('access_token'))) {
            $request->headers->add([
                'Authorization' => 'Bearer ' . $token,
            ]);
        }

        return $next($request);
    }
}
