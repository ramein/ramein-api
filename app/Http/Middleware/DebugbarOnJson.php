<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class DebugbarOnJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($response instanceof JsonResponse &&
            app()->bound('debugbar') &&
            app('debugbar')->isEnabled() &&
            $request->get('debugbar') !== 'false'
        ) {
            return view('debugger')->with('data', $response->getData());
        }

        return $response;
    }
}
