<?php

namespace App\Http\Controllers\Api;

use App\Models;
use App\Http\Requests\Request;
use App\Services\EventService;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EventRequest;
use App\Services\EventSessionService;
use App\Http\Requests\EventScanRequest;

class EventsController extends BaseApiController
{
    /**
     * @var \App\Services\EventService
     */
    protected $eventService;

    /**
     * @var \App\Services\EventSessionService
     */
    protected $eventSessionService;

    /**
     * Constructor.
     *
     * @param  \App\Services\EventService  $eventService
     * @param  \App\Services\EventSessionService  $eventSessionService
     */
    public function __construct(EventService $eventService, EventSessionService $eventSessionService)
    {
        $this->eventService = $eventService;
        $this->eventSessionService = $eventSessionService;
    }

    /**
     * Display a listing of the entire events.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function entireEvents(Request $request)
    {
        // @TODO : Event Visibility Checking

        return $this->ok($this->eventService->getEvents(
            collect($request->all())
        ));
    }

    /**
     * Display a listing of the user events.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Models\User $user)
    {
        // @TODO : Event Visibility Checking

        return $this->ok($this->eventService->getEvents(
            collect($request->all()),
            $user
        ));
    }

    /**
     * Display specific event in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, Models\User $user, Models\Event $event)
    {
        // @TODO : Event Visibility Checking
        abort_unless($event->user_id === $user->id, 404);

        return $this->ok($this->eventService->getEvent(
            collect($request->all()),
            $event
        ));
    }

    /**
     * Handle event QR Code scanning.
     *
     * @param  \App\Http\Requests\EventScanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function scanning(EventScanRequest $request)
    {
        $event = Models\Event::whereCode($request->code)->first();

        return $this->ok($this->eventService->getEvent(
            collect($request->all()),
            $event
        ));
    }

    /**
     * Store a newly created event in storage.
     *
     * @param  \App\Http\Requests\EventRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request, Models\User $user)
    {
        $this->dontAllowAdminOnSuperAdmin($user);
        $this->authorize('events.create'); // Only allow Owner and EventOrganizer
        $this->authorize('events.create.on', $user);

        $event = DB::transaction(function () use ($request, $user) {
            $event = $this->eventService->createAll(
                collect($request->all()),
                $user
            );

            if ($request->hasFile('poster')) {
                if (! $this->eventService->storePoster($request->file('poster'), $user, $event)) {
                    $this->conflict('Whoops! Something went wrong when uploading your poster.');
                }
            }

            return $event;
        });

        return $this->ok($event->load([
            'sessions.type', 'sessions.location', 'sessions.questions', 'category', 'poster',
        ]));
    }

    /**
     * Update specific event in storage.
     *
     * @param  \App\Http\Requests\EventRequest  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Models\User $user, Models\Event $event)
    {
        abort_unless($event->user_id === $user->id, 404);
        $this->authorize('update', $event);

        $event = $this->eventService->updateAll(
            collect($request->all()),
            $event
        );

        return $this->ok($event->load([
            'sessions.type', 'sessions.location', 'sessions.questions', 'category', 'poster',
        ]));
    }

    /**
     * Update specific event poster in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function updatePoster(Request $request, Models\User $user, Models\Event $event)
    {
        abort_unless($event->user_id === $user->id, 404);
        $this->authorize('update-poster', $event);

        $this->validate($request, [
            'poster' => 'required|mimes:png,jpg,gif,jpeg|max:5000',
        ]);

        if ($request->hasFile('poster')) {
            if (! $this->eventService->updatePoster($request->file('poster'), $user, $event)) {
                $this->conflict('Whoops! Something went wrong when updating your poster.');
            }
        }

        return $this->ok($event->load([
            'poster',
        ]));
    }

    /**
     * Destroy specific event in storage.
     *
     * @param  \App\Models\Event  $event
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Models\User $user, Models\Event $event)
    {
        abort_unless($event->user_id === $user->id, 404);
        $this->authorize('delete', $event);

        $event->delete();

        return $this->modelDeleted();
    }
}
