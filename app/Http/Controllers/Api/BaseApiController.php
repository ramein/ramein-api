<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Http\Controllers\Controller as BaseController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BaseApiController extends BaseController
{
    use ApiResponse;

    /**
     * Don't allow on Administrator or Super Administrator resources and
     * dont allow Administrator on Super Administrator resources.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function dontAllowOnAdminOrSuperAdmin(User $user)
    {
        $this->dontAllowAdminOnSuperAdmin($user);

        if (! request()->user()->isSuperAdminOrAdmin() && $user->isSuperAdminOrAdmin()) {
            abort(403);
        }
    }

    /**
     * Don't allow Administrator on Super Administrator resources.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function dontAllowAdminOnSuperAdmin(User $user)
    {
        if (request()->user()->isAdmin() && $user->isSuperAdmin()) {
            abort(403);
        }
    }
}
