<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Requests\UserRequest;

class UsersController extends BaseApiController
{
    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Constructor.
     *
     * @param  \App\Services\UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display the logged in user data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function current(Request $request)
    {
        $user = $request->user();

        $result = collect($user->toArray());

        if ($user->isCustomer()) {
            $tickets = $user->userable->tickets()->with([
                    'transaction', 'eventSessionTicket.eventTicket',
                ])
                ->paid()
                ->declined(false)
                ->get();

            $eventBought = $tickets->groupBy('eventSessionTicket.eventTicket.event_id')
                ->count();

            $eventVisited = $tickets->where('is_scanned', true)
                ->groupBy('eventSessionTicket.eventTicket.event_id')
                ->count();

            $ticketPurchased = $tickets->count();

            $result->put('events', [
                'event_bought' => $eventBought,
                'event_visited' => $eventVisited,
            ]);

            $result->put('tickets', [
                'ticket_purchased' => $ticketPurchased,
            ]);
        }

        return $result;
    }

    /**
     * Display a listing of the users.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $user = $request->user();
        $data = null;

        if ($user->isSuperAdmin()) {
            $data = new User();
        } elseif ($user->isAdmin()) {
            $data = User::whereIsNot(['Super Administrator']);
        } else {
            $data = User::active()->whereIsNot([
                'Super Administrator',
                'Administrator',
            ]);
        }

        return $this->ok($this->userService->getUsers(
            collect($request->all()),
            $data
        ));
    }

    /**
     * Display specific user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, User $user)
    {
        $this->dontAllowOnAdminOrSuperAdmin($user);

        // Only allow to see active users.
        if (! $request->user()->isSuperAdminOrAdmin() && ! $user->isActive()) {
            abort(403);
        }

        return $this->ok($this->userService->getUser(
            collect($request->all()),
            $user
        ));
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = $this->userService->create(
            collect($request->all())
        );

        return $this->ok($user);
    }

    /**
     * Update specific user in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $user = $this->userService->update(
            $user,
            collect($request->all())
        );

        return $this->ok($user);
    }

    /**
     * Update specific user photo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePhoto(Request $request, User $user)
    {
        $this->authorize('update-photo', $user);

        $this->validate($request, [
            'photo' => 'required|mimes:png,jpg,gif,jpeg|max:1000',
        ]);

        if ($request->hasFile('photo')) {
            if (! $this->userService->storeMedia($request->file('photo'), $user)) {
                return $this->conflict([
                    'message' => 'Whoops! Something went wrong when updating your photo.',
                ]);
            }
        }

        return $this->ok($user);
    }

    /**
     * Destroy specific user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();

        return $this->modelDeleted();
    }
}
