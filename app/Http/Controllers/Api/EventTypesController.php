<?php

namespace App\Http\Controllers\Api;

use App\Models\EventType;
use Illuminate\Http\Request;

class EventTypesController extends BaseApiController
{
    /**
     * Display a listing of the event categories.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, EventType $eventType)
    {
        if (! is_null($q = $request->get('q'))) {
            $eventType = $eventType->search($q);
        }

        if (! is_null($limit = (int)$request->get('limit')) && $limit > 0) {
            $eventType = $eventType->limit($limit);
        }

        if ($request->get('paginate') === 'false') {
            return $this->ok($eventType->get());
        }

        $perPage = 10;

        if (! is_null($request->get('perPage')) && $request->get('perPage') > 0) {
            $perPage = $request->get('perPage');
        }

        return $this->ok(paginate($eventType->get(), $perPage));
    }

    /**
     * Display specific event category in storage.
     *
     * @param  \App\Models\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function get(EventType $eventType)
    {
        return $this->ok($eventType);
    }

    /**
     * Store a newly created event category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('event_types.create');

        $this->validate($request, [
            'name' => 'required|string|max:255|unique:event_types',
        ]);

        $eventType = EventType::create($request->all());

        return $this->ok($eventType);
    }

    /**
     * Update specific event category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventType $eventType)
    {
        $this->authorize('event_types.update');

        $this->validate($request, [
            'name' => 'required|string|max:255|unique:event_types,name,' . $eventType->id,
        ]);

        $eventType->update($request->all());

        return $this->ok($eventType);
    }

    /**
     * Destroy specific event category in storage.
     *
     * @param  \App\Models\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventType $eventType)
    {
        $this->authorize('event_types.delete');

        $eventType->delete();

        return $this->modelDeleted();
    }
}
