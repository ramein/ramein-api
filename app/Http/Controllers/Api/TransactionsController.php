<?php

namespace App\Http\Controllers\Api;

use App\Models;
use App\Http\Requests\Request;
use App\Services\TransactionService;
use App\Http\Requests\TransactionRequest;

class TransactionsController extends BaseApiController
{
    /**
     * @var \App\Services\TransactionService
     */
    protected $transactionService;

    /**
     * Constructor.
     *
     * @param  \App\Services\TransactionService  $transactionService
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Display a listing of the entire transaction.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function entireTransactions(Request $request)
    {
        $this->authorize('transactions.view_all');

        return $this->ok($this->transactionService->getTransactions(
            collect($request->all())
        ));
    }

    /**
     * Display a listing of the transaction.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Models\Customer $customer)
    {
        $this->authorize('transactions.view', $customer->user);

        return $this->ok($this->transactionService->getTransactions(
            collect($request->all()),
            $customer
        ));
    }

    /**
     * Display the specified transaction.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, Models\Customer $customer, Models\Transaction $transaction)
    {
        abort_unless($transaction->customer_id === $customer->id, 404);

        return $this->ok($this->transactionService->getTransaction(
            collect($request->all()),
            $transaction
        ));
    }

    /**
     * Store a newly created transaction in storage.
     *
     * @param  \App\Http\Requests\TransactionRequest  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request, Models\Customer $customer)
    {
        $this->authorize('transactions.create.on', $customer->user);

        $result = null;

        $result = $this->transactionService->create(
            collect($request->get('items')),
            $customer
        );

        return $this->ok($result);
    }

    /**
     * Update the specified transaction in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Models\Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified transaction from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Models\Customer $customer, Models\Transaction $transaction)
    {
        abort_unless($transaction->customer_id === $customer->id, 404);
        $this->authorize('delete', $transaction);

        $transaction->delete();

        return $this->modelDeleted();
    }
}
