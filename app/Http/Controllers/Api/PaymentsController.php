<?php

namespace App\Http\Controllers\Api;

use App\Models;
use App\Http\Requests\Request;
use App\Services\PaymentService;

class PaymentsController extends BaseApiController
{
    /**
     * @var \App\Services\PaymentService
     */
    protected $paymentService;

    /**
     * Constructor.
     *
     * @param  \App\Services\PaymentService  $paymentService
     */
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    /**
     * Display a listing of the entire payment.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function entirePayments(Request $request)
    {
        $this->authorize('payments.view_all');

        return $this->ok($this->paymentService->getPayments(
            collect($request->all())
        ));
    }

    /**
     * Display a listing of the entire customer payment.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function entireTransactionPayments(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction
    ) {
        $this->authorize('transactions.payments.view', $customer->user);

        abort_unless($transaction->customer_id === $customer->id, 404);

        return $this->ok($this->paymentService->getPayments(
            collect($request->all()),
            $transaction
        ));
    }

    /**
     * Display a listing of the payment.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Models\Customer $customer)
    {
        $this->authorize('payments.view', $customer->user);

        return $this->ok($this->paymentService->getPayments(
            collect($request->all()),
            $customer
        ));
    }

    /**
     * Display the specified payment.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function get(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction,
        Models\Payment $payment
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);
        abort_unless($payment->transaction_id === $transaction->id, 404);

        return $this->ok($this->paymentService->getPayment(
            collect($request->all()),
            $payment
        ));
    }

    /**
     * Store a newly created payment in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);

        //
    }

    /**
     * Update the specified payment in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction,
        Models\Payment $payment
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);
        abort_unless($payment->transaction_id === $transaction->id, 404);

        //
    }

    /**
     * Remove the specified payment from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Models\Customer $customer,
        Models\Transaction $transaction,
        Models\Payment $payment
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);
        abort_unless($payment->transaction_id === $transaction->id, 404);

        $this->authorize('delete', $payment);

        $payment->delete();

        return $this->modelDeleted();
    }
}
