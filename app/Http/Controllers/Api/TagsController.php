<?php

namespace App\Http\Controllers\Api;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
    use ApiResponse;

    /**
     * Display a listing of the tags.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Tag $tag)
    {
        if (! is_null($limit = (int)$request->get('limit')) && $limit > 0) {
            $tag = $tag->limit($limit);
        }

        if ($request->get('paginate') === 'false') {
            return $this->ok($tag->all());
        }

        $perPage = 10;

        if (! is_null($request->get('perPage')) && $request->get('perPage') > 0) {
            $perPage = $request->get('perPage');
        }

        return $this->ok(paginate($tag->get(), $perPage));
    }

    /**
     * Display specific tag in storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function get(Tag $tag)
    {
        return $this->ok($tag);
    }

    /**
     * Store a newly created tag in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('tags.create');

        $this->validate($request, [
            'name' => 'required|hashtag|max:255|unique:tags',
        ]);

        $tag = Tag::create($request->all());

        return $this->ok($tag);
    }

    /**
     * Update specific tag in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $this->authorize('tags.update');

        $this->validate($request, [
            'name' => 'required|hashtag|max:255|unique:tags,name,' . $tag->id,
        ]);

        $tag->update($request->all());

        return $this->ok($tag);
    }

    /**
     * Destroy specific tag in storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $this->authorize('tags.delete');

        $tag->delete();

        return $this->modelDeleted();
    }
}
