<?php

namespace App\Http\Controllers\Api;

use App\Models;
use Illuminate\Http\Request;
use App\Services\PostService;

class PostsController extends BaseApiController
{
    /**
     * @var \App\Services\PostService
     */
    protected $postService;

    /**
     * Constructor.
     *
     * @param  \App\Services\PostService  $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Display a listing of the entire posts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function entirePosts(Request $request)
    {
        $user = $request->user();

        if ($user->isSuperAdminOrAdmin()) {
            return $this->ok(Models\Post::with('media')->paginate(10));
        }

        return $this->ok(Models\Post::with('media')->visibleFor($user)->paginate(10));
    }

    /**
     * Display a listing of the user posts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Models\User $user)
    {
        if (! $request->user()->isSuperAdminOrAdmin()
            && ! $request->user()->canView($user)) {
            return $this->ok([
                'total' => $user->posts->count(),
            ]);
        }

        return $this->ok($user->posts()->with('media')->paginate(10));
    }

    /**
     * Display specific user post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, Models\User $user, Models\Post $post)
    {
        abort_unless($post->user_id === $user->id, 404);

        if (! $user->visibility) {
            $this->authorize('view', $post);
        }

        return $this->ok($post->load('media'));
    }

    /**
     * Like the specific user post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function like(Request $request, Models\User $user, Models\Post $post)
    {
        abort_unless($post->user_id === $user->id, 404);

        if (! $user->visibility) {
            $this->authorize('like-dislike', $post);
        }

        if (! $request->user()->isLike($post)) {
            $request->user()->postLikes()->attach($post);
        } else {
            return $this->conflict([
                'message' => 'You have been liked the post.',
            ]);
        }

        return $this->ok($post);
    }

    /**
     * Dislike the specific user post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function dislike(Request $request, Models\User $user, Models\Post $post)
    {
        abort_unless($post->user_id === $user->id, 404);

        if (! $user->visibility) {
            $this->authorize('like-dislike', $post);
        }

        if ($request->user()->isLike($post)) {
            $request->user()->postLikes()->detach($post);
        } else {
            return $this->notFound([
                'message' => 'You aren\'t liked the post.',
            ]);
        }

        return $this->ok($post);
    }

    /**
     * Store a newly created user post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Models\User $user)
    {
        $this->dontAllowAdminOnSuperAdmin($user);
        $this->authorize('posts.create.on', $user);

        $this->validate($request, [
            'media' => 'required|mimes:png,jpg,jpeg,gif,mp4,3gp|max:2000',
            'content' => 'required',
        ]);

        $post = new Models\Post($request->all());
        $user->posts()->save($post);

        if (! $this->postService->storeMedia($request->file('media'), $user, $post)) {
            $post->delete();

            return $this->conflict([
                'message' => 'Whoops! Something wrong when creating your post.',
            ]);
        }

        return $this->ok($post->load('media'));
    }

    /**
     * Update specific user post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Models\User $user, Models\Post $post)
    {
        abort_unless($post->user_id === $user->id, 404);
        $this->authorize('update', $post);

        $this->validate($request, [
            'content' => 'required',
        ]);

        $post->update($request->all());

        return $this->ok($post);
    }

    /**
     * Destroy specific user post in storage.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Models\User $user, Models\Post $post)
    {
        abort_unless($post->user_id === $user->id, 404);
        $this->authorize('delete', $post);

        $post->delete();

        return $this->modelDeleted();
    }
}
