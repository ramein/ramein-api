<?php

namespace App\Http\Controllers\Api;

use App\Models;
use App\Http\Requests\Request;
use App\Services\EventService;
use App\Services\TicketService;
use App\Http\Requests\TicketScanRequest;

class TicketsController extends BaseApiController
{
    /**
     * @var \App\Services\TicketService
     */
    protected $ticketService;

    /**
     * @var \App\Services\EventService
     */
    protected $eventService;

    /**
     * Constructor.
     *
     * @param  \App\Services\TicketService  $ticketService
     * @param  \App\Services\EventService  $eventService
     */
    public function __construct(TicketService $ticketService, EventService $eventService)
    {
        $this->ticketService = $ticketService;
        $this->eventService = $eventService;
    }

    /**
     * Display a listing of the entire ticket.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function entireTickets(Request $request)
    {
        $this->authorize('tickets.view_all');

        return $this->ok($this->ticketService->getTickets(
            collect($request->all())
        ));
    }

    /**
     * Display a listing of the entire customer ticket.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function entireTransactionTickets(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction
    ) {
        $this->authorize('transactions.tickets.view', $customer->user);

        abort_unless($transaction->customer_id === $customer->id, 404);

        return $this->ok($this->ticketService->getTickets(
            collect($request->all()),
            $transaction
        ));
    }

    /**
     * Display a listing of sold tickets on the event.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\User  $user
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function entireSoldTicketsOnEvent(
        Request $request,
        Models\User $user,
        Models\Event $event
    ) {
        abort_unless($event->user_id === $user->id, 404);

        if (! $request->user()->isSuperAdminOrAdmin()) {
            abort_unless($user->id === $request->user()->id, 403);
        }

        $event = $this->eventService->getEvent(collect($request->all()), $event, [
            'sessions.tickets.transaction',
        ]);

        $tickets = collect();

        collect($event['sessions'])->each(function ($session) use ($tickets) {
            collect($session['tickets'])->each(function ($ticket) use ($tickets) {
                $tickets->push($ticket);
            });
        });

        $data = paginate($tickets)->toArray();

        return $this->ok(array_merge([
            // ini cuman sementara
            'valid_ticket_total' => $tickets->where('is_valid', true)->count(),
            'invalid_ticket_total' => $tickets->where('is_valid', false)->count(),

            'scanned_ticket_total' => $tickets->where('is_scanned', true)->count(),
            'paid_ticket_total' => $tickets->where('transaction.is_paid', true)->count(),
        ], $data));
    }

    /**
     * Display a listing of the ticket.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Models\Customer $customer)
    {
        $this->authorize('tickets.view', $customer->user);

        return $this->ok($this->ticketService->getTickets(
            collect($request->all()),
            $customer
        ));
    }

    /**
     * Display the specified ticket.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function get(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction,
        Models\Ticket $ticket
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);
        abort_unless($ticket->transaction_id === $transaction->id, 404);

        $this->authorize('tickets.view', $customer->user);

        return $this->ok($this->ticketService->getTicket(
            collect($request->all()),
            $ticket
        ));
    }

    /**
     * Handle ticket QR Code scanning.
     *
     * @param  \App\Http\Requests\TicketScanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function scanning(TicketScanRequest $request)
    {
        $ticket = Models\Ticket::whereCode($request->code)->first();

        $eventSession = $ticket->eventSessionTicket->eventSession;
        $creator = $ticket->eventSessionTicket->eventTicket->event->user;

        abort_unless($eventSession->id === (int)$request->event_session_id, 403);

        if ($ticket->is_scanned) {
            abort(403);
        }

        if (! isUserASuperAdminOrAdmin()) {
            abort_unless($creator->id === $request->user()->id, 403);
        }

        $ticket = $this->ticketService->scanning($ticket);

        return $this->ok($this->ticketService->getTicket(
            collect($request->all()),
            $ticket
        ));
    }

    /**
     * Store a newly created ticket in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);

        //
    }

    /**
     * Update the specified ticket in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(
        Request $request,
        Models\Customer $customer,
        Models\Transaction $transaction,
        Models\Ticket $ticket
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);
        abort_unless($ticket->transaction_id === $transaction->id, 404);

        //
    }

    /**
     * Remove the specified ticket from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @param  \App\Models\Transaction  $transaction
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Models\Customer $customer,
        Models\Transaction $transaction,
        Models\Ticket $ticket
    ) {
        abort_unless($transaction->customer_id === $customer->id, 404);
        abort_unless($ticket->transaction_id === $transaction->id, 404);

        $this->authorize('delete', $ticket);

        $ticket->delete();

        return $this->modelDeleted();
    }

    /**
     * Handle un-scanning the ticket.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function unscan(Models\Ticket $ticket)
    {
        $ticket->forceFill([
            'is_scanned' => false,
            'scanned_on' => null,
        ])->save();

        return $this->ok($ticket);
    }
}
