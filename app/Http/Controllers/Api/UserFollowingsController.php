<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserFollowingsController extends Controller
{
    use ApiResponse;

    /**
     * Display specific user following in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, User $user)
    {
        if ($request->user()->isNotAn('Administrator')
            && $user->isAn('Administrator')) {
            $this->deny();
        }

        if ($request->user()->isNotAn('Administrator')
            && ! $request->user()->canView($user)) {
            return $this->ok([
                'total' => $user->following->count(),
            ]);
        }

        return $this->ok($user->following()->paginate(10));
    }
}
