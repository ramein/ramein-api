<?php

namespace App\Http\Controllers\Api;

use App\Models;
use App\Http\Requests\Request;
use App\Services\EventSessionService;

class EventSessionsController extends BaseApiController
{
    /**
     * @var \App\Services\EventSessionService
     */
    protected $eventSessionService;

    /**
     * Constructor.
     *
     * @param  \App\Services\EventSessionService  $eventSessionService
     */
    public function __construct(EventSessionService $eventSessionService)
    {
        $this->eventSessionService = $eventSessionService;
    }

    /**
     * Display specific event in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Event  $event
     * @param  \App\Models\EventSession  $eventSession
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, Models\Event $event, Models\EventSession $eventSession)
    {
        abort_unless($eventSession->event_id === $event->id, 404);

        return $this->ok($this->eventSessionService->getEventSession(
            collect($request->all()),
            $eventSession
        ));
    }
}
