<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;

class UserFriendshipsController extends Controller
{
    use ApiResponse;

    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Constructor.
     *
     * @param  \App\Services\UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Store a friend to the user following.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $friend
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $friend)
    {
        $user = $request->user();

        if ($user->is($friend)) {
            $this->flowException([
                'message' => 'You can\'t follow yourself.',
            ]);
        }

        if (! $user->isFollow($friend)) {
            $user->following()->attach($friend);

            // If friend visibility is public, always set is_accepted as true,
            // and attach ability for user to view friend resource.
            if ($friend->visibility) {
                $friend->followers()->updateExistingPivot($user->id, [
                    'is_accepted' => true,
                ]);

                $this->userService->attachPostsAbility($friend, $user);
            }
        } else {
            return $this->conflict([
                'message' => 'You have been followed the user.',
            ]);
        }

        return $this->ok($friend);
    }

    /**
     * Accept a friend friendship request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $friend
     * @return \Illuminate\Http\Response
     */
    public function accept(Request $request, User $friend)
    {
        $user = $request->user();

        if ($user->is($friend)) {
            return abort(403);
        }

        if (! $friend->isFollow($user)) {
            return $this->notFound([
                'message' => 'The user isn\'t followed you.',
            ]);
        }

        // Only run this operation if the user has friendship request
        // from those friend (still requesting). If isn't, never run
        // this. It's to prevent user touch his rejected followers.
        if ($user->hasFriendshipRequestFrom($friend)) {
            $user->followers()->updateExistingPivot($friend->id, [
                'is_accepted' => true,
            ]);

            $this->userService->attachPostsAbility($user, $friend);
        }

        return $this->ok($friend);
    }

    /**
     * Reject a friend friendship request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $friend
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, User $friend)
    {
        $user = $request->user();

        if ($user->is($friend)) {
            return abort(403);
        }

        if (! $friend->isFollow($user)) {
            return $this->notFound([
                'message' => 'The user isn\'t followed you.',
            ]);
        }

        // Only run this operation if the user has friendship request
        // from those friend (still requesting). If isn't, never run
        // this. It's to prevent user touch his accepted followers.
        if ($user->hasFriendshipRequestFrom($friend)) {
            $this->userService->detachPostsAbility($user, $friend);

            $user->followers()->updateExistingPivot($friend->id, [
                'is_accepted' => false,
            ]);
        }

        return $this->ok($friend);
    }

    /**
     * Destroy a friend from the user following.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $friend
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $friend)
    {
        $user = $request->user();

        if ($user->is($friend)) {
            $this->flowException([
                'message' => 'You can\'t unfollow yourself.',
            ]);
        }

        if ($user->isFollow($friend)) {
            $this->userService->detachPostsAbility($friend, $user);
            $user->following()->detach($friend);
        } else {
            return $this->notFound([
                'message' => 'You aren\'t follow the user.',
            ]);
        }

        return $this->ok($friend);
    }
}
