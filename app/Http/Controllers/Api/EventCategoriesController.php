<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EventCategory;
use App\Services\EventCategoryService;

class EventCategoriesController extends BaseApiController
{
    /**
     * @var \App\Services\EventCategoryService
     */
    protected $eventCategoryService;

    /**
     * Constructor.
     *
     * @param  \App\Services\EventCategoryService  $eventCategoryService
     */
    public function __construct(EventCategoryService $eventCategoryService)
    {
        $this->eventCategoryService = $eventCategoryService;
    }

    /**
     * Display a listing of the event categories.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, EventCategory $eventCategory)
    {
        return $this->ok($this->eventCategoryService->getEventCategories(
            collect($request->all())
        ));
    }

    /**
     * Display specific event category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, EventCategory $eventCategory)
    {
        return $this->ok($this->eventCategoryService->getEventCategory(
            collect($request->all()),
            $eventCategory
        ));
    }

    /**
     * Store a newly created event category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('event_categories.create');

        $this->validate($request, [
            'name' => 'required|string|max:255|unique:event_categories',
        ]);

        $eventCategory = EventCategory::create($request->all());

        return $this->ok($eventCategory);
    }

    /**
     * Update specific event category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventCategory $eventCategory)
    {
        $this->authorize('event_categories.update');

        $this->validate($request, [
            'name' => 'required|string|max:255|unique:event_categories,name,' . $eventCategory->id,
        ]);

        $eventCategory->update($request->all());

        return $this->ok($eventCategory);
    }

    /**
     * Destroy specific event category in storage.
     *
     * @param  \App\Models\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventCategory $eventCategory)
    {
        $this->authorize('event_categories.delete');

        $eventCategory->delete();

        return $this->modelDeleted();
    }
}
