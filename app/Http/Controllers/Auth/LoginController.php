<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\BadResponseException;

class LoginController extends Controller
{
    const REFRESH_TOKEN = 'refresh_token';

    protected $database;

    protected $cookie;

    protected $config;

    protected $errorMessage = 'The credentials not found in our database.';

    /**
     * Constructor.
     *
     */
    public function __construct()
    {
        $this->database = app()->make('db');
        $this->cookie = app()->make('cookie');
        $this->config = app()->make('config');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $this->credentials($request);

        if (! $this->checkUser($credentials)) {
            return $this->sendFailedLoginResponse();
        }

        $credentials = array_merge([
            'username' => $credentials['email'],
        ], $credentials);

        return $this->access('password', $credentials);
    }

    /**
     * Handle a refresh token request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function refreshToken(Request $request)
    {
        $this->validate($request, [
            'refresh_token' => 'required|string',
        ]);

        $refreshToken = $request->refresh_token;

        return $this->access('refresh_token', [
            'refresh_token' => $refreshToken,
        ]);
    }

    /**
     * Send request to the laravel passport.
     *
     * @param  string  $grantType
     * @param  array  $data
     * @return \Illuminate\Http\Response
     */
    private function access($grantType, array $data = [])
    {
        try {
            $data = array_merge([
                'client_id'     => $this->config->get('secrets.client_id'),
                'client_secret' => $this->config->get('secrets.client_secret'),
                'grant_type'    => $grantType,
            ], $data);

            $http = new Client();

            $guzzleResponse = $http->post($this->config->get('app.url').'/oauth/token', [
                'form_params' => $data,
            ]);
        } catch (BadResponseException $e) {
            $guzzleResponse = $e->getResponse();
        }

        $response = response()->json(json_decode($guzzleResponse->getBody()));
        $response->setStatusCode($guzzleResponse->getStatusCode());

        $headers = $guzzleResponse->getHeaders();
        foreach ($headers as $headerType => $headerValue) {
            $response->header($headerType, $headerValue);
        }

        return $response;
    }

    /**
     * Check the given user credentials.
     *
     * @param  array  $credentials
     * @return boolean
     */
    protected function checkUser(array $credentials)
    {
        $user = User::whereEmail($credentials['email'])->first();

        if (! is_null($user) && Hash::check($credentials['password'], $user->password)) {
            if ($user->isBanned()) {
                $this->errorMessage = 'You have been banned from our services.';
                return false;
            } elseif ($user->isPending()) {
                $this->errorMessage = 'Your account not active. Please check your mailbox for a confirmation email.';
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Get the failed login response instance.
     *
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse()
    {
        return response()->json([
            'message' => $this->errorMessage,
        ], 401);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Logout user from the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $accessToken = $request->user()->token();

        $this->database->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        return response()->json([
            'message' => 'You have successfully logged out from the app.',
        ]);
    }
}
