<?php

namespace App\Http\Controllers\Auth;

use App\Services\UserService;
use App\Http\Requests\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Constructor.
     *
     * @param  \App\Services\UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle a user registration request.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function register(UserRequest $request)
    {
        $user = $this->userService->create(
            collect($request->all())
        );

        return response()->json($user);
    }

    /**
     * Handle a user activation request.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function activateByToken(Request $request)
    {
        $this->userService->activateByToken($request->token);

        return response()->json([
            'message' => 'Your account has been activated.',
        ]);
    }
}
