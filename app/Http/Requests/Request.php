<?php

namespace App\Http\Requests;

use Illuminate\Http\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Get the route handling the request.
     *
     * @param  string|null  $param
     *
     * @return array|string
     */
    public function route($param = null)
    {
        $route = app('request')->route();

        if (is_null($route) || is_null($param) || ! isset($route[2])) {
            return $route;
        } else {
            return array_get($route[2], $param, null);
        }
    }

    /**
     * Determine is the user authenticated.
     *
     * @return boolean
     */
    public function isAuthenticated()
    {
        return ! is_null($this->user);
    }

    /**
     * Get an input element from the request.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        if ($this->offsetExists($key)) {
            return $this->offsetGet($key);
        }

        $queries = app('request')->query();

        if (! is_null($value = array_get($queries, $key))) {
            return $value;
        }

        return $this->route($key);
    }
}
