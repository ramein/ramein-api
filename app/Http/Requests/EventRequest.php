<?php

namespace App\Http\Requests;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $event = $this->route('event');

        $rules = collect([
            'title' => 'required|max:255',
            'public' => 'required|boolean',
            'password' => 'confirmed|min:6',
            'event_category' => 'required|exists:event_categories,id',
            'sessions' => 'required|array|min:1|must_contains:session,0',
            'sessions.*.event_type' => 'exists:event_types,id',
            'sessions.*.session' => 'required|min:1|max:10|distinct',
            'sessions.*.details' => 'required',
            'sessions.*.from' => 'required|date_format:d/m/Y H:i',
            'sessions.*.until' => 'required|date_format:d/m/Y H:i|after:sessions.*.from',
            'sessions.*.location' => 'required|array',
            'sessions.*.location.longitude' => 'required|numeric',
            'sessions.*.location.latitude' => 'required|numeric',
            'sessions.*.tickets' => 'array',
            'sessions.*.tickets.*.ticket_currency' => 'required|exists:ticket_currencies,id',
            'sessions.*.tickets.*.max_refund_on' => 'exists:ticket_refund_rules,id',
            'sessions.*.tickets.*.name' => 'required|string|max:255',
            'sessions.*.tickets.*.price' => 'required|integer|min:0',
            'sessions.*.tickets.*.quota' => 'required|integer|min:0',
            'sessions.*.tickets.*.description' => 'string',
            'sessions.*.tickets.*.auto_approve_ticket' => 'boolean',
            'sessions.*.tickets.*.auto_hide_ticket' => 'boolean',
            'sessions.*.tickets.*.from' => 'required|date_format:d/m/Y H:i|before:sessions.*.from',
            'sessions.*.tickets.*.until' => 'required|date_format:d/m/Y H:i|after:sessions.*.tickets.*.from',
//            'sessions.*.tickets.*.until' => 'required|date_format:d/m/Y H:i|after:sessions.*.tickets.*.from|' .
//                'before:sessions.*.from',
            'sessions.*.questions' => 'array',
            'sessions.*.questions.*.value' => 'required|string|max:255',
            'sessions.*.questions.*.is_required' => 'required|boolean',
            'sessions.*.questions.*.need_attachment' => 'required|boolean',
            'poster' => 'mimes:png,jpg,gif,jpeg|max:2000',
        ]);

        switch ($this->method()) {
            case 'POST':
                return $rules->toArray();
            case 'PUT':
            case 'PATCH':
                return $rules->merge([
                    'sessions.*.session_id' => 'exists:event_sessions,id',
                    'sessions.*.tickets.*.ticket_id' => 'exists:event_tickets,id',
                    'sessions.*.questions.*.question_id' => 'exists:event_session_questions,id',
                ])->toArray();
        }
    }
}
