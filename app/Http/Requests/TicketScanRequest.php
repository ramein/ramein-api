<?php

namespace App\Http\Requests;

class TicketScanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|exists:tickets,code',
            'event_session_id' => 'required|exists:event_sessions,id',
        ];
    }
}
