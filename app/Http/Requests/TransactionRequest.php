<?php

namespace App\Http\Requests;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $transaction = $this->route('transaction');

        $rules = collect([
            'items' => 'required|array',
            'items.*.event_session_ticket' => 'required|distinct|exists:event_session_tickets,id',
            'items.*.quantity' => 'required|integer|min:1|max:5',
        ]);

        switch ($this->method()) {
            case 'POST':
                return $rules->toArray();
            case 'PUT':
            case 'PATCH':
                return $rules->toArray();
        }
    }
}
