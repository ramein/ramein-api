<?php

namespace App\Http\Requests;

use Silber\Bouncer\BouncerFacade as Bouncer;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->isAuthenticated()) {
            if ($this->method() === 'POST') {
                return Bouncer::allows('users.create');
            }

            return Bouncer::allows('update', $this->user);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user');

        $rules = collect([
            'username' => 'required|alpha_dash|max:255|unique:users',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'photo' => 'mimes:png,jpg,gif,jpeg|max:1000',
            'role' => 'in:OWNER,CUSTOMER,EVENT_ORGANIZER',
            'type' => 'required_if:role,EVENT_ORGANIZER|in:FREE,PREMIUM',
            'phone' => 'max:255',
            'visibility' => 'boolean',
        ]);

        switch ($this->method()) {
            case 'POST':
                if (! $this->isAuthenticated()) {
                    $rules = $rules->merge([
                        'role' => 'required|in:CUSTOMER,EVENT_ORGANIZER',
                        'type' => 'required_if:role,EVENT_ORGANIZER|in:FREE',
                    ]);
                }

                return $rules->toArray();
            case 'PUT':
            case 'PATCH':
                return $rules->merge([
                    'username' => $rules->get('username') . ',username,' . $user->id,
                    'email' => $rules->get('email') . ',email,' . $user->id,
                    'current_password' => 'old_password:' . $user->password,
                    'password' => '',
                    'type' => 'in:FREE,PREMIUM',
                ])->toArray();
        }
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $validator->sometimes('password', 'required|string|min:6|confirmed', function ($input) {
                return ! is_null($input->current_password);
            });

            $validator->sometimes('current_password', 'required', function ($input) {
                return ! is_null($input->password);
            });
        }

        if (in_array($this->method(), ['POST'])) {
            $validator->sometimes('role', 'required', function ($input) {
                return true;
            });
        }
    }
}
