<?php

namespace App\Listeners\Transaction;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Transaction\TransactionPaid;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Transaction\TransactionPaidMail;

class SendTransactionPaidEmail
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Transaction\TransactionPaid  $event
     * @return void
     */
    public function handle(TransactionPaid $event)
    {
        Mail::send(new TransactionPaidMail($event->transaction));
    }
}
