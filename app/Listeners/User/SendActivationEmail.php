<?php

namespace App\Listeners\User;

use App\Services\UserService;
use App\Events\User\UserCreated;
use App\Mail\User\ActivationMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendActivationEmail
{
    /**
     * @var \App\Services\UserService
     */
    public $userService;

    /**
     * Create the event listener.
     *
     * @param  \App\Services\UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\User\UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $event->user->forceFill([
            'activation_token' => $this->userService->generateActivationToken(),
        ])->save();

        Mail::send(new ActivationMail($event->user, $event->user->activation_token));
    }
}
