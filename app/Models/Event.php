<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use App\Models\Concerns\Reportable;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use Reportable;
//        Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'is_public',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_public' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'has_session',
        'minimum_ticket',
    ];

    /**
     * Always set and hash the password attribute if not empty.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        if (! empty($value)) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    /**
     * Scope a query to only include events that has tickets.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasTickets($query)
    {
        return $query->whereHas('sessions', function ($query) {
            return $query->whereHas('tickets');
        });
    }

    /**
     * Alias for hasTickets scope (means has tickets bought by Customer).
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBought($query)
    {
        return $this->scopeHasTickets($query);
    }

    /**
     * Get the event poster.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function poster()
    {
        return $this->morphOne(File::class, 'fileable');
    }

    /**
     * Get the category for the event.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(EventCategory::class, 'event_category_id');
    }

    /**
     * Get all sessions for the event.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany(EventSession::class)->orderBy('session');
    }

    /**
     * Get the user that owns the event.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all event invitations for the event.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invitations()
    {
        return $this->hasMany(EventInvitation::class);
    }

    /**
     * Get has session attribute.
     *
     * @return boolean
     */
    public function getHasSessionAttribute()
    {
        return $this->sessions->count() > 1;
    }

    /**
     * Get minimum ticket attribute.
     *
     * @return array|null
     */
    public function getMinimumTicketAttribute()
    {
        // @TODO: Don't make like this.
        // This is a very bad approach. Very difficult to maintain.
        $session = $this->sessions->sortBy('minimum_ticket.price')->first();

        return $session ? $session->minimum_ticket : null;
    }

    /**
     * Get the event relations that should be eager loaded.
     *
     * @return array
     */
    public static function shouldLoads()
    {
        return [
            'sessions.eventTickets.currency',
        ];
    }
}
