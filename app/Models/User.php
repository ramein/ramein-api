<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Laravel\Passport\HasApiTokens;
use App\Models\Concerns\Reportable;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable,
        Authorizable,
        HasApiTokens,
        HasRolesAndAbilities,
        Notifiable,
        Reportable,
//        Searchable,
        UserStatus;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'address',
        'phone',
        'biography',
        'visibility',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'activation_token',
        'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'visibility' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'status_name',
    ];

    /**
     * Scope a query to only include user that don't have the given role.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $role
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereIsNot($query, $role)
    {
        $roles = array_slice(func_get_args(), 1);

        return $query->whereHas('roles', function ($query) use ($roles) {
            $query->whereIn('name', $roles);
        }, '<', 1);
    }

    /**
     * Always set and make name attribute as 'ucwords'.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords(strtolower($value));
    }

    /**
     * Always set and hash the password attribute if not empty.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        if (! empty($value)) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    /**
     * Get the profile photo URL attribute.
     *
     * @param string|null $value
     * @return string
     */
    public function getPhotoUrlAttribute($value)
    {
        return $value ? url(Storage::disk('public')->url($value)) : url('images/profile/user-default.png');
    }

    /**
     * Get all users following us.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(User::class, 'user_follower', 'follower_id', 'user_id')->withTimestamps();
    }

    /**
     * Get all users we are following.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function following()
    {
        return $this->belongsToMany(User::class, 'user_follower', 'user_id', 'follower_id')->withTimestamps();
    }

    /**
     * Get the posts for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * The post likes that belong to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function postLikes()
    {
        return $this->belongsToMany(Post::class, 'post_like', 'user_id', 'post_id')->withTimestamps();
    }

    /**
     * Get the files uploaded by the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }

    /**
     * Get the events for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    /**
     * Determine whether the user is following the friend.
     *
     * @param  \App\Models\User  $friend
     * @return boolean
     */
    public function isFollow(User $friend)
    {
        $result = $this->following()
            ->whereFollowerId($friend->id)
            ->first();

        return ! is_null($result);
    }

    /**
     * Determine whether the user is liked the post.
     *
     * @param  \App\Models\Post  $post
     * @return boolean
     */
    public function isLike(Post $post)
    {
        $result = $this->postLikes()
            ->wherePostId($post->id)
            ->first();

        return ! is_null($result);
    }

    /**
     * Determine whether the user can view the friend resource.
     *
     * @param  \App\Models\User  $friend
     * @return boolean
     */
    public function canView(User $friend)
    {
        $result = $this->following()
            ->whereFollowerId($friend->id)
            ->wherePivot('is_accepted', true)
            ->first();

        return ! is_null($result);
    }

    /**
     * Determine whether the user is visible for the friend.
     *
     * @param  \App\Models\User  $friend
     * @return boolean
     */
    public function isVisibleFor(User $friend)
    {
        $result = $this->followers()
            ->whereUserId($friend->id)
            ->wherePivot('is_accepted', true)
            ->first();

        return ! is_null($result);
    }

    /**
     * Determine whether the user has friendship request from the given friend.
     *
     * @param  \App\Models\User  $friend
     * @return boolean
     */
    public function hasFriendshipRequestFrom(User $friend)
    {
        $result = $this->followers()
            ->whereUserId($friend->id)
            ->wherePivot('is_accepted', null)
            ->first();

        return ! is_null($result);
    }

    /**
     * Determine whether the user is Super Administrator or Administrator.
     *
     * @return boolean
     */
    public function isSuperAdminOrAdmin()
    {
        return $this->isA('Super Administrator') || $this->isAn('Administrator');
    }

    /**
     * Determine whether the user is Super Administrator.
     *
     * @return boolean
     */
    public function isSuperAdmin()
    {
        return $this->isA('Super Administrator');
    }

    /**
     * Determine whether the user is an Administrator.
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->isAn('Administrator');
    }

    /**
     * Determine whether the user is an Event Organizer.
     *
     * @return boolean
     */
    public function isEventOrganizer()
    {
        return $this->userable_type === EventOrganizer::class;
    }

    /**
     * Determine whether the user is a Customer.
     *
     * @return boolean
     */
    public function isCustomer()
    {
        return $this->userable_type === Customer::class;
    }

    /**
     * Get the entity of userable.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function userable()
    {
        return $this->morphTo();
    }
}
