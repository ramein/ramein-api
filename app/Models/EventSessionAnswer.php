<?php

namespace App\Models;

use App\Models\Concerns\Fileable;
use Illuminate\Database\Eloquent\Model;

class EventSessionAnswer extends Model
{
    use Fileable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
    ];

    /**
     * Get the event session question for the event session answer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(EventSessionQuestion::class, 'event_session_question_id');
    }

    /**
     * Get the transaction for the event session answer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
