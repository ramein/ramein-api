<?php

namespace App\Models;

use App\Models\Concerns\Fileable;
use App\Models\Concerns\Reportable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Reportable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * The tags that belong to the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * The likes that belong to the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes()
    {
        return $this->belongsToMany(User::class, 'post_like', 'post_id', 'user_id')->withTimestamps();
    }

    /**
     * Get the user that make a post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the abilities for the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function abilities()
    {
        return $this->morphMany(Ability::class, 'entity');
    }

    /**
     * Get the post media.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function media()
    {
        return $this->morphOne(File::class, 'fileable');
    }

    /**
     * Scope a query to only include post that is visible for the user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \App\Models\User  $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisibleFor($query, User $user)
    {
        return $query->whereHas('user', function ($query) use ($user) {
            $query->where(function ($query) {
                $query->whereVisibility(true);
            })->orWhere(function ($query) use ($user) {
                $query->whereVisibility(false)
                    ->whereHas('posts', function ($query) use ($user) {
                        $query->whereHas('abilities', function ($query) use ($user) {
                            $query->whereHas('permissions', function ($query) use ($user) {
                                $query->whereEntityId($user->id)
                                    ->whereEntityType(User::class);
                            });
                        });
                    });
            });
        });
    }
}
