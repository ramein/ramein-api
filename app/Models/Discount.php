<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'amount',
        'is_percentile',
        'allow_multiple',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_percentile' => 'boolean',
        'allow_multiple' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'valid_from',
        'valid_until',
    ];

    /**
     * Get all of the event tickets that are assigned this discount.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function eventTickets()
    {
        return $this->morphedByMany(EventTicket::class, 'discountable');
    }

    /**
     * Get all of the transactions that are assigned this discount.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function transactions()
    {
        return $this->morphedByMany(Transaction::class, 'discountable');
    }
}
