<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_scanned' => 'boolean',
        'is_declined' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'scanned_on',
        'valid_from',
        'valid_until',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_valid',
    ];

    /**
     * Scope a query to only include ticket based on is_scanned column.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeScanned($query, $value = true)
    {
        return $query->where('is_scanned', $value);
    }

    /**
     * Scope a query to only include ticket based on is_declined column.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeclined($query, $value = true)
    {
        return $query->where('is_declined', $value);
    }

    /**
     * Scope a query to only include ticket for the given Customer.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyCustomer($query, Customer $customer)
    {
        return $query->whereHas('transaction', function ($query) use ($customer) {
            return $query->where('customer_id', $customer->id);
        });
    }

    /**
     * Scope a query to only include ticket that has been paid or not based on some criteria.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaid($query, $value = true)
    {
        return $query->whereHas('transaction', function ($query) use ($value) {
            $query->paid($value);
        });
    }

    /**
     * Scope a query to only include ticket that is active.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->whereHas('transaction', function ($query) {
            $query->paid();
        })
        ->where('is_scanned', false)
        ->where('is_declined', false)
        ->where('valid_until', '>=', Carbon::now());
    }

    /**
     * Scope a query to only include ticket where the transaction canceled or not.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCanceled($query, $value = true)
    {
        return $query->whereHas('transaction', function ($query) use ($value) {
            $query->paid(!$value);

            if ($value) {
                $query->where(
                    'created_at',
                    '<',
                    Carbon::now()->addDays(-Transaction::$maxPaidAfter)
                );
            }
        });
    }

    /**
     * Scope a query to only include ticket that has been used (scanned).
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUsed($query)
    {
        return $query->whereHas('transaction', function ($query) {
            $query->paid();
        })
        ->where('is_scanned', true)
        ->where('is_declined', false);
    }

    /**
     * Get the event session ticket for the ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventSessionTicket()
    {
        return $this->belongsTo(EventSessionTicket::class);
    }

    /**
     * Get the transaction for the ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    /**
     * Get customer that has the ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->transaction->customer();
    }

    /**
     * Determine whether the ticket is paid.
     *
     * @return boolean
     */
    public function isPaid()
    {
        return $this->transaction->is_paid;
    }

    /**
     * Determine whether the ticket is valid.
     *
     * @return boolean
     */
    public function isValid()
    {
        return $this->isPaid() && ! $this->is_scanned && ! $this->is_declined
            && $this->valid_until->gte(Carbon::now());
    }

    /**
     * Get is valid attribute.
     *
     * @return boolean
     */
    public function getIsValidAttribute()
    {
        return $this->isValid();
    }

    /**
     * Get the ticket relations that should be eager loaded.
     *
     * @return array
     */
    public static function shouldLoads()
    {
        return [
            'transaction',
        ];
    }
}
