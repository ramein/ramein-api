<?php

namespace App\Models;

use App\Models\Concerns\Userable;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    use Userable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];
}
