<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_verified' => 'boolean',
        'is_declined' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'paid_on',
    ];

    /**
     * Scope a query to only include payment based on is_verified column.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVerified($query, $value = true)
    {
        return $query->where('is_verified', $value);
    }

    /**
     * Scope a query to only include payment based on is_declined column.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeclined($query, $value = true)
    {
        return $query->where('is_declined', $value);
    }

    /**
     * Get the transaction for the payment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    /**
     * Get the destination bank for the payment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function destinationBank()
    {
        return $this->belongsTo(Bank::class);
    }

    /**
     * Get the customer that has the payment.
     *
     * @return \App\Models\Customer
     */
    public function customer()
    {
        return $this->transaction->customer;
    }

    /**
     * Determine whether the payment is verified.
     *
     * @return boolean
     */
    public function isVerified()
    {
        return $this->is_verified;
    }

    /**
     * Determine whether the payment is declined.
     *
     * @return boolean
     */
    public function isDeclined()
    {
        return $this->is_declined;
    }

    /**
     * Get the event session relations that should be eager loaded.
     *
     * @return array
     */
    public static function shouldLoads()
    {
        return [
            'transaction.customer',
        ];
    }
}
