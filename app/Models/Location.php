<?php

namespace App\Models;

use Geocoder\Laravel\Facades\Geocoder;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $googleMaps;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'longitude',
        'latitude',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'formatted_address',
        'country',
        'province',
        'district',
        'sub_district',
        'village',
        'street_name',
        'postal_code',
    ];

    /**
     * Get the entity of locatable.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function locatable()
    {
        return $this->morphTo();
    }

    /**
     * Get formatted address attribute.
     *
     * @return string|null
     */
    public function getFormattedAddressAttribute()
    {
        return $this->getLocation()['formatted_address'];
    }

    /**
     * Get country attribute.
     *
     * @return string|null
     */
    public function getCountryAttribute()
    {
        return $this->getLocation()['country'];
    }

    /**
     * Get province attribute.
     *
     * @return string|null
     */
    public function getProvinceAttribute()
    {
        return $this->getLocation()['province'];
    }

    /**
     * Get district attribute.
     *
     * @return string|null
     */
    public function getDistrictAttribute()
    {
        return $this->getLocation()['district'];
    }

    /**
     * Get sub district attribute.
     *
     * @return string|null
     */
    public function getSubDistrictAttribute()
    {
        return $this->getLocation()['sub_district'];
    }

    /**
     * Get village attribute.
     *
     * @return string|null
     */
    public function getVillageAttribute()
    {
        return $this->getLocation()['village'];
    }

    /**
     * Get street name attribute.
     *
     * @return string|null
     */
    public function getStreetNameAttribute()
    {
        return $this->getLocation()['street_name'];
    }

    /**
     * Get postal code attribute.
     *
     * @return string|null
     */
    public function getPostalCodeAttribute()
    {
        return $this->getLocation()['postal_code'];
    }

    /**
     * Get location using Google Maps API.
     *
     * @return array
     */
    protected function getLocation()
    {
        $maps = Geocoder::reverse($this->latitude, $this->longitude)->get()->first();

        if (! is_null($maps)) {
            $administrative = $maps->getAdminLevels()->all();
        }

        return [
            'formatted_address' => $maps ? $maps->getFormattedAddress() : null,
            'country' => $maps ? ($country = $maps->getCountry()) ? $country->getName() : null : null,
            'province' => isset($administrative[1]) ? $administrative[1]->getName() : null,
            'district' => isset($administrative[2]) ? $administrative[2]->getName() : null,
            'sub_district' => isset($administrative[3]) ? $administrative[3]->getName() : null,
            'village' => isset($administrative[4]) ? $administrative[4]->getName() : null,
            'street_name' => $maps ? $maps->getStreetName() : null,
            'postal_code' => $maps ? $maps->getPostalCode() : null,
        ];
    }
}
