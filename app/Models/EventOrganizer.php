<?php

namespace App\Models;

use App\Models\Concerns;
use Illuminate\Database\Eloquent\Model;

class EventOrganizer extends Model
{
    use Concerns\Bankable,
        Concerns\Userable,
        Concerns\Locatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
    ];

    /**
     * Always set and make type attribute as 'uppercase'.
     *
     * @param  string  $value
     * @return void
     */
    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = strtoupper($value);
    }
}
