<?php

namespace App\Models\Concerns;

use App\Models\User;

trait Userable
{
    /**
     * Get model's user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function user()
    {
        return $this->morphOne(User::class, 'userable');
    }

    /**
     * Scope a query to only include userable that is active.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->whereHas('user', function ($query) {
            return $query->where('status', User::$statuses['active'][0]);
        });
    }

    /**
     * Scope a query to only include userable that is banned.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBanned($query)
    {
        return $query->whereHas('user', function ($query) {
            return $query->where('status', User::$statuses['banned'][0]);
        });
    }
}
