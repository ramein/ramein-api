<?php

namespace App\Models\Concerns;

use App\Models\Location;

trait Locatable
{
    /**
     * Get model's location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function location()
    {
        return $this->morphOne(Location::class, 'locatable');
    }
}
