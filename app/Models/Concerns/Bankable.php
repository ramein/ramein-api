<?php

namespace App\Models\Concerns;

use App\Models\Bank;

trait Bankable
{
    /**
     * Get model's banks.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function banks()
    {
        return $this->morphMany(Bank::class, 'bankable')->withTimestamps();
    }
}
