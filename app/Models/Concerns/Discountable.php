<?php

namespace App\Models\Concerns;

use App\Models\Discount;

trait Discountable
{
    /**
     * Get model's discounts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function discounts()
    {
        return $this->morphToMany(Discount::class, 'discountable')
            ->withPivot(['min']);
    }
}
