<?php

namespace App\Models;

use App\Models\Concerns\Discountable;
use Illuminate\Database\Eloquent\Model;

class EventTicket extends Model
{
    use Discountable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'quota',
        'description',
        'auto_approve_ticket',
        'auto_hide_ticket',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'auto_approve_ticket' => 'boolean',
        'auto_hide_ticket' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'from',
        'until',
    ];

    /**
     * Scope query to only returned the event ticket that has price == 0 (free).
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFree($query)
    {
        return $query->wherePrice(0);
    }

    /**
     * Get all event sessions for the event ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function eventSessions()
    {
        return $this->belongsToMany(EventSession::class, 'event_session_tickets')
            ->orderBy('session');
    }

    /**
     * Get the event for the event ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * Get the ticket currency for the event ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(TicketCurrency::class, 'ticket_currency_id');
    }

    /**
     * Get the max refund on for the event ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function maxRefundOn()
    {
        return $this->belongsTo(TicketRefundRule::class, 'max_refund_on');
    }

    /**
     * Get all event session tickets for the event tickets.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventSessionTickets()
    {
        return $this->hasMany(EventSessionTicket::class);
    }

    /**
     * Get event session ticket for the event tickets.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function eventSessionTicket()
    {
        return $this->hasOne(EventSessionTicket::class);
    }

    /**
     * Get all tickets for the event tickets.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, EventSessionTicket::class);
    }

    /**
     * Get all tickets that's not canceled for the event tickets.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function ticketsNotCanceled()
    {
        return $this->hasManyThrough(Ticket::class, EventSessionTicket::class)
            ->canceled(false);
    }

    /**
     * Get the event ticket relations that should be eager loaded.
     *
     * @return array
     */
    public static function shouldLoads()
    {
        return [
            //
        ];
    }
}
