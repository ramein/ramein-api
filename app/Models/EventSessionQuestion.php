<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSessionQuestion extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'is_required',
        'need_attachment',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_required' => 'boolean',
        'need_attachment' => 'boolean',
    ];

    /**
     * Scope a query to only include event session question based on is_required column.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRequired($query, $value = true)
    {
        return $query->where('is_required', $value);
    }

    /**
     * Scope a query to only include event session question based on need_attachment column.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNeedAttachment($query, $value = true)
    {
        return $query->where('need_attachment', $value);
    }

    /**
     * Get the event session for the event session question.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventSession()
    {
        return $this->belongsTo(EventSession::class);
    }

    /**
     * Get all event session answers for the event session question.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(EventSessionAnswer::class);
    }
}
