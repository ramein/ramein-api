<?php

namespace App\Models;

use App\Models\Concerns\Locatable;
use Illuminate\Database\Eloquent\Model;

class EventSession extends Model
{
    use Locatable;

    /**
     * The ticket (bought) should have validity (before & after).
     *
     * @var integer
     */
    public static $ticketValidLength = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address',
        'details',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'auto_approve_ticket' => 'boolean',
        'show_map' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'from',
        'until',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'minimum_ticket',
    ];

    /**
     * Get the event for the event session.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * Get the event type for the event session.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(EventType::class, 'event_type_id');
    }

    /**
     * Get all event session questions for the event session.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(EventSessionQuestion::class);
    }

    /**
     * Get all event tickets for the event session.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function eventTickets()
    {
        return $this->belongsToMany(EventTicket::class, 'event_session_tickets');
    }

    /**
     * Get all event session tickets for the event session.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventSessionTickets()
    {
        return $this->hasMany(EventSessionTicket::class);
    }

    /**
     * Get all tickets for the event session.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, EventSessionTicket::class);
    }

    /**
     * Get minimum ticket attribute.
     *
     * @return array|null
     */
    public function getMinimumTicketAttribute()
    {
        // @TODO: Don't make like this.
        // This is a very bad approach. Very difficult to maintain.
        $eventTicket = $this->eventTickets->sortBy('price')->first();

        return $eventTicket ? [
            'price' => $eventTicket->price,
            'currency' => $eventTicket->currency,
        ] : null;
    }

    /**
     * Get the event session relations that should be eager loaded.
     *
     * @return array
     */
    public static function shouldLoads()
    {
        return [
            'eventTickets.currency',
        ];
    }
}
