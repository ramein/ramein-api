<?php

namespace App\Models\Database\Concerns;

use App\Models\Database\Relations\HasManyThroughBelongsTo;

trait HasManyThroughBelongsToRelation
{
    /**
     * Define a hash-many-through-belongs-to relationship.
     *
     * @param  string  $related
     * @param  string  $through
     * @param  string  $firstKey
     * @param  string  $secondKey
     * @return \App\Models\Database\Relations\HasManyThroughBelongsTo
     */
    public function hasManyThroughBelongsTo($related, $through, $firstKey = null, $secondKey = null)
    {
        $through = new $through;
        $related = new $related;

        $firstKey  = $firstKey ?: $this->getForeignKey();
        $secondKey = $secondKey ?: $related->getForeignKey();

        return new HasManyThroughBelongsTo($related->newQuery(), $this, $through, $firstKey, $secondKey);
    }
}
