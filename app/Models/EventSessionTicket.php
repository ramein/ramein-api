<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSessionTicket extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the event ticket for the event session ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventTicket()
    {
        return $this->belongsTo(EventTicket::class);
    }

    /**
     * Get the event session for the event session ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventSession()
    {
        return $this->belongsTo(EventSession::class);
    }

    /**
     * Get the tickets for the event session ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
