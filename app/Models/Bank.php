<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_name',
        'bank_number',
        'bank_holder_name',
    ];

    /**
     * Get the entity of bankable.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function bankable()
    {
        return $this->morphTo();
    }
}
