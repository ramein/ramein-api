<?php

namespace App\Models;

use App\Models\Concerns;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use Concerns\Bankable,
        Concerns\Userable,
        Concerns\Locatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * Get all payments made by the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function payments()
    {
        return $this->hasManyThrough(Payment::class, Transaction::class);
    }

    /**
     * Get all tickets for the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, Transaction::class);
    }

    /**
     * Get all transactions for the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
