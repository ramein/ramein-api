<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Concerns\Discountable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Database\Concerns\HasManyThroughBelongsToRelation;

class Transaction extends Model
{
    use Discountable,
        HasManyThroughBelongsToRelation;

    /**
     * The maximum days the transaction must be marked as canceled.
     *
     * @var integer
     */
    public static $maxPaidAfter = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_paid' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_canceled',
        'valid_until',
    ];

    /**
     * Get the transaction is canceled attribute.
     *
     * @return boolean
     */
    public function getIsCanceledAttribute()
    {
        return ! $this->is_paid && Carbon::now()->gt(Carbon::parse($this->valid_until));
    }

    /**
     * Get the transaction valid until attribute.
     *
     * @return string
     */
    public function getValidUntilAttribute()
    {
        return $this->created_at->addDays(self::$maxPaidAfter)->format($this->getDateFormat());
    }

    /**
     * Scope a query to only include transaction that has been paid.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  boolean  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaid($query, $value = true)
    {
        return $query->whereIsPaid($value);
    }

    /**
     * Get the customer that has the transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Get all tickets for the transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * Get all payments for the transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * Get all event session tickets for the transaction.
     *
     * @return \App\Models\Database\Relations\HasManyThroughBelongsTo
     */
    public function eventSessionTickets()
    {
        return $this->hasManyThroughBelongsTo(EventSessionTicket::class, Ticket::class);
    }

    /**
     * Get all payments, verified and not declined for the transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function validPayments()
    {
        return $this->hasMany(Payment::class)
            ->verified()
            ->declined(false);
    }
}
