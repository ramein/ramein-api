<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketRefundRule extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'name',
    ];

    /**
     * Get name attribute.
     *
     * @return string|null
     */
    public function getNameAttribute()
    {
        return null;
    }

    /**
     * Get all event tickets for the rules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventTickets()
    {
        return $this->hasMany(EventTicket::class, 'max_refund_on');
    }
}
