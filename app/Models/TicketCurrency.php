<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketCurrency extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'symbol',
        'abbr',
    ];

    /**
     * Always set and make name attribute as 'ucwords'.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords(strtolower($value));
    }

    /**
     * Always set and make abbr attribute as 'uppercase'.
     *
     * @param  string  $value
     * @return void
     */
    public function setAbbrAttribute($value)
    {
        $this->attributes['abbr'] = strtoupper($value);
    }

    /**
     * Get all event session tickets for the currency.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessionTickets()
    {
        return $this->hasMany(EventTicket::class);
    }
}
