<?php

namespace App\Mail\Transaction;

use App\Models\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransactionPaidMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Models\Transaction
     */
    public $transaction;

    /**
     * Create a new message instance.
     *
     * @param  \App\Models\Transaction  $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->transaction->customer->user->email)
            ->subject('Transaksi ' . $this->transaction->name . ' berhasil diselesaikan.')
            ->markdown('emails.transactions.paid');
    }
}
