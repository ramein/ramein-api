<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->unsignedBigInteger('ticket_currency_id');
            $table->foreign('ticket_currency_id')->references('id')->on('ticket_currencies')->onDelete('cascade');
            $table->string('name');
            $table->integer('price')->default(0);
            $table->integer('quota');
            $table->integer('stock');
            $table->text('description')->nullable();
            $table->boolean('auto_approve_ticket')->default(true);
            $table->boolean('auto_hide_ticket')->default(true);
            $table->unsignedBigInteger('max_refund_on')->nullable();
            $table->foreign('max_refund_on')->references('id')->on('ticket_refund_rules')->onDelete('cascade');
            $table->timestamp('from')->useCurrent();
            $table->timestamp('until')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_tickets');
    }
}
