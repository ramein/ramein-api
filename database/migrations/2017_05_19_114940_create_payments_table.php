<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->unsignedBigInteger('transaction_id');
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->unsignedBigInteger('destination_bank_id')->nullable();
            $table->foreign('destination_bank_id')->references('id')->on('banks')->onDelete('cascade');
            $table->string('provider')->nullable();
            $table->string('provider_number')->nullable();
            $table->string('provider_holder_name')->nullable();
            $table->integer('amount')->default(0);
            $table->text('description')->nullable();
            $table->boolean('is_verified')->default(false);
            $table->boolean('is_declined')->default(false);
            $table->timestamp('paid_on')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
