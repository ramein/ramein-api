<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSessionTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_session_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('event_session_id');
            $table->foreign('event_session_id')->references('id')->on('event_sessions')->onDelete('cascade');
            $table->unsignedBigInteger('event_ticket_id');
            $table->foreign('event_ticket_id')->references('id')->on('event_tickets')->onDelete('cascade');
            $table->index(['event_session_id', 'event_ticket_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_session_tickets');
    }
}
