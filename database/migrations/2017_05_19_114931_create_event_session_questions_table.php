<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSessionQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_session_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('event_session_id');
            $table->foreign('event_session_id')->references('id')->on('event_sessions')->onDelete('cascade');
            $table->string('value');
            $table->boolean('is_required')->default(false);
            $table->boolean('need_attachment')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_session_questions');
    }
}
