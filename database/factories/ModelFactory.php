<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'username' => $faker->username,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = '12345678',
    ];
});

$factory->state(App\Models\User::class, 'active', function (Faker\Generator $faker) {
    return [
        'status' => App\Models\User::statuses()->get('active'),
    ];
});

$factory->state(App\Models\User::class, 'banned', function (Faker\Generator $faker) {
    return [
        'status' => App\Models\User::statuses()->get('banned'),
    ];
});

$factory->state(App\Models\User::class, 'pending', function (Faker\Generator $faker) {
    return [
        'activation_token' => app(App\Services\UserService::class)->generateActivationToken(),
        'status' => App\Models\User::statuses()->get('pending'),
    ];
});

$factory->define(App\Models\Owner::class, function (Faker\Generator $faker) {
    return [
        //
    ];
});

$factory->define(App\Models\Customer::class, function (Faker\Generator $faker) {
    return [
        //
    ];
});

$factory->define(App\Models\EventOrganizer::class, function (Faker\Generator $faker) {
    return [
        'type' => $faker->randomElement(['FREE', 'PREMIUM']),
    ];
});

$factory->state(App\Models\EventOrganizer::class, 'free', function (Faker\Generator $faker) {
    return [
        'type' => 'FREE',
    ];
});

$factory->state(App\Models\EventOrganizer::class, 'premium', function (Faker\Generator $faker) {
    return [
        'type' => 'PREMIUM',
    ];
});

$factory->define(App\Models\Report::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->paragraph,
        'user_id' => App\Models\User::active()->get()->random()->id,
    ];
});

$factory->define(App\Models\File::class, function (Faker\Generator $faker) {
    return [
        'user_id' => App\Models\User::active()->get()->random()->id,
        'name' => $faker->unique()->word,
        'location' => (new Illuminate\Http\UploadedFile(
            public_path('images/default.png'),
            'default.png',
            'image/png',
            filesize(public_path('images/default.png'))
        ))->store('default', 'public'),
    ];
});

$factory->define(App\Models\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});

$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->paragraph,
        'user_id' => App\Models\User::active()->get()->random()->id,
    ];
});

$factory->define(App\Models\Location::class, function (Faker\Generator $faker) {
    return [
        'longitude' => '110.103393',
        'latitude' => '-7.88706',
    ];
});

$factory->define(App\Models\EventCategory::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});

$factory->define(App\Models\EventType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});

$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {
    return [
        'code' => app(App\Services\EventService::class)->generateCode(),
        'user_id' => App\Models\User::active()->get()->random()->id,
        'title' => $faker->words(8, true),
        'event_category_id' => App\Models\EventCategory::all()->random()->id,
    ];
});

$factory->define(App\Models\EventSession::class, function (Faker\Generator $faker) {
    return [
        'event_id' => App\Models\Event::all()->random()->id,
        'session' => $faker->numberBetween(1, 200),
        'address' => $faker->address,
        'details' => $faker->paragraph,
        'from' => Carbon\Carbon::now()->addDays(rand(1, 30)),
        'until' => Carbon\Carbon::now()->addWeeks(rand(1, 52)),
        'event_type_id' => App\Models\EventType::all()->random()->id,
    ];
});

$factory->define(App\Models\TicketCurrency::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->name,
        'symbol' => substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), -3),
        'abbr' => substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), -3),
    ];
});

$factory->define(App\Models\TicketRefundRule::class, function (Faker\Generator $faker) {
    return [
        'value' => $faker->unique()->numberBetween(1, 30),
    ];
});

$factory->define(App\Models\EventTicket::class, function (Faker\Generator $faker) {
    return [
        'event_id' => App\Models\Event::all()->random()->id,
        'ticket_currency_id' => App\Models\TicketCurrency::all()->random()->id,
        'name' => 'Ticket ' . $faker->unique()->uuid,
        'quota' => $faker->numberBetween(10, 200),
        'price' => $faker->randomElement([100000, 500000, 1000000, 10000000]),
        'auto_approve_ticket' => $faker->boolean(60),
        'auto_hide_ticket' => $faker->boolean(60),
        'from' => Carbon\Carbon::now()->addDays(rand(1, 30)),
        'until' => Carbon\Carbon::now()->addWeeks(rand(1, 52)),
        'max_refund_on' => App\Models\TicketRefundRule::all()->random()->id,
    ];
});

$factory->define(App\Models\EventSessionQuestion::class, function (Faker\Generator $faker) {
    return [
        'event_session_id' => App\Models\EventSession::all()->random()->id,
        'value' => $faker->paragraph(2) . '?',
        'is_required' => $faker->boolean(60),
        'need_attachment' => $faker->boolean(20),
    ];
});

$factory->define(App\Models\Transaction::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->unique()->uuid,
        'name' => 'TRX/RAMEIN/'. App\Models\Customer::active()->get()->random()->id . '/' . Carbon\Carbon::now()->timestamp . '/' . $faker->unique()->uuid,
        'customer_id' => App\Models\Customer::active()->get()->random()->id,
    ];
});

$factory->define(App\Models\EventSessionAnswer::class, function (Faker\Generator $faker) {
    return [
        'event_session_question_id' => App\Models\EventSessionQuestion::all()->random()->id,
        'transaction_id' => App\Models\Transaction::all()->random()->id,
        'value' => $faker->paragraph,
    ];
});

$factory->define(App\Models\Ticket::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->unique()->uuid,
        'event_session_ticket_id' => App\Models\EventSessionTicket::all()->random()->id,
        'transaction_id' => App\Models\Transaction::all()->random()->id,
        'valid_from' => Carbon\Carbon::now()->addDays(rand(1, 30)),
        'valid_until' => Carbon\Carbon::now()->addWeeks(rand(1, 52)),
        'is_scanned' => $faker->boolean(20),
        'is_declined' => $faker->boolean(20),
    ];
});

$factory->state(App\Models\Ticket::class, 'scanned', function (Faker\Generator $faker) {
    return [
        'is_scanned' => true,
        'scanned_on' => Carbon\Carbon::now(),
    ];
});

$factory->state(App\Models\Ticket::class, 'declined', function (Faker\Generator $faker) {
    return [
        'is_declined' => true,
    ];
});

$factory->define(App\Models\Payment::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->unique()->uuid,
        'name' => 'Payment ' . $faker->unique()->uuid,
        'transaction_id' => App\Models\Transaction::all()->random()->id,
        'amount' => $faker->randomElement([100000, 500000, 1000000, 10000000]),
        'is_verified' => $faker->boolean(70),
        'is_declined' => $faker->boolean(20),
    ];
});

$factory->state(App\Models\Payment::class, 'verified', function (Faker\Generator $faker) {
    return [
        'is_verified' => true,
    ];
});

$factory->state(App\Models\Payment::class, 'declined', function (Faker\Generator $faker) {
    return [
        'is_declined' => true,
    ];
});
