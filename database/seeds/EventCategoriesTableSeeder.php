<?php

use App\Models\EventCategory;
use Illuminate\Database\Seeder;

class EventCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EventCategory::class)->create(['name' => 'Sport']);
        factory(EventCategory::class)->create(['name' => 'Food & Drink']);
        factory(EventCategory::class)->create(['name' => 'Programming']);

        factory(EventCategory::class, 20)->create();
    }
}
