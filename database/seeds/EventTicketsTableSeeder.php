<?php

use App\Models\Event;
use App\Models\EventTicket;
use Illuminate\Database\Seeder;

class EventTicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::all()->each(function ($event) {
            $event->sessions->each(function ($eventSession) use ($event) {
                $eventSession->eventTickets()->attach(factory(EventTicket::class)->create([
                    'event_id' => $event->id,
                ]));
            });
        });

        factory(EventTicket::class, 20)->create()->each(function ($eventTicket) {
            $eventTicket->eventSessions()->attach($eventTicket->event->sessions->random());
        });
    }
}
