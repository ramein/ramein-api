<?php

use App\Models\File;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereIsNot(['Administrator'])
            ->inRandomOrder()
            ->limit(10)
            ->get();

        $users->each(function ($user) {
            factory(Post::class, 2)->create()
                ->each(function ($post) use ($user) {
                    $post->media()->save(factory(File::class)->make([
                        'user_id' => $user->id,
                        'location' => (new UploadedFile(
                            public_path('images/post/default.jpg'),
                            'default.png',
                            'image/png',
                            filesize(public_path('images/post/default.jpg'))
                        ))->store('default/post', 'public'),
                    ]));

                    $user->posts()->save($post);
                });
        });
    }
}
