<?php

use Illuminate\Database\Seeder;
use App\Models\TicketRefundRule;

class TicketRefundRulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TicketRefundRule::class)->create([
            'value' => 7,
        ]);

        factory(TicketRefundRule::class)->create([
            'value' => 10,
        ]);

        factory(TicketRefundRule::class)->create([
            'value' => 30,
        ]);
    }
}
