<?php

use App\Models\{Report, User};
use Illuminate\Database\Seeder;

class ReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereIsNot(['Administrator'])
            ->inRandomOrder()
            ->limit(10)
            ->get();

        $users->each(function ($user) {
            $user->reports()->saveMany(factory(Report::class, 2)->make());
        });
    }
}
