<?php

use Carbon\Carbon;
use App\Models\File;
use App\Models\User;
use App\Models\Event;
use App\Models\Location;
use App\Models\EventType;
use App\Models\EventTicket;
use App\Models\EventSession;
use App\Models\EventOrganizer;
use App\Models\TicketCurrency;
use App\Services\EventService;
use Illuminate\Database\Seeder;
use App\Models\TicketRefundRule;
use Illuminate\Http\UploadedFile;

class PreliminaryEventSeeder extends Seeder
{
    /**
     * @var  \App\Services\EventService
     */
    protected $eventService;

    /**
     * Constructor.
     *
     * @param  \App\Services\EventService  $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // @EventOrganizer
        $user = factory(User::class)->states('active')->make([
            'name' => 'I\'m Ramein',
            'username' => 'iamramein',
            'email' => 'hi@ramein.id',
            'password' => '12345678',
        ]);

        factory(EventOrganizer::class)->create()->user()->save($user);

        // @Event
        $event = factory(Event::class)->create([
            'code' => $this->eventService->generateCode(),
            'user_id' => $user->id,
            'title' => 'Google I/O Extended 2017 Jogjakarta Recap',
        ]);

        $event->poster()->save(factory(File::class)->make([
            'user_id' => $event->user->id,
            'location' => (new UploadedFile(
                public_path('images/event/default.jpg'),
                'default.png',
                'image/png',
                filesize(public_path('images/event/default.jpg'))
            ))->store('default/event', 'public'),
        ]));

        // @EventSession
        $eventSession = factory(EventSession::class)->create([
            'event_id' => $event->id,
            'session' => 0,
            'address' => 'Entrepreneur Development Service, UGM',
            'details' => "" .
"Google I/O Extended 2017 Jogjakarta Recap is a day of inspirational talks and codelabs about Google's latest developer products released recently held Google I/O 2017 at Mountain View, California.<br><br>" .
"To inspire people to be able to innovate and to create, GDG Jogjakarta wants to share the latest technology from Google, share stories, and also network and mingle with the community.<br>" .
"<br><br>We will invite these mentors to give talks and technical workshops on Kotlin:<br><br>" .
"1. Deny Prasetyo, Kotlin and Container Technology Enthusiast (linkedin)<br><br>" .
"2. Pratama Wijaya, Kotlin User Group Indonesia - @KotlinID (linkedin)<br><br><br><br>" .
"Please bring your laptop / notebook with these installed:<br><br>1. Android Studio<br>2. Kotlin compiler<br><br><br>" .
"Note: We have a limit of 30 slot attendees. So we're selecting members by your answers and confirmation of attendance. Members in the waiting list, you still have a chance of attending :)",
            'from' => Carbon::now()->addDays(10)->startOfDay()->addHours(9),
            'until' => Carbon::now()->addDays(10)->startOfDay()->addYear(),
            'event_type_id' => EventType::firstOrCreate([ 'name' => 'Workshop' ])->id,
        ]);

        // @Location
        $eventSession->location()->save(factory(Location::class)->make([
            'longitude' => '110.3735013',
            'latitude' => '-7.771056',
        ]));

        $event->sessions()->save($eventSession);

        // @EventTicket
        $eventSession->eventTickets()->attach(factory(EventTicket::class)->create([
            'event_id' => $event->id,
            'ticket_currency_id' => TicketCurrency::firstOrCreate([
                'name' => 'Indonesian',
                'symbol' => 'Rp',
                'abbr' => 'IDR',
            ]),
            'name' => 'Free Ticket',
            'quota' => 30,
            'price' => 0,
            'auto_approve_ticket' => true,
            'auto_hide_ticket' => true,
            'from' => $eventSession->from->subDays(10)->startOfDay(),
            'until' => $eventSession->from->subDays(10)->endOfDay(),
            'max_refund_on' => TicketRefundRule::firstOrCreate([ 'value' => 7 ]),
        ]));
    }
}
