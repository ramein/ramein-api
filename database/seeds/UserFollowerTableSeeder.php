<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserFollowerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereIsNot(['Administrator'])->get();

        $users->each(function ($user) {
            foreach (range(1, 10) as $item) {
                $user->following()->save(
                    User::whereIsNot(['Administrator'])
                        ->where('id', '!=', $user->id)
                        ->whereNotIn('id', $user->following->pluck('id')->toArray())
                        ->inRandomOrder()
                        ->first()
                );
            }
        });
    }
}
