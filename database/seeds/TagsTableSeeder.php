<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tag::class)->create(['name' => '#dotdev']);
        factory(Tag::class)->create(['name' => '#seminar']);
        factory(Tag::class)->create(['name' => '#event']);

        factory(Tag::class, 20)->create();
    }
}
