<?php

use App\Models\Event;
use App\Models\Location;
use App\Models\EventSession;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class EventSessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Another Random EventSession
        Event::inRandomOrder()->take(10)->get()->each(function ($event) {
            $eventSessions = factory(EventSession::class, 2)->create()->each(function ($eventSession) {
                $eventSession->location()->save(factory(Location::class)->make());
            });

            $event->sessions()->saveMany($eventSessions);
        });
    }
}
