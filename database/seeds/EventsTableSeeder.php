<?php

use App\Models\File;
use App\Models\Event;
use App\Models\Location;
use App\Models\EventSession;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 20)->create()->each(function ($event) {
            $event->poster()->save(factory(File::class)->make([
                'user_id' => $event->user->id,
                'location' => (new UploadedFile(
                    public_path('images/event/default.jpg'),
                    'default.png',
                    'image/png',
                    filesize(public_path('images/event/default.jpg'))
                ))->store('default/event', 'public'),
            ]));

            // Required Event Session
            $eventSession = factory(EventSession::class)->create([
                'session' => 0,
            ]);
            $event->sessions()->save($eventSession);
            $eventSession->location()->save(factory(Location::class)->make());
        });
    }
}
