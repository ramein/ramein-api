<?php

use App\Models\Ticket;
use App\Models\Transaction;
use Illuminate\Database\Seeder;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::all()->each(function ($transaction) {
            $tickets = factory(Ticket::class, rand(1, 5))->create();

            $tickets->each(function ($ticket) {
                if ($ticket->is_scanned) {
                    $ticket->update([
                        'scanned_on' => $ticket->valid_until->addDays(-2),
                    ]);
                }
            });

            $transaction->tickets()->saveMany($tickets);
        });
    }
}
