<?php

use App\Models\User;
use App\Models\Owner;
use App\Models\EventType;
use App\Models\EventCategory;
use App\Services\UserService;
use App\Models\TicketCurrency;
use Illuminate\Database\Seeder;
use App\Models\TicketRefundRule;
use Illuminate\Support\Facades\Artisan;
use Silber\Bouncer\BouncerFacade as Bouncer;

class PreliminaryDataSeeder extends Seeder
{
    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Constructor.
     *
     * @param  \App\Services\UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('bouncer:seed');
        Bouncer::allow('Super Administrator')->everything();
        $this->call(PassportSeeder::class);

        factory(Owner::class)->create()->user()->save(
            factory(User::class)->states('active')->make([
                'name' => 'Administrator',
                'username' => 'admin123',
                'email' => 'dev@ramein.id',
                'password' => '12345678',
            ])
        );

        $this->userService->assignSuperAdminRole(User::whereEmail('dev@ramein.id')->first());

        factory(Owner::class)->create()->user()->save(
            factory(User::class)->states('active')->make([
                'name' => 'Nabil Muhammad Firdaus',
                'username' => 'nmfzone',
                'email' => 'nabilftd@gmail.com',
                'password' => '12345678',
            ])
        );

        // Ticket Refund Rule
        TicketRefundRule::firstOrCreate([ 'value' => 7 ]);
        TicketRefundRule::firstOrCreate([ 'value' => 10 ]);
        TicketRefundRule::firstOrCreate([ 'value' => 30 ]);

        // Ticket Currency
        TicketCurrency::firstOrCreate([
            'name' => 'Indonesian',
            'symbol' => 'Rp',
            'abbr' => 'IDR',
        ]);
        TicketCurrency::firstOrCreate([
            'name' => 'U.S. Dollars',
            'symbol' => '$',
            'abbr' => 'USD',
        ]);
        TicketCurrency::firstOrCreate([
            'name' => 'Euros',
            'symbol' => '€',
            'abbr' => 'EUR',
        ]);

        // Event Category
        EventCategory::firstOrCreate([ 'name' => 'Sport' ]);
        EventCategory::firstOrCreate([ 'name' => 'Food & Drink' ]);
        EventCategory::firstOrCreate([ 'name' => 'Programming' ]);

        // Event Type
        EventType::firstOrCreate([ 'name' => 'Seminar' ]);
        EventType::firstOrCreate([ 'name' => 'Workshop' ]);
        EventType::firstOrCreate([ 'name' => 'Sharing' ]);
        EventType::firstOrCreate([ 'name' => 'Contest' ]);
    }
}
