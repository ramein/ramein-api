<?php

use App\Models\User;
use App\Models\Owner;
use App\Services\UserService;
use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade as Bouncer;

class OwnersTableSeeder extends Seeder
{
    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Constructor.
     *
     * @param  \App\Services\UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::allow('Super Administrator')->everything();

        factory(Owner::class)->create()->user()->save(
            factory(User::class)->states('active')->make([
                'name' => 'Super Administrator',
                'username' => 'admin',
                'email' => 'admin@example.com',
                'password' => '12345678',
            ])
        );

        $this->userService->assignSuperAdminRole(User::whereEmail('admin@example.com')->first());

        // Active
        factory(Owner::class)->create()->user()->save(
            factory(User::class)->states('active')->make([
                'name' => 'Owner Active',
                'username' => 'owner.active',
                'email' => 'owner.active@example.com',
                'password' => '12345678',
            ])
        );
        // Banned
        factory(Owner::class)->create()->user()->save(
            factory(User::class)->states('banned')->make([
                'name' => 'Owner Banned',
                'username' => 'owner.banned',
                'email' => 'owner.banned@example.com',
                'password' => '12345678',
            ])
        );
        // Pending
        factory(Owner::class)->create()->user()->save(
            factory(User::class)->states('pending')->make([
                'name' => 'Owner Pending',
                'username' => 'owner.pending',
                'email' => 'owner.pending@example.com',
                'password' => '12345678',
            ])
        );
    }
}
