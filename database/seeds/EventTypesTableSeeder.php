<?php

use App\Models\EventType;
use Illuminate\Database\Seeder;

class EventTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EventType::class)->create(['name' => 'Seminar']);
        factory(EventType::class)->create(['name' => 'Workshop']);
        factory(EventType::class)->create(['name' => 'Sharing']);
        factory(EventType::class)->create(['name' => 'Contest']);
    }
}
