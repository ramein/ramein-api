<?php

use App\Models\User;
use App\Models\Customer;
use Illuminate\Database\Seeder;
use App\Services\TransactionService;

class PreliminaryTransactionSeeder extends Seeder
{
    /**
     * @var  \App\Services\TransactionService
     */
    protected $transactionService;

    /**
     * Constructor.
     *
     * @param  \App\Services\TransactionService  $transactionService
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eo = User::whereEmail('hi@ramein.id')->first();

        $eventSessionTicket = $eo->events()
            ->first()
            ->sessions()
            ->first()
            ->eventTickets()
            ->first()
            ->eventSessionTicket;

        // @Customer
        $user = factory(User::class)->states('active')->make([
            'name' => 'I\'m Customer',
            'username' => 'iamcustomer',
            'email' => 'info@ramein.id',
            'password' => '12345678',
        ]);

        factory(Customer::class)->create()->user()->save($user);

        $customer = $user->userable;

        $this->transactionService->create(collect([
            [
                'event_session_ticket' => $eventSessionTicket->id,
                'quantity' => 1,
            ]
        ]), $customer);
    }
}
