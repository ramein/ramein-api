<?php

use App\Models\User;
use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Active
        factory(Customer::class)->create()->user()->save(
            factory(User::class)->states('active')->make([
                'name' => 'Customer Active',
                'username' => 'customer.active',
                'email' => 'customer.active@example.com',
                'password' => '12345678',
            ])
        );
        // Banned
        factory(Customer::class)->create()->user()->save(
            factory(User::class)->states('banned')->make([
                'name' => 'Customer Banned',
                'username' => 'customer.banned',
                'email' => 'customer.banned@example.com',
                'password' => '12345678',
            ])
        );
        // Pending
        factory(Customer::class)->create()->user()->save(
            factory(User::class)->states('pending')->make([
                'name' => 'Customer Pending',
                'username' => 'customer.pending',
                'email' => 'customer.pending@example.com',
                'password' => '12345678',
            ])
        );

        factory(Customer::class, 10)->create()->each(function ($customer) {
            $customer->user()->save(factory(User::class)->states('active')->make());
        });
    }
}
