<?php

use App\Models\Payment;
use App\Models\Transaction;
use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Full Paid
        Transaction::take(10)->get()->each(function ($transaction) {
            $transaction->payments()->save(
                factory(Payment::class)->create(['amount' => $transaction->total])
            );
        });

        // Random
        Transaction::skip(10)->take(20)->get()->each(function ($transaction) {
            $transaction->payments()->saveMany(
                factory(Payment::class, rand(1, 10))->create()
            );
        });
    }
}
