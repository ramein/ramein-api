<?php

use App\Models\User;
use App\Models\EventOrganizer;
use Illuminate\Database\Seeder;

class EventOrganizersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Active
        // Free
        factory(EventOrganizer::class)->states('free')->create()->user()->save(
            factory(User::class)->states('active')->make([
                'name' => 'Free Event Organizer',
                'username' => 'eo.free',
                'email' => 'eo.free@example.com',
                'password' => '12345678',
            ])
        );
        // Premium
        factory(EventOrganizer::class)->states('premium')->create()->user()->save(
            factory(User::class)->states('active')->make([
                'name' => 'Premium Event Organizer',
                'username' => 'eo.premium',
                'email' => 'eo.premium@example.com',
                'password' => '12345678',
            ])
        );

        // Banned
        factory(EventOrganizer::class)->create()->user()->save(
            factory(User::class)->states('banned')->make([
                'name' => 'Event Organizer Banned',
                'username' => 'eo.banned',
                'email' => 'eo.banned@example.com',
                'password' => '12345678',
            ])
        );
        // Pending
        factory(EventOrganizer::class)->create()->user()->save(
            factory(User::class)->states('pending')->make([
                'name' => 'Event Organizer Pending',
                'username' => 'eo.pending',
                'email' => 'eo.pending@example.com',
                'password' => '12345678',
            ])
        );

        factory(EventOrganizer::class, 10)->create()->each(function ($eventOrganizer) {
            $eventOrganizer->user()->save(factory(User::class)->states('active')->make());
        });
    }
}
