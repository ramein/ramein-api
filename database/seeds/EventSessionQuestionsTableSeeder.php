<?php

use App\Models\EventSession;
use Illuminate\Database\Seeder;
use App\Models\EventSessionQuestion;

class EventSessionQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventSession::inRandomOrder()->take(10)->get()->each(function ($eventSession) {
            $eventSession->questions()->saveMany(
                factory(EventSessionQuestion::class, rand(1, 5))->create()
            );
        });
    }
}
