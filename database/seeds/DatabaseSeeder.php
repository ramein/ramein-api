<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('bouncer:seed');
        File::deleteDirectory(storage_path('app/public/default'));

        Mail::fake();

        $this->call(PassportSeeder::class);
        $this->call(OwnersTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(EventOrganizersTableSeeder::class);
        $this->call(UserFollowerTableSeeder::class);
        $this->call(ReportsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(EventCategoriesTableSeeder::class);
        $this->call(EventTypesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(EventSessionsTableSeeder::class);
        $this->call(EventSessionQuestionsTableSeeder::class);
        $this->call(TicketCurrenciesTableSeeder::class);
        $this->call(TicketRefundRulesTableSeeder::class);
        $this->call(EventTicketsTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(TicketsTableSeeder::class);
        $this->call(EventSessionAnswersTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(PreliminaryDataSeeder::class);
        $this->call(PreliminaryEventSeeder::class);
        $this->call(PreliminaryTransactionSeeder::class);
    }
}
