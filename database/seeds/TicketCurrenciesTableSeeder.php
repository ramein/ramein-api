<?php

use App\Models\TicketCurrency;
use Illuminate\Database\Seeder;

class TicketCurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TicketCurrency::class)->create([
            'name' => 'Indonesian',
            'symbol' => 'Rp',
            'abbr' => 'IDR',
        ]);

        factory(TicketCurrency::class)->create([
            'name' => 'U.S. Dollars',
            'symbol' => '$',
            'abbr' => 'USD',
        ]);

        factory(TicketCurrency::class)->create([
            'name' => 'Euros',
            'symbol' => '€',
            'abbr' => 'EUR',
        ]);
    }
}
