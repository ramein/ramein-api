<?php

use App\Models\File;
use App\Models\Ticket;
use Illuminate\Database\Seeder;
use App\Models\EventSessionAnswer;

class EventSessionAnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ticket::all()->each(function ($ticket) {
            $ticket->eventSessionTicket->eventSession->questions()->required()->get()->each(function ($question) use ($ticket) {
                $answer = factory(EventSessionAnswer::class)->create([
                    'event_session_question_id' => $question->id,
                    'transaction_id' => $ticket->transaction->id,
                ]);

                if ($question->need_attachment) {
                    $answer->file()->save(factory(File::class)->make());
                }
            });
        });
    }
}
