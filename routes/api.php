<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "auth" middleware group. Enjoy building your API!
|
*/

$app->post('/logout', 'Auth\LoginController@logout');

$app->group(['prefix' => 'v1', 'middleware' => ['debugbar', 'dev_api']], function ($app) {
    // Tags
    rest($app, '/tags', 'TagsController', 'tag', 'Api', ['only' => ['all', 'get']]);

    // Event Categories
    rest($app, '/event-categories', 'EventCategoriesController', 'eventCategory', 'Api', ['only' => ['all', 'get']]);

    // Event Types
    rest($app, '/event-types', 'EventTypesController', 'eventType', 'Api', ['only' => ['all', 'get']]);

    // Events
    $app->get('/events', 'Api\EventsController@entireEvents');
    $app->get('/events/scan', 'Api\EventsController@scanning');
    rest($app, '/users/{user}/events', 'EventsController', 'event', 'Api', ['only' => ['all', 'get']]);

    // Event Sessions
    rest($app, '/users/{user}/events/{event}/sessions', 'EventSessionsController', 'eventSession', 'Api', ['only' => ['get']]);

    $app->group(['middleware' => 'auth:api'], function ($app) {
        // Friendships
        $app->post('/users/friendships/{friend}', 'Api\UserFriendshipsController@store');
        $app->post('/users/friendships/{friend}/accept', 'Api\UserFriendshipsController@accept');
        $app->post('/users/friendships/{friend}/reject', 'Api\UserFriendshipsController@reject');
        $app->delete('/users/friendships/{friend}', 'Api\UserFriendshipsController@destroy');

        // Posts
        $app->get('/posts', 'Api\PostsController@entirePosts');
        $app->post('/users/{user}/posts/{post}/like', 'Api\PostsController@like');
        $app->post('/users/{user}/posts/{post}/dislike', 'Api\PostsController@dislike');
        rest($app, '/users/{user}/posts', 'PostsController', 'post', 'Api');

        // Users
        $app->get('/user', 'Api\UsersController@current');
        $app->get('/users/{user}/followers', 'Api\UserFollowersController@get');
        $app->get('/users/{user}/following', 'Api\UserFollowingsController@get');
        $app->post('/users/{user}/update-photo', 'Api\UsersController@updatePhoto');
        rest($app, '/users', 'UsersController', 'user', 'Api');

        // Tags
        rest($app, '/tags', 'TagsController', 'tag', 'Api', ['except' => ['all', 'get']]);

        // Event Categories
        rest($app, '/event-categories', 'EventCategoriesController', 'eventCategory', 'Api', ['except' => ['all', 'get']]);

        // Event Types
        rest($app, '/event-types', 'EventTypesController', 'eventType', 'Api', ['except' => ['all', 'get']]);

        // Events
        $app->post('/users/{user}/events/{event}/update-poster', 'Api\EventsController@updatePoster');
        rest($app, '/users/{user}/events', 'EventsController', 'event', 'Api', ['except' => ['all', 'get']]);

        // Transactions
        $app->get('/transactions', 'Api\TransactionsController@entireTransactions');
        rest($app, '/customers/{customer}/transactions', 'TransactionsController', 'transaction', 'Api');

        // Payments
        $app->get('/payments', 'Api\PaymentsController@entirePayments');
        $app->get('/customers/{customer}/payments', 'Api\PaymentsController@all');
        $app->get('/customers/{customer}/transactions/{transaction}/payments', 'Api\PaymentsController@entireTransactionPayments');
        rest($app, '/customers/{customer}/transactions/{transaction}/payments', 'PaymentsController', 'payment', 'Api', ['except' => ['all']]);

        // Tickets
        $app->get('/tickets', 'Api\TicketsController@entireTickets');
        $app->post('/tickets/scan', 'Api\TicketsController@scanning');
        $app->get('/customers/{customer}/tickets', 'Api\TicketsController@all');
        $app->get('/customers/{customer}/transactions/{transaction}/tickets', 'Api\TicketsController@entireTransactionTickets');
        rest($app, '/customers/{customer}/transactions/{transaction}/tickets', 'TicketsController', 'ticket', 'Api', ['except' => ['all']]);

        // All sold tickets on the event
        $app->get('/users/{user}/events/{event}/tickets', 'Api\TicketsController@entireSoldTicketsOnEvent');
    });

    if (env('APP_ENV', 'local') !== 'production') {
        $app->get('/tickets/unscan/{ticket}', 'Api\TicketsController@unscan');
    }
});
