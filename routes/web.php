<?php

use App\Events\User\UserCreated;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    $contents = [
        'message' => 'You need to contact dev@ramein.id if you want to use the API',
    ];

    return response()->json($contents);
});

$app->get('resend-email-activation', function () {
    $users = App\Models\User::where('activation_token', '!=', null)->get();

    $users->each(function ($user) {
        event(new UserCreated($user));
    });
});

$app->get('resend-email-activation/{user}', function (App\Models\User $user) {
    event(new UserCreated($user));
});

$app->post('/login', 'Auth\LoginController@login');
$app->post('/refresh-token', 'Auth\LoginController@refreshToken');
$app->post('/register', 'Auth\RegisterController@register');
$app->get('/register/activation/{token}', 'Auth\RegisterController@activateByToken');
