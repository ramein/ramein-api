#!/bin/bash
# Ensure we fail fast if there is a problem.
set -eo pipefail

# For more information, view https://docs.gitlab.com/ce/ci/ssh_keys/README.html

eval $(ssh-agent -s)

ssh-add <(echo "$SSH_PRIVATE_KEY")

mkdir -p ~/.ssh
echo "$SSH_PRIVATE_KEY" > ~/.ssh/deploy_private_key
chmod 400 ~/.ssh/deploy_private_key

[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
