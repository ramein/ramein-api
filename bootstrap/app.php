<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();
$app->withEloquent();

$app->configure('app');
$app->configure('database');
$app->configure('cors');
$app->configure('auth');
$app->configure('secrets');
$app->configure('filesystems');
$app->configure('scout');
$app->configure('mail');
$app->configure('services');
$app->configure('view');
$app->configure('googlemaps');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Cookie\QueueingFactory::class,
    'cookie'
);

$app->singleton(
    Illuminate\Contracts\Filesystem\Factory::class,
    function ($app) {
        return new Illuminate\Filesystem\FilesystemManager($app);
    }
);

$app->singleton(
    Illuminate\Contracts\Mail\Mailer::class,
    function ($app) {
        return $app['mailer'];
    }
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    Illuminate\Cookie\Middleware\EncryptCookies::class,
    Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
    Barryvdh\Cors\HandleCors::class,
    App\Http\Middleware\TrimStrings::class,
    App\Http\Middleware\ConvertEmptyStringsToNull::class,
]);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
    'debugbar' => App\Http\Middleware\DebugbarOnJson::class,
    'dev_api' => App\Http\Middleware\DevApi::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// Core
$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);
$app->register(Illuminate\Cookie\CookieServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);
$app->register(App\Providers\FormRequestServiceProvider::class);
$app->register(App\Providers\AppRequestServiceProvider::class);
$app->register(Illuminate\Mail\MailServiceProvider::class);
$app->register(Illuminate\Notifications\NotificationServiceProvider::class);

// Packages
$app->register(Laravel\Passport\PassportServiceProvider::class);
$app->register(Dusterio\LumenPassport\PassportServiceProvider::class);
$app->register(Silber\Bouncer\BouncerServiceProvider::class);
$app->register(Barryvdh\Cors\ServiceProvider::class);
$app->register(Jenssegers\Date\DateServiceProvider::class);
$app->register(App\Providers\ScoutServiceProvider::class);
$app->register(ScoutEngines\Elasticsearch\ElasticsearchProvider::class);
$app->register(Geocoder\Laravel\Providers\GeocoderService::class);

if (env('APP_DEBUG')) {
    $app->register(Barryvdh\Debugbar\LumenServiceProvider::class);
}

// Application
$app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(App\Providers\RouteBindingServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Register Class Aliases
|--------------------------------------------------------------------------
|
| Now we will register a few class aliases.
|
*/

// class_alias(Silber\Bouncer\BouncerFacade::class, 'Bouncer');
class_alias(Illuminate\Support\Facades\Config::class, 'Config');

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group([
    'namespace' => 'App\Http\Controllers',
], function ($app) {
    require base_path('routes/web.php');
});

$app->group([
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'api',
], function ($app) {
    require base_path('routes/api.php');
});

return $app;
