@component('mail::message')
  # Transaksi {{ $transaction->name }}

  Terima kasih telah menyelesaikan transaksi di {{ config('app.company') }}.

  Salam hangat,<br>
  <b>Tim {{ config('app.company') }}</b>
@endcomponent
