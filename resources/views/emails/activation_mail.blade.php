@component('mail::message')
  # Selamat Datang, {{ $user->name }}

  Agar dapat menggunakan akun Anda, kami memerlukan verifikasi alamat email yang Anda gunakan. Untuk itu, silahkan tekan tombol di bawah ini.

  @component('mail::button', ['url' => url('/register/activation', $token)])
    Aktifkan Akun
  @endcomponent

  Salam hangat,<br>
  <b>Tim {{ config('app.company') }}</b>
@endcomponent
