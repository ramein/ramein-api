<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data</title>
  </head>
  <body>
    <div>
      <pre><code class="json"></code></pre>
    </div>

    <script type="text/javascript">
      var data = {!! json_encode($data) !!};
      document.getElementsByClassName("json")[0].innerHTML = JSON.stringify(data, undefined, 2);
    </script>
    <script src="/js/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
  </body>
</html>
