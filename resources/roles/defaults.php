<?php

return [

    \App\Models\Owner::class => [
        'transactions.create',
    ],

    \App\Models\Customer::class => [
        'tags.create',
    ],

    \App\Models\EventOrganizer::class => [
        'tags.create',
        'events.create',
    ],

];
