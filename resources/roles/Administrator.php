<?php

return [
    'name' => 'Administrator',
    'permissions' => [
        'users.create',
        'events.create',
        'tags.create',
        'tags.update',
        'tags.delete',
        'event_categories.create',
        'event_categories.update',
        'event_categories.delete',
        'event_types.create',
        'event_types.update',
        'event_types.delete',
        'transactions.view_all',
        'payments.view_all',
        'tickets.view_all',
    ]
];
