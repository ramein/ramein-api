# Ramein API

Ramein API used for managing the resource that used by another Ramein Application.

## How to Use
1. Clone this repository
`git clone git@gitlab.com:ramein/ramein-api.git`
2. Cd into that directory
3. Run `composer install`
4. Duplicate `.env.example` to `.env`
5. Provide the `APP_KEY` value with some random string, like:
`base64:8Y6coT87VMVS5lfWa9ZwbgpWH5YcKhWgzkR/YiJZCNs=`
6. Set the database settings according to your system's database.
7. Run `php artisan migrate --seed`
8. To run the application, issue this command `php artisan serve`. By default it's use port 8000. If you like to use different port, just specify the port as an argument, like `php artisan serve --port=9000`
9. This API uses Laravel Storage to manage files. To make it work, don't forget to issue this command:
    ```
    php artisan storage:link
    ```

## Users
# Owner
admin@example.com/12345678 -> Super Administrator

owner.active@example.com/12345678 -> Active

owner.banned@example.com/12345678 -> Banned

owner.pending@example.com/12345678 -> Pending

# Customer
customer.active@example.com/12345678 -> Active

customer.banned@example.com/12345678 -> Banned

customer.pending@example.com/12345678 -> Pending

# Event Organizer
eo.free@example.com/12345678 -> Free

eo.premium@example.com/12345678 -> Premium

eo.banned@example.com/12345678 -> Banned

eo.pending@example.com/12345678 -> Pending

## Example
The example how to access the API you can see here:

[![Example](http://img.youtube.com/vi/TWvWZvobpO0/0.jpg)](http://www.youtube.com/watch?v=TWvWZvobpO0 "Example")

## Security Vulnerabilities

If you discover a security vulnerability within this API, please send an e-mail to dev@ramein.id. All security vulnerabilities will be promptly addressed.

## License

Ramein Application is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
